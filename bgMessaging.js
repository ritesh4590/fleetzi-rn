import firebase from 'react-native-firebase';

export default async (message) => {
    if (message) {
        console.log(message)
        const {
            topic, title, body
        } = message.data;
        const notification = new firebase.notifications.Notification()
            .setNotificationId(getRandomNotificationId())
            .setTitle(title)
            .setBody(body)
            .setData(message.data);
        // notification
        //     .android.setChannelId('my.channel')
        //     .android.setSmallIcon('ic_stat_ic_notification');
        firebase.notifications().displayNotification(notification);
    }
    Promise.resolve();
}