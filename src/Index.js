/**
* This is the Main file
* This file contains the routes of all the pages
**/

// React native and others libraries imports
import React, { Component } from 'react';
import { Dimensions, View, StyleSheet, ImageBackground, Text, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { StackViewStyleInterpolator } from 'react-navigation-stack';
import { Scene, Router, Stack } from 'react-native-router-flux';

// Our custom files and classes import
import Home2 from './screen/Home2';
import Listing from './screen/Listing';
import ManageListing from './screen/ManageListing';
import CreateListing from './screen/CreateListing';
import OrderMain from './screen/OrderMain';
import Payment from './screen/Payment';
import onBoardingLogin from './screen/onBoardingLogin'
import onBoardingHome from './screen/onBoardingHome'
import onBoardingCreateNew from './screen/onBoardingCreateNew'
import onBoardingRegistration from './screen//onBoardingRegistration'
import onBoardingVerification from './screen/onBoardingVerification'
import onBoardingOptionspage from './screen/onBoardingOptionspage'
import onBoardingOtp from './screen/onBoardingOTPpage'
import onBoardingLoginViaOtp from './screen/onBoardingLoginOTP'
import onBoardingForgotPassword from './screen/onBoardingForgotPassword'
import CompanyAddress from './screen/CompanyAddress'
import CompanyDetails from './screen/CompanyDetails'
import BusinessCertificate from './screen/BusinessCertificate'
import Contract from './screen/Contract'
import BankDetails from './screen/BankDetails'
import RNBootSplash from "react-native-bootsplash";
import HomeFooter from './component/svg/Home'
import Payments from './component/svg/Payments'
import ListingSVG from './component/svg/Listing'
import Order from './component/svg/Order'

(widthDim = Dimensions.get('window').width),
  (heightDim = Dimensions.get('window').height);
export default class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      ACCESSTOKEN: '',
      isLogin: true
    };
  }


  componentDidMount() {
    RNBootSplash.hide({ duration: 350 });
    AsyncStorage.getItem('ACCESSTOKEN').then((token) => {
      if (token !== null) {
        // We have data!!
        var eventstring = new String();
        eventstring = token.replace(/"/g, "");
        this.setState({ ACCESSTOKEN: eventstring, isLoading: false, isLogin: true })
      }
      else {
        this.setState({ isLoading: false, isLogin: false })
      }
    })
  }

  SettingsTab = (props) => {
    let textColor = props.focused ? '#FF8B00' : '#FFFFFF'
    switch (props.title) {
      case 'Home':
        // code block
        var Icon = <HomeFooter />
        break;
      case 'Listing':
        // code block
        var Icon = <ListingSVG />
        break;
      case 'Order':
        // code block
        var Icon = <Order />
        break;
      case 'Payment':
        // code block
        var Icon = <Payments />
        break;
      default:
        // code block
        var Icon = <HomeFooter />
    }

    let borderColor = props.focused ? '#333333' : '#FFFFFF'
    return (
      <View style={props.focused ? styles.iconBox : styles.onlyBox} >
        {Icon}
        <Text style={{ color: textColor, letterSpacing: 1, fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Light', paddingTop: 5, fontSize: 10 }}>{props.title}</Text>
      </View>
    );
  }
  render() {
    console.disableYellowBox = true;
    if (this.state.isLoading) {
      return (
        <ImageBackground
          source={require('../assets/Images/splash.png')}
          style={{ flex: 1 }}
        />
      )
    }
    return (
      <Router>
        <Stack key='root'>
          {/* Routes For Non Login Users  */}
          <Scene key='inactive' hideNavBar={true} initial={this.state.isLogin}>
            <Scene key="onBoardingHome" component={onBoardingHome} hideNavBar />
            <Scene key="login" component={onBoardingLogin} hideNavBar />
            <Scene key="loginviaotp" component={onBoardingLoginViaOtp} hideNavBar />
            <Scene key="onBoardingRegistration" component={onBoardingRegistration} hideNavBar />
            <Scene key="onBoardingOtp" component={onBoardingOtp} hideNavBar />
          </Scene>
          {/* Routes For Login Users  */}
          <Scene key='active' hideNavBar={true} initial={this.state.isLogin}>
            <Scene key='Tabbar' tabs={true} tabBarStyle={styles.tabBar} default='Main' hideNavBar={true} showLabel={false}>
              <Scene type='replace' key="home2" icon={this.SettingsTab} title='Home' component={Home2} hideNavBar />
              <Scene key="listing" icon={this.SettingsTab} title='Listing' component={Listing} hideNavBar />
              <Scene key="ordermain" icon={this.SettingsTab} title='Order' component={OrderMain} hideNavBar />
              <Scene key="payments" icon={this.SettingsTab} title='Payments' component={Payment} hideNavBar />
            </Scene>
            <Scene key="Contract" component={Contract} hideNavBar />
            <Scene key="BusinessCertificate" component={BusinessCertificate} hideNavBar />
            <Scene key="BankDetails" component={BankDetails} hideNavBar />
            <Scene key="CompanyAddress" component={CompanyAddress} hideNavBar />
            <Scene key="CompanyDetails" component={CompanyDetails} hideNavBar />
            <Scene key="createlisting" component={CreateListing} modal hideNavBar />
            <Scene key="managelisting" component={ManageListing} hideNavBar />
            <Scene key="onBoardingCreateNew" component={onBoardingCreateNew} hideNavBar />
            <Scene key="onBoardingOptionspage" component={onBoardingOptionspage} hideNavBar />
            <Scene key="onBoardingForgotPassword" component={onBoardingForgotPassword} hideNavBar />
            {/* <Scene  component={onBoardingVerification} hideNavBar /> */}
          </Scene>
        </Stack>
      </Router>
    );

  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 70
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80
  },
  tabBar: {
    height: 60,
    borderTopWidth: 1,
    opacity: 0.98,
    backgroundColor: '#0F1110',
    justifyContent: 'space-between',
    paddingLeft: 15,
    paddingRight: 15
  },
  iconBox: {
    backgroundColor: '#1A1D1C',
    height: 75,
    width: widthDim * 0.23,
    // paddingTop:15,
    borderRadius: 5,
    flexDirection: 'column', alignItems: 'center', justifyContent: 'center',
  },
  onlyBox: {
    width: widthDim * 0.23,
    flexDirection: 'column', alignItems: 'center', justifyContent: 'center',
  },
});