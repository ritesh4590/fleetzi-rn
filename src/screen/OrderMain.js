/**
* This is the Order Screen
**/

// React native and others libraries imports
import React, { Component, Fragment } from 'react';
import { Image, SafeAreaView, Alert, StyleSheet, Dimensions, Linking, ScrollView, StatusBar, Text, TouchableOpacity, PixelRatio, View } from 'react-native';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';

// Our custom files and classes import
import Header from '../component/Header'
import Footer from '../component/Footer'
import FlatList from '../component/FlatList'
import ListingMain from "../component/ListingMain";
import HeaderComp from "../component/HeaderComp"
import DashboardTwo from "../component/DashboardTwo"
import HomeFooter from '../component/svg/Home'
import Payments from '../component/svg/Payments'
import ListingSVG from '../component/svg/Listing'
import Order from '../component/svg/Order'
import NewOrder from '../component/NewOrder'
import Back from "../component/svg/back"
import LinearGradient from 'react-native-linear-gradient';

export default class OrderMain extends Component {


  render() {
    return (
      <Fragment>
        <StatusBar translucent backgroundColor="transparent" />
        <SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
        <SafeAreaView style={styles.screen}>
            <View style={styles.body}>
                {/* <FlatList /> */}
                <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{x:0, y: 0}}
          end={{x: 1, y: 1}}>
                <HeaderComp />  
                <View style={styles.createNewListingDiv}>
                <TouchableOpacity onPress={() => Actions.pop()}><Back/></TouchableOpacity> 
            
                  <Text style={styles.newListingText}>New Order</Text>
                </View>
                <ScrollView style={styles.container}> 

                <NewOrder type='payment' 
                heading='Payment' 
                prefixTitle='Fork Lift'
                Title='Manitou M50-4'
                sufixTitle="5 ton (2014 or later)"
                from='From'
                fromDate="11 Jan 2020"
                to='TO'
                toDate="2 Mar 2020"
                for="For"
                forDays="600 Days"
                location="Location"
                locationAdd='AL Olaya,'
                locationCity="Riyad "
                dueAmount="Amount Due for June "
                Amount="450000"
                payment='payment'
                thinLineCont='90000 is due for the total period of renting. '




                />
                 
                </ScrollView>
          </LinearGradient>
              
            </View>
            {/* <Footer index='ordermain'/> */}
                 
        </SafeAreaView>
        </Fragment>
    );
  }


}

const styles = StyleSheet.create({
  linearGradient:{
    flex:1
  },
    screen: {
      flex: 10,
      // backgroundColor:'#3e3e3e'
    },
    body: {
      flex: 10
    },
    createNewListingDiv:{
      flexDirection:'row',
      width:'90%',
      marginBottom:20,
      marginRight: 'auto',
      marginLeft: 'auto',
    },
    newListingText:{
      color:'white',
      fontSize:20,
      marginHorizontal:10,
      fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'


  },
    container:{
      width:'90%',
      marginTop: 0,
      marginRight: 'auto',
      marginBottom: 0,
      marginLeft: 'auto',
    },
  });