/**
 * This is the Onboarding Login Via OTP  Screen
 **/

// React native and others libraries imports
import React, { Component, Fragment } from 'react';
import {
  Image,
  SafeAreaView,
  Alert,
  StyleSheet,
  Dimensions,
  Picker,
  Platform,
  StatusBar,
  Text,
  TouchableOpacity,
  PixelRatio,
  View,
  ScrollView,
  Button,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import HeaderComp from '../component/HeaderComp';
import LinearGradient from 'react-native-linear-gradient';
import RNBootSplash from 'react-native-bootsplash';
import defaultConfig from '../config';
import FloatingLabel from 'react-native-floating-labels';
import EyeSvg from '../component/svg/EyeSvg';
import CTA from '../component/CTA'
import FloatingLabelInput from '../component/FloatingLabelInput';
import RNOtpVerify from 'react-native-otp-verify';

(screenwidth = Dimensions.get('window').width),
(screenheight = Dimensions.get('window').height);

function splitToDigit(n){
  return [...n + ''].map(String)
}
export default class onBoardingLoginViaOtp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      ACCESSTOKEN: '',
      full_name: '',
      phone_number: '',
      hidePassword: true,
      password: '',
      timer: 30,
      country_code: '+91',
      display_country_code: 'Ind +91',
      hasError: false,
      errorText: '',
      resendotp: false,
      submitbutoon: false,
      hash: '',
      phoneotp: '',
      phoneInputFouse: false,
      passwordInputFouse: false,
      otpsend: false,
      currentFlag: "🇮🇳",
      phoneotp1Focus: false,
      phoneotp2Focus: false,
      phoneotp3Focus: false,
      phoneotp4Focus: false,
      phoneotp5Focus: false,
    };
  }

  componentDidMount() {
    RNBootSplash.hide({ duration: 250 });
    this.getHash();
    this.startListeningForOtp();
    if (this.props.phone_number) {
      this.setState({ phone_number: this.props.phone_number })
    }
  }

  getHash = () =>
  RNOtpVerify.getHash()
  .then(console.log)
  .catch(console.log);

  startListeningForOtp = () =>
  RNOtpVerify.getOtp()
  .then(p => RNOtpVerify.addListener(this.otpHandler))
  .catch(p => console.log(p));

otpHandler = (message: string) => {
      console.log(message)
      const otp = splitToDigit(/(\d{4})/g.exec(message)[1]);
      console.log(otp)
      this.setState({
        phoneotp1:otp[0],
        phoneotp2:otp[1],
        phoneotp3:otp[2],
        phoneotp4:otp[3],
      })
      RNOtpVerify.removeListener();
      // Keyboard.dismiss();
}
componentWillUnmount() {
  RNOtpVerify.removeListener();
}

_onChangeCountryCode = () => {
  var current_country = this.state.display_country_code
  if (current_country === 'Ind +91') {
    this.setState({display_country_code:'KSA +966', country_code:'+966'})
  }
  if (current_country === 'KSA +966') {
    this.setState({display_country_code:'Ind +91', country_code:'+91'})
  }
}

  componentDidUpdate() {
    if (this.state.timer === 1) {
      this.setState({ timer: 30, resendotp: true });
      clearInterval(this.interval);
    }
  }

  onFocus(input) {
    if (input === 'phoneInputFouse') {
      this.setState({ phoneInputFouse: true, passwordInputFouse: false });
    }
    if (input === 'passwordInputFouse') {
      this.setState({ phoneInputFouse: false, passwordInputFouse: true });
    }
  }
  
  ChangeInputFocus(input) {
    if (input === 'phoneInputFouse') {
      this.setState({ phoneInputFouse: true, passwordInputFouse: false });
    }
    if (input === 'passwordInputFouse') {
      this.setState({ phoneInputFouse: false, passwordInputFouse: true });
    }
  }
  onBlur() {
    console.log('#####: onBlur');
    this.setState({ phoneInputFouse: false, passwordInputFouse: false });
  }

  _ChangeFocus(input) {
    if (input === 'phoneotp1Focus') {
      this.phoneotp2Focus.getInnerRef().focus()
    }
    if (input === 'phoneotp2Focus') {
      this.phoneotp3Focus.getInnerRef().focus()
    }
    if (input === 'phoneotp3Focus') {
      this.phoneotp4Focus.getInnerRef().focus()
    }
    if (input === 'phoneotp4Focus') {
      this.phoneotp5Focus.getInnerRef().focus()
    }
  }

  _OnChangeText(position, value) {
    if (position === 'phoneotp1Focus') {
      if (value) {
        this.setState({phoneotp1:value})
        this.phoneotp2Focus.getInnerRef().focus()
      } else {
        this.setState({phoneotp1:''})
      }
    }
    if (position === 'phoneotp2Focus') {
      if (value) {
        this.setState({phoneotp2:value})
        this.phoneotp3Focus.getInnerRef().focus()  
      } else {
        this.setState({phoneotp2:''})
        this.phoneotp1Focus.getInnerRef().focus()  
      }
    }
    if (position === 'phoneotp3Focus') {
      if (value) {
        this.setState({phoneotp3:value})
        this.phoneotp4Focus.getInnerRef().focus()
      } else {
        this.setState({phoneotp3:''})
        this.phoneotp2Focus.getInnerRef().focus()  
      }
    }
    if (position === 'phoneotp4Focus') {
      if (value) {
        this.setState({phoneotp4:value})
        this.phoneotp5Focus.getInnerRef().focus()
      } else {
        this.setState({phoneotp4:''})
        this.phoneotp3Focus.getInnerRef().focus()  
      }
    }
    if (position === 'phoneotp5Focus') {
      if (value) {
        this.setState({phoneotp5:value})
        this.phoneotp5Focus.getInnerRef().focus()
      } else {
        this.setState({phoneotp5:''})
        this.phoneotp4Focus.getInnerRef().focus()  
      }

    }
  }
  RunTimer() {
    this.interval = setInterval(
      () => this.setState(prevState => ({ timer: prevState.timer - 1 })),
      1000,
    );
  }

  get_otp() {
    if (!!this.state.phone_number && this.state.country_code) {
      this.RunTimer();
      this.setState({ isLoading: true });
      let formdata = new FormData();
      formdata.append('country_code', this.state.country_code);
      formdata.append('number', this.state.phone_number);
      formdata.append('platform', 'part');
      fetch(
        'http://api.fleetzi.com/v1/token/login/otp/request-sms/?format=json',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'multipart/form-data',
          },
          body: formdata,
        },
      )
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.status == 'success') {
            this.setState({
              isLoading: false,
              hash: responseJson.data.hash,
              submitbutoon: true,
              otpsend: true,
            });
          } else {
            this.setState({ isLoading: false });
            console.log('somthing error');
          }
        })
        .catch(error => {
          console.error(error);
          console.log('somthing error');
          this.setState({ isLoading: false });
        });
    } else {
      this.setState({
        isLoading: false,
        hasError: true,
        errorText: 'Enter your phone no.',
      });
    }
  }

  veryfy_otp() {
    if (!!this.state.phoneotp1 && this.state.phoneotp2 && this.state.phoneotp3 && this.state.phoneotp4 && this.state.phoneotp5) {
      this.setState({ isLoading: true });
      let formdata = new FormData();
      formdata.append('country_code', this.state.country_code);
      formdata.append('number', this.state.phone_number);
      formdata.append('hash', this.state.hash);
      formdata.append('otp', this.state.phoneotp1 + this.state.phoneotp2 + this.state.phoneotp3 + this.state.phoneotp4 + this.state.phoneotp5);
      console.log(formdata);
      fetch('http://api.fleetzi.com/v1/token/login/otp/verify/?format=json', {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formdata,
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log(responseJson);
          if (responseJson.status == 'success') {
            AsyncStorage.setItem('USERDATA', JSON.stringify(responseJson.data));
            AsyncStorage.setItem(
              'ACCESSTOKEN',
              JSON.stringify(responseJson.data.access_token),
            );
            this.setState({ isLoading: false });
            Actions.home2();
          } else {
            this.setState({
              isLoading: false,
              hasError: true,
              errorText: responseJson.message,
            });
            Alert.alert(responseJson.message);
            console.log('somthing error');
          }
        })
        .catch(error => {
          console.error(error);
          console.log('somthing error');
          this.setState({ isLoading: false });
        });
    } else {
      this.setState({
        isLoading: false,
        hasError: true,
        errorText: 'Enter your OTP.!',
      });
    }
  }

  setPasswordVisibility = () => {
    this.setState({ hidePassword: !this.state.hidePassword });
  };

  render() {
    console.log(this.state.submitbutoon);
    return (
      <Fragment>
        <LinearGradient
          colors={['#323232', '#14181A', '#000000']}
          style={styles.linearGradient}
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 1 }}>
          {/* <StatusBar translucent backgroundColor="transparent" /> */}
          <SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
          <SafeAreaView style={styles.screen}>
            <ScrollView style={styles.body} keyboardShouldPersistTaps='handled'>
              <HeaderComp customer_support={true} />
              {/* <View style={{ height: '20%' }}></View> */}
              <KeyboardAvoidingView>
                <View
                  style={{
                    paddingLeft: 20,
                    paddingRight: 20,
                    marginTop: -(screenheight * 0.07),
                  }}>

                  <Text
                    style={{
                      fontFamily:
                        Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
                      color: 'white',
                      lineHeight: 60,
                      fontSize: 30,
                      // paddingBottom: 10,
                    }}>
                    {/* login via {'\n'}Verification Code */}
                    Login
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      color: 'white',
                      opacity: 0.4,
                      // lineHeight: 25,
                      fontFamily:
                        Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
                    }}>
                    Enter your phone number to login
                  </Text>
                  {/* <Text
                    style={{
                      fontSize: 14,
                      color: 'white',
                      opacity: 0.4,
                      letterSpacing: 0.3,
                      lineHeight: 25,
                      margin: 0,
                      fontFamily:
                        Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
                    }}>
                    We'll verify your email and phone no. to create your
                    account.
                marginTop:20,
                  </Text> */}
                  <View style={{marginTop: 20,}}>
                    <View style={[
                      styles.formInput,
                      {
                        flexDirection: 'row',
                        borderColor: this.state.phoneInputFouse
                          ? '#FFFFFF'
                          : '#333',
                      },
                    ]}>
                      <TouchableOpacity onPress={this._onChangeCountryCode}  style={{ width: '22%', justifyContent: 'center',justifyContent:'center', }}>
                        <Text style={{fontSize:14, color: 'white', paddingTop: 15, fontFamily: 'Nexa Regular' }}>{this.state.display_country_code}</Text>
                      </TouchableOpacity>
                      <View style={{ width: '80%' }}>
                        <FloatingLabel
                          labelStyle={styles.labelInput}
                          inputStyle={styles.input}
                          keyboardType="numeric"
                          value={this.state.phone_number}
                          onEndEditing={() => this.onBlur()}
                          onFocus={() => this.onFocus('phoneInputFouse')}
                          onChangeText={number =>
                            this.setState({ phone_number: number })
                          }>
                          Mobile number
                      </FloatingLabel>
                      </View>
                    </View>


                    {
                      this.state.otpsend ?
                        <View>
                          <Text style={{
                            color: '#7D7D7D', fontSize: 14, marginTop: 20, fontFamily:
                              Platform.OS === 'ios'
                                ? 'NexaRegular'
                                : 'Nexa Regular',
                               marginTop:'10%' ,}}>
                            Enter SMS Verification Code
                      </Text>
                          <View style={{width:'100%', flexDirection: 'row', justifyContent: 'space-between',height:50 }}>
                            <FloatingLabelInput
                              labelStyle={styles.labelInput}
                              inputStyle={styles.input}
                              style={[styles.formInput1, {
                                borderColor: this.state.phoneotp1Focus
                                  ? '#FFFFFF'
                                  : '#333',
                              }]}
                              keyboardType="number-pad"
                              maxLength={1}
                              value={this.state.phoneotp1}
                              returnKeyType={'next'}
                              onSubmitEditing={() => this._ChangeFocus('phoneotp1Focus')}
                              ref={r => (this.phoneotp1Focus = r)}
                              onFocus={() => this.onFocus('phoneotp1Focus')}
                              onChangeText={text => this._OnChangeText('phoneotp1Focus', text)}>
                              {/* Sent to {this.state.country_code}-{this.state.number} */}
                            </FloatingLabelInput>
                            <FloatingLabelInput
                              labelStyle={styles.labelInput}
                              inputStyle={styles.input}
                              style={[styles.formInput1, {
                                borderColor: this.state.phoneotp2Focus
                                  ? '#FFFFFF'
                                  : '#333',
                              }]}
                              maxLength={1}
                              keyboardType="number-pad"
                              value={this.state.phoneotp2}
                              returnKeyType={'next'}
                              onSubmitEditing={() => this._ChangeFocus('phoneotp2Focus')}
                              ref={r => (this.phoneotp2Focus = r)}
                              onFocus={() => this.onFocus('phoneotp2Focus')}
                              onChangeText={text => this._OnChangeText('phoneotp2Focus', text)}>
                              {/* Sent to {this.state.country_code}-{this.state.number} */}
                            </FloatingLabelInput>
                            <FloatingLabelInput
                              labelStyle={styles.labelInput}
                              inputStyle={styles.input}
                              style={[styles.formInput1, {
                                borderColor: this.state.phoneotp3Focus
                                  ? '#FFFFFF'
                                  : '#333',
                              }]}
                              maxLength={1}
                              keyboardType="number-pad"
                              value={this.state.phoneotp3}
                              returnKeyType={'next'}
                              onSubmitEditing={() => this._ChangeFocus('phoneotp3Focus')}
                              ref={r => (this.phoneotp3Focus = r)}
                              onFocus={() => this.onFocus('phoneotp3Focus')}
                              onChangeText={text => this._OnChangeText('phoneotp3Focus', text)}>
                              {/* Sent to {this.state.country_code}-{this.state.number} */}
                            </FloatingLabelInput>
                            <FloatingLabelInput
                              labelStyle={styles.labelInput}
                              inputStyle={styles.input}
                              style={[styles.formInput1, {
                                borderColor: this.state.phoneotp4Focus
                                  ? '#FFFFFF'
                                  : '#333',
                              }]}
                              maxLength={1}
                              keyboardType="number-pad"
                              value={this.state.phoneotp4}
                              returnKeyType={'next'}
                              onSubmitEditing={() => this._ChangeFocus('phoneotp4Focus')}
                              ref={r => (this.phoneotp4Focus = r)}
                              onFocus={() => this.onFocus('phoneotp4Focus')}
                              onChangeText={text => this._OnChangeText('phoneotp4Focus', text)}>
                              {/* Sent to {this.state.country_code}-{this.state.number} */}
                            </FloatingLabelInput>
                            <FloatingLabelInput
                              labelStyle={styles.labelInput}
                              inputStyle={styles.input}
                              maxLength={1}
                              style={[styles.formInput1, {
                                borderColor: this.state.phoneotp5Focus
                                  ? '#FFFFFF'
                                  : '#333',
                              }]}
                              keyboardType="number-pad"
                              value={this.state.phoneotp5}
                              returnKeyType={'done'}
                              // onSubmitEditing={() => this._ChangeFocus('phoneotp3Focus')}
                              ref={r => (this.phoneotp5Focus = r)}
                              onFocus={() => this.onFocus('phoneotp5Focus')}
                              onChangeText={text => this._OnChangeText('phoneotp5Focus', text)}>
                              {/* Sent to {this.state.country_code}-{this.state.number} */}
                            </FloatingLabelInput>

                          </View>
                        </View>
                        :
                        <View></View>
                    }

                    {/* <View
                    style={{
                      marginTop: 20,
                      flex: 1,
                      justifyContent:'center',
                       alignItems:'center',
                       borderColor:'red',borderWidth:1
                     
                    }}>
                    <Text
                      style={{
                        fontFamily:
                          Platform.OS === 'ios'
                            ? 'NexaRegular'
                            : 'Nexa Regular',
                        color: '#FF9800',
                      }}> */}
                    {/* 00:{this.state.timer} */}
                    {/* </Text> */}
                    <View style={{marginTop:'5%', width: '100%', justifyContent: 'space-between', flexDirection: 'row-reverse' }}>
                      {
                        this.state.resendotp ?
                          <TouchableOpacity onPress={() => this.get_otp()}>
                            <Text
                              style={{
                                fontFamily:
                                  Platform.OS === 'ios'
                                    ? 'NexaRegular'
                                    : 'Nexa Regular',
                                color: '#FFFFFF',
                                // justifyContent:'flex-end'
                              }}>
                              Resend OTP
                        </Text>
                          </TouchableOpacity>
                          :
                          <Text></Text>
                      }
                      {this.state.otpsend ? (
                        <Text
                          style={{
                            marginTop: 5,
                            fontFamily:
                              Platform.OS === 'ios'
                                ? 'NexaRegular'
                                : 'Nexa Regular',
                            color: '#9A631F',

                          }}>
                          00:{this.state.timer}
                        </Text>
                      ) : (
                          <>
                            {/* <Text
                        style={{
                          fontFamily:
                            Platform.OS === 'ios'
                              ? 'NexaRegular'
                              : 'Nexa Regular',
                          color: 'white',
                        }}>
                        Resend OTP 
                      </Text>
                      <Text>{'\n'}</Text>

                      <Text
                      style={{
                        marginTop:-30,
                        fontFamily:
                          Platform.OS === 'ios'
                            ? 'NexaRegular'
                            : 'Nexa Regular',
                        color: '#9A631F',
                      }}>
                       00:{this.state.timer}
                    </Text> */}
                          </>
                        )}</View>


                    <Text>{'\n'}</Text>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                      {this.state.submitbutoon ? (
                        <TouchableOpacity
                          onPress={() => this.veryfy_otp()}
                          style={{ width: '100%', }}>
                          <CTA ctaText='Login' />
                        </TouchableOpacity>
                      ) : (
                          <TouchableOpacity
                            onPress={() => this.get_otp()}
                            style={{ width: '100%', }}>
                            <CTA ctaText='NEXT' />
                          </TouchableOpacity>
                        )}</View>
                  </View>
                </View>
              </KeyboardAvoidingView>
            </ScrollView>
          </SafeAreaView>
        </LinearGradient>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 10,
    backgroundColor: 'transparent',
  },
  body: {
    flex: 10,
  },
  linearGradient: {
    flex: 1,
  },
  labelInput: {
    fontSize:14,
    color: '#7D7D7D',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
  },
  formInput: {
    borderBottomWidth: 1,
    borderColor: '#333',
    color: '#FFFFFF',
  },
  formInput1: {
    borderBottomWidth: 1,
    width: '15%',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },
  input: {
    borderWidth: 0,
    color: '#FFFFFF',
    fontSize: 14,
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },
  ButtonView: {
    backgroundColor: '#FF8B00',
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#FFFFFF',
    borderBottomWidth: 5,
    opacity: 0.85,

  },
  buttomText: {
    color: '#261400',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
    fontSize: 14,
    marginTop: screenheight * 0.01,
  },
});
