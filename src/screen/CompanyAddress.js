import React, { Component, Fragment } from 'react';
import {
	Image,
	SafeAreaView,
	Alert,
	StyleSheet,
	Dimensions,
	Picker,
	Platform,
	StatusBar,
	Text,
	TouchableOpacity,
	PixelRatio,
	View,
	ScrollView,
	Button,
	TextInput,
	KeyboardAvoidingView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import HeaderComp from '../component/HeaderComp';
import LinearGradient from 'react-native-linear-gradient';
import RNBootSplash from 'react-native-bootsplash';
import defaultConfig from '../config';
import FloatingLabelInput from '../component/FloatingLabelInput';
import FloatingLabel from 'react-native-floating-labels';
import Pen from '../component/svg/Pen'
import Cross from '../component/svg/Cross';
import CTA from '../component/CTA'
import Dropdown from '../component/svg/Dropdown'
import Spinner from '../component/Spinner';
import Back from '../component/svg/back'

(screenwidth = Dimensions.get('window').width),
	(screenheight = Dimensions.get('window').height);
export default class CompanyAddress extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: false,
			address: '',
			city: '',
			state: '',
			country: '',
			postal_address: '',
			po_box: '',
			type: 'HQ',
			companyuid: '',
			Error_Text: '',
			error_filed: '',
			ACCESSTOKEN: '',


		};
	}
	componentDidMount() {
		this._retrieveData();
		this.setState({
			companyuid: this.props.companyuid
		})
	}

	_retrieveData = async () => {
		try {
			const value = await AsyncStorage.getItem('ACCESSTOKEN');
			if (value !== null) {
				// We have data!!
				var eventstring = new String();
				eventstring = value.replace(/"/g, "");
				this.setState({ ACCESSTOKEN: eventstring })
				this._getDocument()
				console.log(eventstring);
			}
		} catch (error) {
			// Error retrieving data
		}
	};


	_OnSubmit() {
		if (!!this.state.address && this.state.city && this.state.state && this.state.country &&
			this.state.postal_address && this.state.po_box && this.state.type) {
			this.setState({ isLoading: true })
			var myHeaders = new Headers();
			myHeaders.append("Authorization", "Token " + this.state.ACCESSTOKEN);

			var formdata = new FormData();
			formdata.append("address_line1", this.state.address);
			formdata.append("po_box", this.state.address);
			formdata.append("address_line2");
			formdata.append("landmark", "");
			formdata.append("type", "HQ");
			formdata.append("city", this.state.city);
			formdata.append("zipcode", this.state.postal_address);

			var requestOptions = {
				method: 'POST',
				headers: myHeaders,
				body: formdata,
				redirect: 'follow'
			};

			fetch("http://api.fleetzi.com/v1/management/companies/" + this.state.companyuid + "/addresses", requestOptions)
				.then((response) => response.json())
				.then((responseJson) => {
					console.log(responseJson)
					this.setState({ isLoading: false })
					Actions.Tabbar()
				})
				.catch(error => console.log('error', error));
		} else {
			this._Error_handel()
		}
	}

	_Error_handel = () => {
		console.log(this.state)
		if (this.state.address === '') {
			this.setState({ Error_Text: 'Please Enter address', error_filed: 'address', isLoading: false, })
		} else if (this.state.city === '') {
			this.setState({ Error_Text: 'Please Enter city', error_filed: 'city', isLoading: false, })
		} else if (this.state.state === '') {
			this.setState({ Error_Text: 'Please Enter state', error_filed: 'state', isLoading: false, })
		} else if (this.state.country === '') {
			this.setState({ Error_Text: 'Please Enter country', error_filed: 'country', isLoading: false, })
		} else if (this.state.postal_address === '') {
			this.setState({ Error_Text: 'Please Enter postal address', error_filed: 'postal_address', isLoading: false, })
		} else if (this.state.po_box === '') {
			this.setState({ Error_Text: 'Please Enter po box', error_filed: 'po_box', isLoading: false, })
		} else if (this.state.type === '') {
			this.setState({ Error_Text: 'Please Enter type', error_filed: 'type', isLoading: false, })
		}
	}


	render() {
		return (
			<Fragment>
				<LinearGradient
					colors={['#323232', '#14181A', '#000000']}
					style={styles.linearGradient}
					start={{ x: 0, y: 0 }}
					end={{ x: 1, y: 1 }}>
					<StatusBar translucent backgroundColor="transparent" />
					<SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
					<SafeAreaView style={styles.screen}>
						<Spinner visible={this.state.isLoading} />
						<View style={styles.container}>
							<View style={styles.headerContainer}>
								<View>
									<Text style={styles.header}>
										Enter company address
								</Text>
								</View>
								{/* <TouchableOpacity onPress={() => Actions.pop()}>
									<Cross />
								</TouchableOpacity> */}
							</View>
							<ScrollView style={{ height: '72%', }}>
								<KeyboardAvoidingView>
									<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
										<View style={{ width: '100%' }}>

											<FloatingLabelInput
												label="Address"
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.address}
												returnKeyType={'next'}
												blurOnSubmit={false}
												onSubmitEditing={() => this.refadd.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ address: text })
												}
											></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>
									{
										this.state.error_filed === 'address' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}

									<View style={{ borderBottomWidth: 1, borderColor: '#393a3b', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}>
										<View style={{ width: '100%' }}>
											<Picker
												selectedValue={this.state.type}
												style={{
													color: '#6a6b6c',
													height: 50, width: '100%',
													backgroundColor: 'transparent',
													fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
												}}

												onValueChange={(itemValue, itemIndex) =>
													this.setState({ type: itemValue })
												}>
												<Picker.Item label="Office" value="HQ" />
											</Picker>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}>
											<Dropdown />
										</View>
									</View>
									{
										this.state.error_filed === 'type' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}

									<View style={{ flexDirection: 'row', alignItems: 'center', }}>
										<View style={{ width: '100%' }}>
											<FloatingLabelInput
												label="City"
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.city}
												blurOnSubmit={false}
												returnKeyType={'next'}
												ref={r => (this.refadd = r)}
												onSubmitEditing={() => this.refcity.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ city: text })
												}
											></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>
									{
										this.state.error_filed === 'city' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}

									<View style={{ flexDirection: 'row', alignItems: 'center', }}>
										<View style={{ width: '100%' }}>
											<FloatingLabelInput
												label="State/Province"
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.state}
												blurOnSubmit={false}
												returnKeyType={'next'}
												ref={r => (this.refcity = r)}
												onSubmitEditing={() => this.refstate.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ state: text })
												}
											></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>

									{
										this.state.error_filed === 'state' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}

									<View style={{ flexDirection: 'row', alignItems: 'center', }}>
										<View style={{ width: '100%' }}>
											<FloatingLabelInput
												label="country"
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.country}
												returnKeyType={'next'}
												blurOnSubmit={false}
												ref={r => (this.refstate = r)}
												onSubmitEditing={() => this.refcountry.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ country: text })
												}></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>

									{
										this.state.error_filed === 'country' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}
									<View style={{ flexDirection: 'row', alignItems: 'center', }}>
										<View style={{ width: '100%' }}>
											<FloatingLabelInput
												label="Postal Address"
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.postal_address}
												returnKeyType={'next'}
												blurOnSubmit={false}
												ref={r => (this.refcountry = r)}
												onSubmitEditing={() => this.refpostal.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ postal_address: text })
												}></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>
									{
										this.state.error_filed === 'postal_address' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}
									<View style={{ flexDirection: 'row', alignItems: 'center', }}>
										<View style={{ width: '100%' }}>
											<FloatingLabelInput
												label="P.O. Box"
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.po_box}
												returnKeyType={'done'}
												blurOnSubmit={false}
												ref={r => (this.refpostal = r)}
												onChangeText={text =>
													this.setState({ po_box: text })
												}></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>
									{
										this.state.error_filed === 'po_box' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}

									{/* </View> */}
								</KeyboardAvoidingView>
							</ScrollView>
							<View style={{ flex: 1, justifyContent: 'flex-end', width: '100%', }}>
								<View style={{ marginTop: 30 }}>
									{/* <TouchableOpacity onPress={() => this._OnSubmit()} >
										<CTA ctaText={'UPDATE DETAILS'} />
									</TouchableOpacity> */}

									<View style={{ flexDirection: 'row', width: '100%', marginTop: 30 }}>
										<TouchableOpacity onPress={() => Actions.pop()} style={{ width: '20%', backgroundColor: '#222223', height: 50, justifyContent: "center", alignItems: 'center' }}>
											<Back />
										</TouchableOpacity>
										<TouchableOpacity onPress={() => this._OnSubmit()} style={{ width: '75%', marginHorizontal: 20 }}>
											<CTA ctaText={'UPDATE DETAILS'} />
										</TouchableOpacity>
									</View>
								</View>
							</View>
						</View>
					</SafeAreaView>
				</LinearGradient>
			</Fragment>
		);
	}
}



const styles = StyleSheet.create({
	screen: {
		flex: 10,
		backgroundColor: 'transparent',
	},
	body: {
		flex: 10,
	},
	linearGradient: {
		flex: 1,
	},
	container: {
		flex: 1,
		paddingLeft: 20,
		paddingRight: 20
	},
	header: {
		color: '#FFFFFF',
		fontSize: 20,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
	},
	headerContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: 50
	},
	labelInput: {
		color: '#7D7D7D',
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
	},
	formInput: {
		fontSize: 18,
		borderBottomWidth: 1,
		borderColor: '#333',
		color: '#FFFFFF',
		fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
	},
	input: {
		borderWidth: 0,
		color: '#FFFFFF',
		fontSize: 14,
	},
})