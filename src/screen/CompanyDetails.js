import React, { Component, Fragment } from 'react';
import {
	Image,
	SafeAreaView,
	Alert,
	StyleSheet,
	Dimensions,
	Picker,
	Platform,
	StatusBar,
	Text,
	TouchableOpacity,
	PixelRatio,
	View,
	ScrollView,
	Button,
	TextInput,
	KeyboardAvoidingView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import HeaderComp from '../component/HeaderComp';
import LinearGradient from 'react-native-linear-gradient';
import RNBootSplash from 'react-native-bootsplash';
import defaultConfig from '../config';
import FloatingLabelInput from '../component/FloatingLabelInput';
import FloatingLabel from 'react-native-floating-labels';
import Pen from '../component/svg/Pen'
import Cross from '../component/svg/Cross';
import CTA from '../component/CTA'
import Dropdown from '../component/svg/Dropdown'
import Spinner from '../component/Spinner';
import Back from '../component/svg/back'

(screenwidth = Dimensions.get('window').width),
	(screenheight = Dimensions.get('window').height);
export default class CompanyDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			company_name: '',
			parent_compnay_name: '',
			website: '',
			cr_no: '',
			telephone_no: '',
			fax_no: '',
			company_email: '',
			employee_count: '',
			company_type: 'P',
			type: '',
			year_in_business: '',
			uid: '',
			Error_Text: '',
			error_filed: '',
			ACCESSTOKEN: '',
		};
	}

	componentDidMount() {
		this._retrieveData();
		this.setState({
			uid: this.props.companyuid
		})

	}

	_retrieveData = async () => {
		try {
			const value = await AsyncStorage.getItem('ACCESSTOKEN');
			if (value !== null) {
				// We have data!!
				var eventstring = new String();
				eventstring = value.replace(/"/g, "");
				this.setState({ ACCESSTOKEN: eventstring })
				this._getCompanyDetails()
				console.log(eventstring);
			}
		} catch (error) {
			// Error retrieving data
		}
	};

	_getCompanyDetails() {
		var myHeaders = new Headers();
		myHeaders.append("Authorization", "Token " + this.state.ACCESSTOKEN);
		var requestOptions = {
			method: 'GET',
			headers: myHeaders,
		};

		fetch("http://api.fleetzi.com/v1/management/companies/" + this.state.uid, requestOptions)
			.then((response) => response.json())
			.then((responseJson) => {
				console.log(responseJson.data)
				this.setState({
					uid: responseJson.data.uid,
					company_name: responseJson.data.name,
					employee_count: responseJson.data.company_size,
					parent_compnay_name: responseJson.data.parent_company,
					type: responseJson.data.type,
					website: responseJson.data.website,
					company_email: responseJson.data.email,
					fax_no: responseJson.data.fax,
					cr_no: responseJson.data.registration_number,
					telephone_no: responseJson.data.phone,
					isLoading: false
				})
			})
			.catch(error => console.log('error', error));
	}

	_OnSubmit() {
		if (!!this.state.company_name &&
			this.state.parent_compnay_name &&
			this.state.website &&
			this.state.cr_no &&
			this.state.telephone_no &&
			this.state.fax_no &&
			this.state.company_email &&
			this.state.employee_count &&
			this.state.type) {
			this.setState({ isLoading: true })
			var myHeaders = new Headers();
			myHeaders.append("Authorization", "Token " + this.state.ACCESSTOKEN);
			var formdata = new FormData();
			formdata.append("name", this.state.company_name);
			formdata.append("company_size", this.state.employee_count);
			formdata.append("parent_company", this.state.parent_compnay_name);
			formdata.append("type", '');
			formdata.append("website", this.state.website);
			formdata.append("email", this.state.company_email);
			formdata.append("fax", this.state.fax_no);
			formdata.append("phone", this.state.telephone_no);
			formdata.append("registration_number", this.state.cr_no);
			formdata.append("vat_number", '');
			formdata.append("fleetzi_company_type", "P");
			console.log(formdata)
			var requestOptions = {
				method: 'PUT',
				headers: myHeaders,
				body: formdata,
			};
			fetch("http://api.fleetzi.com/v1/management/companies/" + this.state.uid, requestOptions)
				.then((response) => response.json())
				.then((responseJson) => {
					console.log(responseJson)
					this.setState({ isLoading: false })
					console.log("ab run hoga actions")
					Actions.Tabbar()
				})
				.catch(error => console.log('error', error));
		} else {
			this._Error_handel()
		}

	}

	_Error_handel = () => {
		console.log(this.state)
		if (this.state.company_name === '') {
			this.setState({ Error_Text: 'Please Enter Company Name', error_filed: 'company_name', isLoading: false, })
		} else if (this.state.parent_compnay_name === '') {
			this.setState({ Error_Text: 'Please Enter Parent Company Name', error_filed: 'parent_compnay_name', isLoading: false, })
		} else if (this.state.employee_count === '') {
			this.setState({ Error_Text: 'Please No of Employee ', error_filed: 'employee_count', isLoading: false, })
		} else if (this.state.company_type === '') {
			this.setState({ Error_Text: 'Please Select Type ', error_filed: 'type', isLoading: false, })
		} else if (this.state.website === '') {
			this.setState({ Error_Text: 'Please Enter website Name', error_filed: 'website', isLoading: false, })
		} else if (this.state.cr_no === '') {
			this.setState({ Error_Text: 'Please Enter Company Reg Name', error_filed: 'cr_no', isLoading: false, })
		} else if (this.state.telephone_no === '') {
			this.setState({ Error_Text: 'Please Enter Tel No', error_filed: 'telephone_no', isLoading: false, })
		} else if (this.state.fax_no === '') {
			this.setState({ Error_Text: 'Please Enter Fax No.', error_filed: 'fax_no', isLoading: false, })
		} else if (this.state.company_email === '') {
			this.setState({ Error_Text: 'Please Enter Company Email', error_filed: 'company_email', isLoading: false, })
		}
	}


	render() {
		return (
			<Fragment>
				<LinearGradient
					colors={['#323232', '#14181A', '#000000']}
					style={styles.linearGradient}
					start={{ x: 0, y: 0 }}
					end={{ x: 1, y: 1 }}>
					<StatusBar translucent backgroundColor="transparent" />
					<SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
					<SafeAreaView style={styles.screen}>
						<Spinner visible={this.state.isLoading} />
						<View style={styles.container}>
							<View style={styles.headerContainer}>
								<View>
									<Text style={styles.header}>
										Enter company details
								</Text>
								</View>
								{/* <TouchableOpacity onPress={() => Actions.pop()}>
									<Cross />
								</TouchableOpacity> */}
							</View>
							<ScrollView style={{ height: '72%', }}>
								<KeyboardAvoidingView>
									<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
										<View style={{ width: '100%' }}>
											<FloatingLabelInput
												label="Company Name"
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.company_name}
												returnKeyType={'next'}
												blurOnSubmit={false}
												onSubmitEditing={() => this.refparent.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ company_name: text })
												}
											></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>
									{
										this.state.error_filed === 'company_name' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}

									<View style={{ flexDirection: 'row', alignItems: 'center', }}>
										<View style={{ width: '100%' }}>
											<FloatingLabelInput
												label="Parent company Name"
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.parent_compnay_name}
												returnKeyType={'next'}
												blurOnSubmit={false}
												ref={r => (this.refparent = r)}
												onSubmitEditing={() => this.refwebsite.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ parent_compnay_name: text, Error_Text: '', error_filed: '' })
												}
											></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>
									{
										this.state.error_filed === 'parent_compnay_name' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}
									<View style={{ borderBottomWidth: 1, borderColor: '#393a3b', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}>
										<View style={{ width: '100%' }}>
											<Picker
												selectedValue={this.state.employee_count}
												style={{
													color: '#6a6b6c',
													backgroundColor: 'transparent',
													height: 50, width: '100%',
													fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
												}}
												onValueChange={(itemValue, itemIndex) =>
													this.setState({ employee_count: itemValue, Error_Text: '', error_filed: '' })
												}>
												<Picker.Item label="No. of Employee" value="No. of Employee" />
												<Picker.Item label="0-5" value="0to5" />
												<Picker.Item label="5-10" value="5to10" />
												<Picker.Item label="10-50" value="10to50" />
											</Picker>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}>
											<Dropdown />
										</View>
									</View>
									{
										this.state.error_filed === 'employee_count' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}
									<View style={{
										borderBottomWidth: 1, borderColor: '#393a3b',
										justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center'
									}}>
										<View style={{ width: '100%' }}>
											<Picker
												selectedValue={this.state.type}
												style={{
													color: '#6a6b6c',
													height: 50, width: '100%',
													backgroundColor: 'transparent',
													fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
												}}
												onValueChange={(itemValue, itemIndex) =>
													this.setState({ type: itemValue, Error_Text: '', error_filed: '' })
												}>
												<Picker.Item label="Public Co" value="PC" />
												<Picker.Item label="Limited Liability Co" value="LLC" />
												<Picker.Item label="Partnership" value='P' />
												<Picker.Item label="Establishment" value="E" />
												<Picker.Item label="Others" value="O" />
											</Picker>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}>
											<Dropdown />
										</View>
									</View>
									{
										this.state.error_filed === 'type' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}
									{/* <View style={{ marginTop: 10, borderBottomWidth: 1, borderColor: '#393a3b' ,
									justifyContent:'space-between',flexDirection:'row',alignItems:'center' }}>
									<View style={{width:'100%'}}>
										<Picker
											selectedValue={this.state.year_in_business}
											style={{
												color: '#6a6b6c',
												height: 50, width: '100%',
												backgroundColor: 'transparent',
												fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
											}}
											onValueChange={(itemValue, itemIndex) =>
												this.setState({ language: itemValue })
											}>
											<Picker.Item label="Year in Business" value="Year in Business" />
											<Picker.Item label="12 Year" value="12 Year" />
											<Picker.Item label="10 Year" value="10 Year" />
											<Picker.Item label="8 Year" value="8 Year" />
										</Picker>
										</View>
										<View style={{width:'8%',position:'absolute',right:0}}>
											<Dropdown/>
										</View>
									</View> */}

									<View style={{ flexDirection: 'row', alignItems: 'center', }}>
										<View style={{ width: '100%' }}>
											<FloatingLabelInput
												label="Webiste"
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.website}
												blurOnSubmit={false}
												returnKeyType={'next'}
												ref={r => (this.refwebsite = r)}
												onSubmitEditing={() => this.refcr_no.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ website: text, Error_Text: '', error_filed: '' })
												}
											></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>
									{
										this.state.error_filed === 'website' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}
									<View style={{ flexDirection: 'row', alignItems: 'center', }}>
										<View style={{ width: '100%' }}>
											<FloatingLabelInput
												label="CR No."
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.cr_no}
												blurOnSubmit={false}
												returnKeyType={'next'}
												ref={r => (this.refcr_no = r)}
												onSubmitEditing={() => this.reftelephone.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ cr_no: text, Error_Text: '', error_filed: '' })
												}
											></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>
									{
										this.state.error_filed === 'cr_no' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}

									<View style={{ flexDirection: 'row', alignItems: 'center', }}>
										<View style={{ width: '100%' }}>
											<FloatingLabelInput
												label="Telephone no."
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												keyboardType="number-pad"
												value={this.state.telephone_no}
												returnKeyType={'next'}
												blurOnSubmit={false}
												ref={r => (this.reftelephone = r)}
												onSubmitEditing={() => this.refefax.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ telephone_no: text, Error_Text: '', error_filed: '' })
												}></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>
									{
										this.state.error_filed === 'telephone_no' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}

									<View style={{ flexDirection: 'row', alignItems: 'center', }}>
										<View style={{ width: '100%' }}>
											<FloatingLabelInput
												label="Fax No."
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.fax_no}
												returnKeyType={'next'}
												blurOnSubmit={false}
												ref={r => (this.refefax = r)}
												onSubmitEditing={() => this.refemail.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ fax_no: text, Error_Text: '', error_filed: '' })
												}></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>
									{
										this.state.error_filed === 'fax_no' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}
									<View style={{ flexDirection: 'row', alignItems: 'center' }}>
										<View style={{ width: '100%' }}>
											<FloatingLabelInput
												label="Company Email ID"
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.company_email}
												returnKeyType={'done'}
												blurOnSubmit={false}
												ref={r => (this.refemail = r)}
												onChangeText={text =>
													this.setState({ company_email: text, Error_Text: '', error_filed: '' })
												}></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>
									{
										this.state.error_filed === 'company_email' ?
											<Text style={{ color: 'red', paddingTop: 2 }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}

									{/* </View> */}
								</KeyboardAvoidingView>
							</ScrollView>
							<View style={{ flex: 1, justifyContent: 'flex-end', paddingTop: 10, }}>
								<View style={{ marginTop: 30 }}>
									{/* <TouchableOpacity onPress={() => this._OnSubmit()} >
										<CTA ctaText={'UPDATE DETAILS'} />
									</TouchableOpacity> */}
									<View style={{ flexDirection: 'row', width: '100%', marginTop: 30 }}>
										<TouchableOpacity onPress={() => Actions.pop()} style={{ width: '20%', backgroundColor: '#222223', height: 50, justifyContent: "center", alignItems: 'center' }}>
											<Back />
										</TouchableOpacity>
										<TouchableOpacity onPress={() => this._OnSubmit()} style={{ width: '75%', marginHorizontal: 20 }}>
											<CTA ctaText={'UPDATE DETAILS'} />
										</TouchableOpacity>
									</View>
								</View>
							</View>
						</View>
					</SafeAreaView>
				</LinearGradient>
			</Fragment>
		);
	}
}



const styles = StyleSheet.create({
	screen: {
		flex: 10,
		backgroundColor: 'transparent',
	},
	body: {
		flex: 10,
	},
	linearGradient: {
		flex: 1,
	},
	container: {
		flex: 1,
		paddingLeft: 20,
		paddingRight: 20
	},
	header: {
		color: '#FFFFFF',
		fontSize: 20,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
	},
	headerContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: 50
	},
	labelInput: {
		color: '#7D7D7D',
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
	},
	formInput: {
		fontSize: 18,
		borderBottomWidth: 1,
		borderColor: '#333',
		color: '#FFFFFF',
		fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
	},
	input: {
		borderWidth: 0,
		color: '#FFFFFF',
		fontSize: 14,
	},
})