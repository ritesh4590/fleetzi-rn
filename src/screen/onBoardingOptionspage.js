/**
 * This is the Home Screen
 **/

// React native and others libraries imports
import React, { Component, Fragment } from 'react';
import {
  Image,
  SafeAreaView,
  Alert,
  StyleSheet,
  Dimensions,
  ImageBackground,
  Platform,
  StatusBar,
  Text,
  TouchableOpacity,
  Modal,
  View,
  ScrollView,
  Button,
  TextInput,
  KeyboardAvoidingView,
  BackHandler,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import HeaderComp from '../component/HeaderComp';
import LinearGradient from 'react-native-linear-gradient';
import RNBootSplash from 'react-native-bootsplash';
import CreatePasswordSvg from '../component/svg/CreatePasswordSvg';
import BankDetailsSvg from '../component/svg/BankDetailsSvg';
import BusinessSvg from '../component/svg/BusinessSvg';
import VatSvg from '../component/svg/VatSvg';
import NewListingSvg from '../component/svg/NewListingSvg';
import InactiveSvg from '../component/svg/InactiveSvg';
import PhoneNoSvg from '../component/svg/PhoneNoSvg'
import { Col, Row, Grid } from 'react-native-easy-grid';
import CTA from '../component/CTA';

(screenWidth = Dimensions.get('window').width),
  (screenHeight = Dimensions.get('window').height);

var allMonths = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

export default class onBoardingOptionspage extends Component {
  constructor(props) {
    super(props);
    this.onHandleBackButton = this.handleBackButton.bind(this);
    this.state = {
      modalVisible: false,
      currentDate: new Date(),
      allYears: [],
      selectedMonth: 'January',
      selectedYear: 2020,
    };
  }


  handleBackButton() {
    Actions.pop()
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onHandleBackButton);
    RNBootSplash.hide({ duration: 250 });
    // this._retrieveUser();
    // TODO: You: Do firebase things
    // const { user } = await firebase.auth().signInAnonymously();
    // console.warn('User -> ', user.toJSON());
    let minOffset = 0,
      maxOffset = 10;
    let thisYear = new Date().getFullYear();
    let allYears = [];
    for (let x = 0; x <= maxOffset; x++) {
      allYears.push(thisYear - x);
    }
    this.setState({ allYears: allYears });
  }

  render() {
    return (
      <Fragment>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={{ backgroundColor: 'rgba(0,0,0,0.7)', flex: 1 }}>
            <Grid>
              <Row size={35}></Row>
              <Row size={25}>
                <View style={{ marginLeft: screenWidth / 2 }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setModalVisible(!this.state.modalVisible);
                    }}>
                    <Image
                      style={{ width: 50, height: 50 }}
                      source={require('../../assets/Images/crosicon.png')}
                    />
                  </TouchableOpacity>
                </View>
              </Row>
              <Row
                size={40}
                style={{ justifyContent: 'center', alignItems: 'center' }}>
                {/* <View style={{marginLeft:'20%', marginRight:'20%'}}> */}
                <Col>
                  <ScrollView>
                    {allMonths.map(data => {
                      return (
                        <View key={data} style={{ flexDirection: 'row' }}>
                          <Col size={20} style={{ paddingLeft: screenWidth * 0.08, alignItems: 'center', justifyContent: 'center' }}>

                            {this.state.selectedMonth === data ? (
                              <View style={{ height: 10, width: 10, backgroundColor: '#FFAC00', borderRadius: 10, }}></View>
                            ) : (
                                <View></View>
                              )}
                          </Col>
                          <Col size={80} style={{ justifyContent: 'center', alignItems: 'center' }}  >
                            <View style={{ marginTop: screenHeight * .03 }}>
                              <Text style={styles.yeartext}>{data}</Text>
                            </View>
                          </Col>
                        </View>
                      );
                    })}
                  </ScrollView>
                </Col>
                <Col>
                  <ScrollView>
                    {this.state.allYears.map(data => {
                      return (
                        <View key={data} style={{ flexDirection: 'row' }}>
                          <Col size={20} style={{ alignItems: 'center', justifyContent: 'center', paddingLeft: 20, }}>

                            {this.state.selectedYear === data ? (
                              <View style={{
                                height: 10, width: 10, backgroundColor: '#FFAC00', borderRadius: 10,
                              }}></View>
                            ) : (
                                <View></View>
                              )}
                          </Col>
                          <Col
                            size={80}
                            style={{ justifyContent: 'center', alignItems: 'center' }} >
                            <View style={{ marginTop: screenHeight * .03 }}>
                              <Text style={styles.yeartext}>{data}</Text>
                            </View>
                          </Col>
                        </View>
                      );
                    })}
                  </ScrollView>
                </Col>
              </Row>
            </Grid>
          </View>
        </Modal>
        <LinearGradient
          colors={['#323232', '#14181A', '#000000']}
          style={styles.linearGradient}
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 1 }}>
          <StatusBar translucent backgroundColor="transparent" />
          <SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
          <SafeAreaView style={styles.screen}>
            <View style={styles.body}>
              {/* <HeaderComp /> */}
              <ScrollView>
                <View style={styles.bodyMainView}>
                  <View>
                    <Text style={styles.welcomeText}>
                      welcome! {'\n'}aboard
                    </Text>
                  </View>
                  <View style={styles.optionsView}>
                    <TouchableOpacity style={styles.passwordView} onPress={() => Actions.createlisting()}>
                      <NewListingSvg />
                      <Text style={styles.passwordText}>
                        Create new listing
                      </Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity
                      style={styles.passwordView}
                      onPress={() => Actions.onBoardingCreateNew()}>
                      <CreatePasswordSvg />
                      <Text style={styles.passwordText}>Create a password</Text>
                    </TouchableOpacity> */}
                    <TouchableOpacity
                      style={styles.passwordView}
                      onPress={() => {
                        // this.setModalVisible(true);
                      }}>
                      <BankDetailsSvg />
                      <Text style={styles.passwordText}>Upload bank details</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.passwordView}>
                      <BusinessSvg />
                      <Text style={styles.passwordText}>Upload business certificate</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.passwordView}>
                      <VatSvg />
                      <Text style={styles.passwordText}>Upload VAT ID</Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity style={styles.passwordView} onPress={() => Actions.createlisting()}>
                      <NewListingSvg />
                      <Text style={styles.passwordText}>
                        Create new listing
                      </Text>
                    </TouchableOpacity> */}
                  </View>
                  <ImageBackground style={styles.backgroundImageSty} resizeMode='stretch' source={require('../../assets/Images/Rectangle.png')}>
                    <View style={styles.representativeViewMain}>
                      {/* <View style={styles.do_you_know_text}>
              <Text style={styles.small_text_do}>
                A Fleetzi partner manager will call you shortly for activating your account.
            </Text>
            </View> */}

                      <View style={styles.representativeView}>
                        <View style={{ width: '60%' }}>
                          <Text style={styles.representativeManager} >Partner Manager</Text>
                          <Text style={styles.representativeName} >Mr. Abdullah Kareem  </Text>
                        </View>
                        <View style={{}} >
                          <Image source={require('../../assets/Images/ankit.jpeg')} style={styles.cirularImage} />
                        </View>
                      </View>
                      <View style={{ marginTop: screenHeight * .04 }}>
                        <View style={{}}>
                          <Text style={styles.representativeManager}>Phone Number</Text>
                          <View style={{ flexDirection: 'row', alignItems: "center" }}>
                            <Text style={styles.representativeName}>55 335 6988</Text>
                            <View style={{ marginLeft: 10 }}>
                              <PhoneNoSvg />
                            </View>
                          </View>
                        </View>
                      </View>
                      <View style={styles.contractSigningView}>
                        <Text style={styles.contractSigning} >Account Status</Text>
                        <InactiveSvg />
                      </View>
                      {/*  */}

                      {/*  */}
                    </View>
                  </ImageBackground>

                  {/* <ImageBackground
                    resizeMode="stretch"
                    style={styles.backgroungImageSty}
                    source={require('../../assets/Images/Small-Rectangle.png')}>
                    <View style={styles.statusView}>
                      <Text style={styles.statusText}>Account Status</Text>
                      <InactiveSvg />
                    </View>
                    <View style={styles.zicaptinView}>
                    <Text style={styles.representativeManager} >Partner Manager</Text>
                    <Text style={styles.representativeName} >Mr. Abdullah Kareem  </Text>
                    </View>
                  </ImageBackground> */}
                </View>
              </ScrollView>
              <TouchableOpacity onPress={() => Actions.pop()} style={{position:'absolute',bottom:0,left:20,right:20}}>
                <CTA ctaText={'CONTINUE'} />
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        </LinearGradient>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 10,
    backgroundColor: 'transparent',
  },
  body: {
    flex: 10,
  },
  linearGradient: {
    flex: 1,
  },

  bodyMainView: {
    marginTop: screenHeight * 0.1,
    paddingLeft: 20,
    paddingRight: 20,
  },
  welcomeText: {
    color: '#3C3C3C',
    fontSize: 50,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
  },
  optionsView: {
    marginTop: screenHeight * 0.03,
  },
  passwordView: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: screenHeight * .015,

  },
  passwordText: {
    color: '#FFFFFF',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
    marginHorizontal: screenWidth * 0.04,
    // textAlign: 'left',
    margin: 5,
    fontSize: 18,
  },
  statusView: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: screenWidth * .05,
    justifyContent: 'space-between',
    // borderColor:'red',
    // borderWidth:1,
    height: 70
  },
  statusText: {
    color: 'rgba(255, 255, 255, 0.400896)',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },

  yeartext: {
    color: '#FFFFFF',
    fontSize: 22,
    paddingBottom: screenHeight * .03,
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },
  representativeManager: {
    color: '#FF9800',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },
  representativeName: {
    fontSize: 16,
    marginTop: 5,
    color: '#FFFFFF',
    letterSpacing: .1,
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },

  backgroundImageSty: {
    width: "100%",
    marginTop: 20
  },
  representativeViewMain: {
    padding: 20,
    paddingTop: 0
  },
  representativeView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 15
  },
  representativeManager: {
    color: '#FF9800',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },
  representativeName: {
    fontSize: 16,
    marginTop: 5,
    color: '#FFFFFF',
    letterSpacing: .1,
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },
  cirularImage: {
    height: 50,
    width: 50,
    borderRadius: 25
  },
  contractSigningView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: screenHeight * .015,
    justifyContent: 'space-between'
  },
  contractSigning: {
    color: '#FFFFFF',
    opacity: .3,
    fontSize: 14,
    marginRight: 15,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',

  },
});
