/**
* This is the Home Screen
**/

// React native and others libraries imports
import React, { Component, Fragment } from 'react';
import { Image, SafeAreaView, Alert, StyleSheet, Dimensions, Linking, Platform, StatusBar, Text, TouchableOpacity, ImageBackground, View, ScrollView, Button, TextInput, KeyboardAvoidingView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import Header from '../component/Header'
import Footer from '../component/Footer'
import FlatList from '../component/FlatList'
import RNBootSplash from "react-native-bootsplash";
import HeaderComp from "../component/HeaderComp"
import DashboardTwo from "../component/DashboardTwo"
import HomeFooter from '../component/svg/Home'
import Payments from '../component/svg/Payments'
import ListingSVG from '../component/svg/Listing'
import Order from '../component/svg/Order'
import LinearGradient from 'react-native-linear-gradient';
import NewOrder from '../component/NewOrder'
import Back from '../component/svg/back'
import Payment from '../component/Payment'
import DoneComp from '../component/svg/Done'
import ProcessingComp from '../component/svg/Processing'
import PendingComp from '../component/svg/Pending'

screenWidth = Dimensions.get('window').width,
	screenHeight = Dimensions.get('window').height
export default class onBoardingVerification extends Component {

	constructor(props) {
		super(props);
		this.state = {
		  isLoading: false,
		  ACCESSTOKEN:''
	
		};
	}


	componentDidMount() {
		RNBootSplash.hide({ duration: 250 });
		this._retrieveAccesstoken();
		
	}

	_retrieveAccesstoken = async () => {
		try {
		  const value = await AsyncStorage.getItem('ACCESSTOKEN');
		  if (value !== null) {
			// We have data!!
			var eventstring = new String();
			eventstring = value.replace(/"/g, "");
			this.setState({ACCESSTOKEN:eventstring})
			this._retrieveRMData()
			console.log(eventstring);
		  }
		} catch (error) {
		  // Error retrieving data
		}
	};

	_retrieveRMData() {
		fetch("http://34.87.55.15/api/v1/token/login/user/?format=json", {
		method: 'GET',
		headers: {
			'Authorization':'Token '+this.state.ACCESSTOKEN,
			'Content-Type': 'multipart/form-data',
		},
		})
		.then((response) => response.json())
		.then((responseJson) => {
		console.log(responseJson)
		// if (responseJson.status == 'success') {
		// 	this.setState({isLoading: false});
		// 	Actions.managelisting()
		// } else {
		// 	this.setState({isLoading: false});
		// 	console.log("somthing error");
		// }
		})
		.catch((error) => {
		console.error(error);
		console.log("somthing error");
		this.setState({isLoading: false});
		});
	}



	render() {
		return (
			<Fragment>
				<LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{ x: 0, y: 0 }}
					end={{ x: 1, y: 1 }}>
					<StatusBar translucent backgroundColor="transparent" />
					<SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
					<SafeAreaView style={styles.screen}>

						<ScrollView style={styles.body}>
							<HeaderComp />
							<View style={styles.upperTextView}>
								<Text style={styles.congoText}>congratulations!{'\n'}account created</Text>
								<View style={styles.BottomTextView}>								
								<Text style={styles.smallText}>You can start using fleet
									<Text style={styles.smallcoloredText}>zi</Text>
									<Text style={styles.smallText}> in 3 simple steps.</Text>
								</Text>
								</View>
							</View>

							<View style={styles.outerView}>
								<ImageBackground style={styles.backgroundImageSty} resizeMode='stretch' source={require('../../assets/Images/Rectangle.png')}>
								 
								<View style={styles.representativeViewMain}>
										<View style={styles.representativeView}>
											<Text style={styles.representativeText} >Representative Verification</Text>
											<DoneComp/>
										</View>
										 
										 <View style={{ marginTop: screenHeight * .02, }} >
											<Image source={require('../../assets/Images/ankit.jpeg')} style={styles.cirularImage} />
										</View>
										<View style={{ marginTop: screenHeight * .04 }}>
											<View style={{ flexDirection: 'row', }}>
												<Text style={styles.title}>Your representative</Text>
												<Text style={styles.representativeName}>Mr. Abdullah Kareem   </Text>
											</View>
											<View style={{ flexDirection: 'row', paddingTop: 5 }}>
												<Text style={styles.title}>Phone no.</Text>
												<Text style={styles.representativePhone}>555555555</Text>
											</View>
										</View>
									 	<View style={styles.horizontalLine}></View>
										<View style={styles.contractSigningView}>
											<Text style={styles.contractSigning} >Contract Signing</Text>
											<ProcessingComp />
										</View>
										<View style={styles.horizontalLine}></View>
										<View style={styles.contractSigningView}>
											<Text style={styles.contractSigning} >Submit Deposit</Text>
											<PendingComp />
										</View>
								</View>  
									  
								</ImageBackground>
								<TouchableOpacity onPress={() => Actions.onBoardingOptionspage()} style={{width:'50%'}}>
                    <LinearGradient
                      colors={['#FFAC00', '#FF8B00']}
                      style={styles.ButtonView}
                      start={{x: 0, y: 0}}
                      end={{x: 1, y: 1}}>
                      <Text style={styles.buttomText}>Continue</Text>
                    </LinearGradient>
                  </TouchableOpacity>
								</View>  
					 

						</ScrollView>


					</SafeAreaView>
				</LinearGradient>
			</Fragment>
		);
	}


}

const styles = StyleSheet.create({
	screen: {
		flex: 10,
		backgroundColor: 'transparent'
	},
	body: {
		flex: 10,
	},
	linearGradient: {
		flex: 1
	},
	upperTextView: {
		marginTop:-(screenHeight*.05),
		paddingLeft: 20,
		paddingRight: 20,
		textAlign:'center',
		justifyContent:'center',
		alignItems:'center'
	},
	BottomTextView:{
		width:'100%',
		paddingLeft:3,
		marginTop:screenHeight*.015,
	},
	congoText: {
		width:'100%',
		letterSpacing:.73,
		color: '#3C3C3C',
		lineHeight:50,
		fontSize: 40,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',

	},
	smallText: {
		color: '#FFFFFF',
		fontSize: 14,
		fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
		letterSpacing: .25,
		fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',

	},
	smallcoloredText: {
		color: '#FF9800',
		fontSize: 14,
		letterSpacing: .25,
		fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
	},
	representativeView: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	contractSigningView:{
		flexDirection:'row',
		alignItems: 'center',
		marginTop:screenHeight*.015
	},
	representativeText: {
		color: '#FFFFFF',
		fontSize: 14,
		marginRight: 15,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',

	},
	contractSigning:{
		color: '#FFFFFF',
		opacity:.3,
		fontSize: 14,
		marginRight: 15,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',

	},
	representativeViewMain:{
		padding:15
	},
	title: {
		width: '55%',
		fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
		color: 'rgba(255, 255, 255, 0.400896)'
	},
	representativeName: {
		width: '45%',
		color: '#FFFFFF',
		fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',

	},
	representativePhone: {
		width: '45%',
		color: '#FF9800',
		fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
	},
	horizontalLine: {
		borderWidth: 1,
		borderColor: '#2C2C2C',
		marginTop: screenHeight * .015
	},
	outerView: {
		marginTop:screenHeight*.04,
		paddingRight: 20,
		paddingLeft: 20,
	},
	
	backgroundImageSty: {
		width: "100%",
		// height: '100%',
	},
	cirularImage: {
		height: 50,
		width: 50,
		borderRadius: 25
	},
	ButtonView: {
		backgroundColor: '#FF8B00',
		width: '100%',
		height: 50,
		justifyContent: 'center',
		alignItems: 'center',
		borderBottomColor: '#FFEED9',
		borderBottomWidth: 5,
	  },
	  buttomText: {
		color: '#261400',
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
		fontSize: 15,
		marginTop: screenheight * 0.005,
	  },
});