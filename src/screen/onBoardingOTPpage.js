/**
 * This is the Home Screen
 **/

// React native and others libraries imports
import React, { Component, Fragment } from 'react';
import {
  Image,
  SafeAreaView,
  Alert,
  StyleSheet,
  Dimensions,
  Linking,
  Platform,
  StatusBar,
  Text,
  TouchableOpacity,
  PixelRatio,
  View,
  ScrollView,
  Button,
  TextInput,
  KeyboardAvoidingView,
  Modal,
  TouchableHighlight
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import RNBootSplash from 'react-native-bootsplash';
import HeaderComp from '../component/HeaderComp';
import LinearGradient from 'react-native-linear-gradient';
// import FloatingLabel from 'react-native-floating-labels';
import CTA from '../component/CTA'
import Back from '../component/svg/back'
import IncorrectOTP from '../component/svg/incorrectOTP'
import FloatingLabelInput from '../component/FloatingLabelInput';
import API from '../utils/API'
import RNOtpVerify from 'react-native-otp-verify';

(widthDim = Dimensions.get('window').width),
  (heightDim = Dimensions.get('window').height);

  function splitToDigit(n){
    return [...n + ''].map(String)
  }

export default class onBoardingOtp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneotp1: '',
      phoneotp2: '',
      phoneotp3: '',
      phoneotp4: '',
      phoneotp5: '',
      emailotp: '',
      country_code: '+91',
      number: '8860653763',
      email: '',
      hash: '',
      device_id: '',
      timer: 30,
      resendotp: false,
      hasError: true,
      errorText: '',
      Alert_Visibility: false,
      phoneotp1Focus: false,
      phoneotp2Focus: false,
      phoneotp3Focus: false,
      phoneotp4Focus: false,
      phoneotp5Focus: false,
    };
  }

  componentDidMount() {
    this.RunTimer();
    this.getHash();
    this.startListeningForOtp();
    this.setState({
      country_code: this.props.country_code,
      number: this.props.number,
      hash: this.props.hash,
      email: this.props.email,
      device_id: '',
      phoneotp: '',
      emailotp: '',
    });
  }

  componentDidUpdate() {
    if (this.state.timer === 1) {
      this.setState({ timer: 30, resendotp: true });
      clearInterval(this.interval);
    }
  }


  getHash = () =>
    RNOtpVerify.getHash()
    .then(console.log)
    .catch(console.log);

    startListeningForOtp = () =>
    RNOtpVerify.getOtp()
    .then(p => RNOtpVerify.addListener(this.otpHandler))
    .catch(p => console.log(p));

 otpHandler = (message: string) => {
        console.log(message)
        const otp = splitToDigit(/(\d{4})/g.exec(message)[1]);
        console.log(otp)
        this.setState({
          phoneotp1:otp[0],
          phoneotp2:otp[1],
          phoneotp3:otp[2],
          phoneotp4:otp[3],
        })
        RNOtpVerify.removeListener();
        // Keyboard.dismiss();
}

componentWillUnmount() {
  RNOtpVerify.removeListener();
}

  setModalVisible(visible) {
    this.setState({ makeyearmodalVisible: visible });
  }


  RunTimer() {
    this.interval = setInterval(
      () => this.setState(prevState => ({ timer: prevState.timer - 1 })),
      1000,
    );
  }
  onFocus(input) {
    if (input === 'phoneotp1Focus') {
      this.setState({ phoneotp1Focus: true, phoneotp2Focus: false, phoneotp3Focus: false, phoneotp4Focus: false, phoneotp5Focus: false });
    }
    if (input === 'phoneotp2Focus') {
      this.setState({ phoneotp1Focus: false, phoneotp2Focus: true, phoneotp3Focus: false, phoneotp4Focus: false, phoneotp5Focus: false });
    }
    if (input === 'phoneotp3Focus') {
      this.setState({ phoneotp1Focus: false, phoneotp2Focus: false, phoneotp3Focus: true, phoneotp4Focus: false, phoneotp5Focus: false });
    }
    if (input === 'phoneotp4Focus') {
      this.setState({ phoneotp1Focus: false, phoneotp2Focus: false, phoneotp3Focus: false, phoneotp4Focus: true, phoneotp5Focus: false });
    }
    if (input === 'phoneotp5Focus') {
      this.setState({ phoneotp1Focus: false, phoneotp2Focus: false, phoneotp3Focus: false, phoneotp4Focus: false, phoneotp5Focus: true });
    }
  }

  // Change the Input Focus 
  _ChangeFocus(input){
    if (input === 'phoneotp1Focus') {
      this.phoneotp2Focus.getInnerRef().focus()
    }
    if (input === 'phoneotp2Focus') {
      this.phoneotp3Focus.getInnerRef().focus()
    }
    if (input === 'phoneotp3Focus') {
      this.phoneotp4Focus.getInnerRef().focus()
    }
    if (input === 'phoneotp4Focus') {
      this.phoneotp5Focus.getInnerRef().focus()
    }
  }

  // Chnage the Input Focus Onchange Text for veryfication code

  _OnChangeText(position, value) {
    if (position === 'phoneotp1Focus') {
      if (value) {
        this.setState({phoneotp1:value})
        this.phoneotp2Focus.getInnerRef().focus()
      } else {
        this.setState({phoneotp1:''})
      }
    }
    if (position === 'phoneotp2Focus') {
      if (value) {
        this.setState({phoneotp2:value})
        this.phoneotp3Focus.getInnerRef().focus()  
      } else {
        this.setState({phoneotp2:''})
        this.phoneotp1Focus.getInnerRef().focus()  
      }
    }
    if (position === 'phoneotp3Focus') {
      if (value) {
        this.setState({phoneotp3:value})
        this.phoneotp4Focus.getInnerRef().focus()
      } else {
        this.setState({phoneotp3:''})
        this.phoneotp2Focus.getInnerRef().focus()  
      }
    }
    if (position === 'phoneotp4Focus') {
      if (value) {
        this.setState({phoneotp4:value})
        this.phoneotp5Focus.getInnerRef().focus()
      } else {
        this.setState({phoneotp4:''})
        this.phoneotp3Focus.getInnerRef().focus()  
      }
    }
    if (position === 'phoneotp5Focus') {
      if (value) {
        this.setState({phoneotp5:value})
        this.phoneotp5Focus.getInnerRef().focus()
      } else {
        this.setState({phoneotp5:''})
        this.phoneotp4Focus.getInnerRef().focus()  
      }

    }
  }

  // Resend OTP Function

  get_otp() {
    if (!!this.state.number && this.state.country_code) {
      this.RunTimer();
      this.setState({ isLoading: true, resendotp:false, Alert_Visibility: false  });
      let formdata = new FormData();
      formdata.append('country_code', this.state.country_code);
      formdata.append('number', this.state.number);
      formdata.append('platform', 'part');
      fetch(
        'http://api.fleetzi.com/v1/token/login/otp/request-sms/?format=json',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'multipart/form-data',
          },
          body: formdata,
        },
      )
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.status == 'success') {
            this.setState({
              isLoading: false,
              hash: responseJson.data.hash,
            });
          } else {
            this.setState({ isLoading: false });
            console.log('somthing error');
          }
        })
        .catch(error => {
          console.error(error);
          console.log('somthing error');
          this.setState({ isLoading: false });
        });
    } else {
      this.setState({
        isLoading: false,
        hasError: true,
        errorText: 'server error',
      });
    }
  }


  veryfy_otp() {
    if (!!this.state.phoneotp1 && this.state.phoneotp2 && this.state.phoneotp3 && this.state.phoneotp4 && this.state.phoneotp5) {
      this.setState({ isLoading: true });
      let formdata = new FormData();
      formdata.append('country_code', this.state.country_code);
      formdata.append('number', this.state.number);
      formdata.append('hash', this.state.hash);
      formdata.append('otp', this.state.phoneotp1 + this.state.phoneotp2 + this.state.phoneotp3 + this.state.phoneotp4 + this.state.phoneotp5);
      console.log(formdata);

      fetch('http://api.fleetzi.com/v1/token/login/otp/verify/?format=json', {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formdata,
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log(responseJson);
          if (responseJson.status == 'success') {
            AsyncStorage.setItem('USERDATA', JSON.stringify(responseJson.data));
            AsyncStorage.setItem(
              'ACCESSTOKEN',
              JSON.stringify(responseJson.data.access_token),
            );
            this.setState({ isLoading: false });
            Actions.active();
          } else {
            this.setState({ isLoading: false, Alert_Visibility: true  });

            // Alert.alert(responseJson.message);
            console.log('somthing error');
          }
        })
        .catch(error => {
          console.error(error);
          console.log('somthing error');
          this.setState({ isLoading: false });
        });
    } else {
      this.setState({
        isLoading: false,
        hasError: true,
        errorText: 'Please enter OTP',
      });
    }
  }

  render() {
    return (
      <Fragment>
        <LinearGradient
          colors={['#323232', '#14181A', '#000000']}
          style={styles.linearGradient}
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 1 }}>
          <StatusBar translucent backgroundColor="transparent" />
          <SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
          <SafeAreaView style={styles.screen}>
            <View style={styles.body}>
              <HeaderComp />
              <View style={{ paddingRight: 20, paddingLeft: 20 }}>
                <Text
                  style={{
                    fontFamily:
                      Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
                    color: 'white',
                    fontSize: 30,
                    paddingBottom: 10,
                  }}>
                  enter SMS verification code to continue
                </Text>

                <View style={{}}>
                  {/* <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    style={[styles.formInput, {
                      borderColor: this.state.phoneInputFouse
                        ? '#FFFFFF'
                        : '#333',
                    }]}
                    value={this.state.emailotp}
                    keyboardType="number-pad"
                    onFocus={() => this.onFocus('phoneInputFouse')}
                    onChangeText={text => this.setState({ emailotp: text })}>
                    Send to {this.state.email}
                  </FloatingLabel> */}
                  {/* <View></View> */}
                  <Text style={{
                    color: '#7D7D7D', fontSize: 18, marginTop: 20, fontFamily:
                      Platform.OS === 'ios'
                        ? 'NexaRegular'
                        : 'Nexa Regular',
                  }}>
                    Sent to {this.state.country_code}-{this.state.number}
                  </Text>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                    <FloatingLabelInput
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={[styles.formInput, {
                        borderColor: this.state.phoneotp1Focus
                          ? '#FFFFFF'
                          : '#333',
                      }]}
                      keyboardType="number-pad"
                      maxLength={1}
                      value={this.state.phoneotp1}
                      returnKeyType={'next'}
                      onSubmitEditing={() => this._ChangeFocus('phoneotp1Focus')}
                      ref={r => (this.phoneotp1Focus = r)}
                      onFocus={() => this.onFocus('phoneotp1Focus')}
                      onChangeText={text => this._OnChangeText('phoneotp1Focus', text )}>
                      {/* Sent to {this.state.country_code}-{this.state.number} */}
                    </FloatingLabelInput>
                    <FloatingLabelInput
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={[styles.formInput, {
                        borderColor: this.state.phoneotp2Focus
                          ? '#FFFFFF'
                          : '#333',
                      }]}
                      maxLength={1}
                      keyboardType="number-pad"
                      value={this.state.phoneotp2}
                      returnKeyType={'next'}
                      onSubmitEditing={() => this._ChangeFocus('phoneotp2Focus')}
                      ref={r => (this.phoneotp2Focus = r)}
                      onFocus={() => this.onFocus('phoneotp2Focus')}
                      onChangeText={text => this._OnChangeText('phoneotp2Focus', text )}>
                      {/* Sent to {this.state.country_code}-{this.state.number} */}
                    </FloatingLabelInput>
                    <FloatingLabelInput
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={[styles.formInput, {
                        borderColor: this.state.phoneotp3Focus
                          ? '#FFFFFF'
                          : '#333',
                      }]}
                      maxLength={1}
                      keyboardType="number-pad"
                      value={this.state.phoneotp3}
                      returnKeyType={'next'}
                      onSubmitEditing={() => this._ChangeFocus('phoneotp3Focus')}
                      ref={r => (this.phoneotp3Focus = r)}
                      onFocus={() => this.onFocus('phoneotp3Focus')}
                      onChangeText={text => this._OnChangeText('phoneotp3Focus', text )}>
                      {/* Sent to {this.state.country_code}-{this.state.number} */}
                    </FloatingLabelInput>
                    <FloatingLabelInput
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={[styles.formInput, {
                        borderColor: this.state.phoneotp4Focus
                          ? '#FFFFFF'
                          : '#333',
                      }]}
                      maxLength={1}
                      keyboardType="number-pad"
                      value={this.state.phoneotp4}
                      returnKeyType={'next'}
                      onSubmitEditing={() => this._ChangeFocus('phoneotp4Focus')}
                      ref={r => (this.phoneotp4Focus = r)}
                      onFocus={() => this.onFocus('phoneotp4Focus')}
                      onChangeText={text => this._OnChangeText('phoneotp4Focus', text )}>
                      {/* Sent to {this.state.country_code}-{this.state.number} */}
                    </FloatingLabelInput>
                    <FloatingLabelInput
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      maxLength={1}
                      style={[styles.formInput, {
                        borderColor: this.state.phoneotp5Focus
                          ? '#FFFFFF'
                          : '#333',
                      }]}
                      keyboardType="number-pad"
                      value={this.state.phoneotp5}
                      returnKeyType={'done'}
                      // onSubmitEditing={() => this._ChangeFocus('phoneotp3Focus')}
                      ref={r => (this.phoneotp5Focus = r)}
                      onFocus={() => this.onFocus('phoneotp5Focus')}
                      onChangeText={text => this._OnChangeText('phoneotp5Focus', text )}>
                      {/* Sent to {this.state.country_code}-{this.state.number} */}
                    </FloatingLabelInput>
                  </View>
                  {this.state.hasError ? (
                    <Text style={{ paddingTop: 10, color: 'red' }}>
                      {this.state.errorText}
                    </Text>
                  ) : (
                      <Text></Text>
                    )}
                </View>
                <View
                  style={{
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    height: 50,
                    alignItems: 'center'
                  }}>
                  <Text
                    style={{
                      fontFamily:
                        Platform.OS === 'ios'
                          ? 'NexaRegular'
                          : 'Nexa Regular',
                      color: '#FF9800',
                    }}>
                    00:{this.state.timer}
                  </Text>
                  {this.state.resendotp ? (
                    <TouchableOpacity onPress={() => this.get_otp()}>
                      <Text
                        style={{
                          fontFamily:
                            Platform.OS === 'ios'
                              ? 'NexaRegular'
                              : 'Nexa Regular',
                          color: '#FFFFFF',
                        }}>
                        Resend code
                        </Text>
                    </TouchableOpacity>
                  ) : (
                      <Text
                        style={{
                          fontFamily:
                            Platform.OS === 'ios'
                              ? 'NexaRegular'
                              : 'Nexa Regular',
                          color: '#333',
                        }}>
                        Resend code
                      </Text>
                    )}
                </View>

                {/* <View style={{ marginTop:20, justifyContent: 'flex-end', paddingBottom: 20 }}> */}
                <View style={{ flexDirection: 'row', width: '100%', marginTop: 30 }}>
                  <TouchableOpacity onPress={() => Actions.pop()} style={{ width: '20%', backgroundColor: '#222223', height: 50, justifyContent: "center", alignItems: 'center' }}>
                    <Back />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.veryfy_otp()} style={{ width: '75%', marginHorizontal: 20 }}>
                    <CTA ctaText={'VERIFY & LOGIN'} />
                  </TouchableOpacity>
                </View>

              </View>
              <Modal
                visible={this.state.Alert_Visibility}
                transparent={true}
                animationType={"fade"}
                onRequestClose={() => {
                  this.setState({ Alert_Visibility: false })
                }} >

                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.7)' }}>

                  <View style={styles.MainAlertView}>
                    <View style={{ width: '60%', marginBottom: 20, flexDirection: 'row', justifyContent: 'space-between', }} >
                      <IncorrectOTP />
                      <Text style={{ color: '#000', fontSize: 20, fontWeight: '700' }}>Incorrect OTP</Text>
                    </View>

                    <Text style={styles.AlertMessage}>the OTP you entered is incorrect Please try again or request a new otp via resend</Text>

                    <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'flex-end', marginTop: 10 }}>
                      <TouchableOpacity onPress={() => this.get_otp()}>
                        <Text style={{
                          fontWeight: '700', fontSize: 16,
                          fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
                        }}>
                          Resend
                      </Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={{ marginHorizontal: 30 }}
                        onPress={() => {
                          this.setState({ Alert_Visibility: false })
                        }}>
                        <Text style={{
                          fontWeight: '700', fontSize: 16, color: '#8ccb33',
                          fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
                        }} >
                          Try Again
                      </Text>
                      </TouchableOpacity>
                    </View>
                    {/* 
                    <TouchableHighlight
                      onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                      }}>
                      <Text>Hide Modal</Text>
                    </TouchableHighlight> */}
                    {/* <TouchableOpacity onPress={() => this.alertaction()} activeOpacity={1} style={{ position: 'absolute', bottom: -20, left: 20, right: 20 }}  >
                      <CTA ctaText={'VIEW ALL LISTINGS'} />
                    </TouchableOpacity> */}


                  </View>
                </View>
              </Modal>




            </View>
          </SafeAreaView>
        </LinearGradient>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 10,
    backgroundColor: 'transparent',
  },
  body: {
    flex: 10,
  },
  linearGradient: {
    flex: 1,
  },
  ButtonView: {
    backgroundColor: '#FF8B00',
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#FFEED9',
    borderBottomWidth: 5,
  },
  buttomText: {
    color: '#261400',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
    fontSize: 13,
    marginTop: screenheight * 0.005,
  },
  labelInput: {
    color: '#7D7D7D',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
  },
  formInput: {
    borderBottomWidth: 1,
    width: '15%',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },
  input: {
    borderWidth: 0,
    color: '#FFFFFF',
    fontSize: 14,
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',

  },
  enterOtp: {
    color: 'white',
    fontSize: 30,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
  },
  MainAlertView: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#FFFFFF",
    height: 200,
    width: '80%',
    borderRadius: 10,
    // borderColor:'red',
    // borderWidth:1
  },
  AlertTitle: {
    fontSize: 18,
    color: "#5ECA94",
    textAlign: 'center',
    padding: 15,
    height: '22%',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular'
  },
  AlertMessage: {
    fontSize: 12,
    color: "#999999",
    textAlign: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    height: '40%',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
    lineHeight: 20,

  },
});
