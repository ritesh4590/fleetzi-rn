/**
* This is the Home Screen
**/

// React native and others libraries imports
import React, { Component } from 'react';
import { Image, SafeAreaView, Alert, StyleSheet, Dimensions, Linking, Platform, ScrollView, Text, FlatList, TouchableOpacity, PixelRatio, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import Header from '../component/Header'

export default class Detail extends Component {
  
  render() {
      console.log("fcm token")
    return (
        <SafeAreaView style={styles.screen}>
            <Header text='Title/Logo'>
                <Text style={{color:'red'}}>Hellop</Text>
            </Header>
            <View style={styles.body}>
            <Text>Detail screen</Text>
            </View>
        </SafeAreaView>
    );
  }


}

const styles = StyleSheet.create({
    screen: {
      flex: 10
    },
    body: {
      flex: 10
    }
  });