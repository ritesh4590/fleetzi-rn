/**
* This is the Home Screen
**/

// React native and others libraries imports
import React, { Component, Fragment } from 'react';
import { Image, SafeAreaView, Alert, StyleSheet, Dimensions, Linking, Platform, StatusBar, Text, TouchableOpacity, PixelRatio, View,ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import Header from '../component/Header'
import Footer from '../component/Footer'
import FlatList from '../component/FlatList'
import RNBootSplash from "react-native-bootsplash";
import HeaderComp from "../component/HeaderComp"
import DashboardTwo from "../component/DashboardTwo"
import HomeFooter from '../component/svg/Home'
import Payments from '../component/svg/Payments'
import ListingSVG from '../component/svg/Listing'
import Order from '../component/svg/Order'
import LinearGradient from 'react-native-linear-gradient';
import NewOrder from '../component/NewOrder'
import Back from '../component/svg/back'
import Payment from '../component/Payment'
export default class Home extends Component {
  

  async componentDidMount() {
    RNBootSplash.hide({ duration: 250 });
    // this._retrieveUser();
    // TODO: You: Do firebase things
    // const { user } = await firebase.auth().signInAnonymously();
    // console.warn('User -> ', user.toJSON());
    this.checkPermission();
    this.createNotificationListeners(); //add this line
  }


  ////////////////////// Add these methods //////////////////////
  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  //3
  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    this.setState({ fcmToken: fcmToken })
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // console.log("=================================================");
        // console.log(fcmToken);
        this.setState({ fcmToken: fcmToken })
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }


  //Remove listeners allocated in createNotificationListeners()
  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
    Linking.removeEventListener('url', this.handleOpenURL);
  }

  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const { title, body } = notification;
      // this.showAlert(title, body);
    });

    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      // console.log("app is in background")
      const { title, body } = notificationOpen.notification;
      // this.showAlert(title, body);
    });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    const data = Platform.OS === 'android' ? notificationOpen.notification.data.customDataKeyName : notificationOpen.customDataKeyName;
    if (notificationOpen) {
      const notification = notificationOpen.notification;
      // console.log("ye han actinon")
      console.log(notification)
    }
    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      // console.log("app is in payload ")
      // console.log(JSON.stringify(message));
    });
  }
  // handel URL Deeplinking
  handleOpenURL(event) {
    this.navigate(event.url);
  }

  // Handel Deeplink navigation
  navigate = (url) => { // E
    if (url) {
      const route = url.replace(/.*?:\/\//g, '');
      console.log(route)
    }
  }

  showAlert(title, body) {
    Alert.alert(
      title, body,
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false },
    );
  }

  render() {
    return (
      <Fragment>
         <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{x:0, y: 0}}
          end={{x: 1, y: 1}}>
        <StatusBar translucent backgroundColor="transparent" />
        <SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
        <SafeAreaView style={styles.screen}>
            
            <View style={styles.body}>
              
             <HeaderComp />  
                <DashboardTwo />
            </View>
            {/* <Footer index='home'/> */}
                
        </SafeAreaView>
        </LinearGradient>
        </Fragment>
    );
  }


}

const styles = StyleSheet.create({
    screen: {
      flex: 10,
      backgroundColor:'transparent'
    },
    body: {
      flex: 10,
    },
    linearGradient:{
      flex:1
      },
      iconBox:{
        backgroundColor:'#1A1D1C',
        height:80,
        width:70,
        paddingTop:15,
        borderRadius:5
      }
  });