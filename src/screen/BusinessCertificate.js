import React, { Component, Fragment } from 'react';
import {
	Image,
	SafeAreaView,
	Alert,
	StyleSheet,
	Dimensions,
	Picker,
	Platform,
	StatusBar,
	Text,
	TouchableOpacity,
	PixelRatio,
	View,
	ScrollView,
	Button,
	TextInput,
	KeyboardAvoidingView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import HeaderComp from '../component/HeaderComp';
import LinearGradient from 'react-native-linear-gradient';
import RNBootSplash from 'react-native-bootsplash';
// import defaultConfig from '../config';
import FloatingLabelInput from '../component/FloatingLabelInput';
import FloatingLabel from 'react-native-floating-labels';
import Pen from '../component/svg/Pen'
import Cross from '../component/svg/Cross';
import CTA from '../component/CTA'
import Plus from '../component/svg/Plus'
import DocumentPicker from 'react-native-document-picker';
import Spinner from '../component/Spinner';
import Back from '../component/svg/back'

(screenwidth = Dimensions.get('window').width),
	(screenheight = Dimensions.get('window').height);

export default class BusinessCertificate extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			cr_number: '',
			vat: '',
			singleFileOBJ1: '',
			singleFileOBJ2: '',
			UUID_CRN: '',
			UUID_VAT: '',
			Error_Text: '',
			error_filed: '',
			ACCESSTOKEN: '',
			uid: '',

		};
	}


	componentDidMount() {
		this._retrieveData();
		this.setState({
			uid: this.props.companyuid
		})
	}

	_retrieveData = async () => {
		try {
			const value = await AsyncStorage.getItem('ACCESSTOKEN');
			if (value !== null) {
				// We have data!!
				var eventstring = new String();
				eventstring = value.replace(/"/g, "");
				this.setState({ ACCESSTOKEN: eventstring })
				this._getDocument()
				console.log(eventstring);
			}
		} catch (error) {
			// Error retrieving data
		}
	};

	_getDocument() {
		var myHeaders = new Headers();
		myHeaders.append("Authorization", "Token " + this.state.ACCESSTOKEN);
		var requestOptions = {
			method: 'GET',
			headers: myHeaders,
		};

		fetch("http://api.fleetzi.com/v1/management/companies/" + this.state.uid + "/documents?=", requestOptions)
			.then((response) => response.json())
			.then((responseJson) => {
				if (responseJson.status === "success") {
					console.log(responseJson)
					var data = responseJson.data
					for (let i = 0; i < data.length; i++) {
						if (data[i].type === 'CRN') { this.setState({ cr_number: data[i].value }) }
						if (data[i].type === 'VRN') { this.setState({ vat: data[i].value }) }
					}
					this.setState({
						isLoading: false
					})
				} else {
					this.setState({
						isLoading: false
					})
				}
			})
			.catch(error => console.log('error', error));
	}

	async SingleFilePicker() {
		try {
			const res = await DocumentPicker.pick({
				type: [DocumentPicker.types.allFiles],
			});
			this.setState({ singleFileOBJ1: res });
			this._UploadFile('crno');

		} catch (err) {
			if (DocumentPicker.isCancel(err)) {
				Alert.alert('Canceled');
			} else {
				Alert.alert('Unknown Error: ' + JSON.stringify(err));
				throw err;
			}
		}
	}

	async SingleFilePickervatno() {
		try {
			const res = await DocumentPicker.pick({
				type: [DocumentPicker.types.allFiles],
			});
			this.setState({ singleFileOBJ2: res });
			this._UploadFile('vatno');

		} catch (err) {
			if (DocumentPicker.isCancel(err)) {
				Alert.alert('Canceled');
			} else {
				Alert.alert('Unknown Error: ' + JSON.stringify(err));
				throw err;
			}
		}
	}

	_UploadFile(data) {
		if (data === 'crno') {
			var formdata = new FormData();
			formdata.append("file", this.state.singleFileOBJ1, this.state.singleFileOBJ1.name);
			formdata.append("type", "CON");

			var requestOptions = {
				method: 'POST',
				body: formdata,
				redirect: 'follow'
			};
			console.log("post hoga ab")

			fetch("http://api.fleetzi.com/v1/file/", requestOptions)
				.then((response) => response.json())
				.then((responseJson) => {
					console.log(responseJson)
					this.setState({ UUID_CRN: responseJson.data.uid })
				})
				.catch(error => console.log('error', error));
		}
		if (data === 'vatno') {
			var formdata = new FormData();
			formdata.append("file", this.state.singleFileOBJ2, this.state.singleFileOBJ2.name);
			formdata.append("type", "CON");

			var requestOptions = {
				method: 'POST',
				body: formdata,
				redirect: 'follow'
			};
			console.log("post hoga ab second wala")

			fetch("http://api.fleetzi.com/v1/file/", requestOptions)
				.then((response) => response.json())
				.then((responseJson) => {
					console.log(responseJson)
					this.setState({ UUID_VAT: responseJson.data.uid })
				})
				.catch(error => console.log('error', error));
		}
	}


	_OnSubmit() {
		if (!!this.state.cr_number && this.state.vat && this.state.UUID_CRN && this.state.UUID_VAT) {
			this.setState({ isLoading: true })
			var myHeaders = new Headers();
			myHeaders.append("Authorization", "Token " + this.state.ACCESSTOKEN);
			myHeaders.append("Content-Type", "application/json");

			var raw = JSON.stringify(
				{
					"data": [
						{
							"type": "CRN",
							"value": this.state.cr_number,
							"files": [this.state.UUID_CRN]
						},
						{
							"type": "VRN",
							"value": this.state.vat,
							"files": [this.state.UUID_VAT]
						},
						{
							"type": "CON",
							"value": this.state.vat,
							"files": [this.state.UUID_VAT]
						}
					]
				}
			);
			var requestOptions = {
				method: 'POST',
				headers: myHeaders,
				body: raw,
				redirect: 'follow'
			};
			console.log(raw)
			fetch("http://api.fleetzi.com/v1/management/companies/" + this.state.uid + "/documents", requestOptions)
				.then((response) => response.json())
				.then((responseJson) => {
					console.log(responseJson)
					this.setState({ isLoading: false })
					Actions.Tabbar()
				})
				.catch(error => console.log('error', error));

		} else {
			this._Error_handel()
		}
	}


	_Error_handel = () => {
		console.log(this.state)
		if (this.state.cr_number === '') {
			this.setState({ Error_Text: 'Please Enter CR No', error_filed: 'cr_number', isLoading: false, })
		} else if (this.state.vat === '') {
			this.setState({ Error_Text: 'Please Enter vat Name', error_filed: 'vat', isLoading: false, })
		} else if (this.state.vat === '') {
			this.setState({ Error_Text: 'upload Vat Document', error_filed: 'vat', isLoading: false, })
		} else if (this.state.cr_number === '') {
			this.setState({ Error_Text: 'Upload Car Document', error_filed: 'cr_number', isLoading: false, })
		}
	}


	render() {
		return (
			<Fragment>
				<LinearGradient
					colors={['#323232', '#14181A', '#000000']}
					style={styles.linearGradient}
					start={{ x: 0, y: 0 }}
					end={{ x: 1, y: 1 }}>
					<StatusBar translucent backgroundColor="transparent" />
					<SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
					<SafeAreaView style={styles.screen}>
						<Spinner visible={this.state.isLoading} />
						<View style={styles.container}>
							<View style={styles.headerContainer}>
								<View>
									<Text style={styles.header}>
										Enter business certificate
								</Text>
								</View>
								{/* <TouchableOpacity onPress={() => Actions.pop()}>
									<Cross />
								</TouchableOpacity> */}
							</View>
							<ScrollView>
								<KeyboardAvoidingView>
									<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
										<View style={{ width: '100%' }}>

											<FloatingLabelInput
												label="CR Number"
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.cr_number}
												returnKeyType={'next'}
												blurOnSubmit={false}
												// onSubmitEditing={() => this.refadd.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ cr_number: text })
												}
											></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>
									{
										this.state.error_filed === 'cr_number' ?
											<Text style={{ color: 'red' }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}


									<TouchableOpacity style={styles.doucumentUpload} onPress={this.SingleFilePicker.bind(this)}>
										<View style={styles.uploadedBox}></View>
										<View style={styles.status}>
											<Text style={styles.statusText}>Uploaded</Text>
										</View>
									</TouchableOpacity>
									<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
										<View style={{ width: '100%' }}>

											<FloatingLabelInput
												label="VAT"
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.vat}
												returnKeyType={'next'}
												blurOnSubmit={false}
												// onSubmitEditing={() => this.refadd.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ vat: text })
												}
											></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>
									{
										this.state.error_filed === 'vat' ?
											<Text style={{ color: 'red' }}>{this.state.Error_Text}</Text>
											: <Text></Text>
									}



									<TouchableOpacity style={styles.doucumentUpload} onPress={this.SingleFilePickervatno.bind(this)}>
										<View style={styles.uploadBox}>
											<Plus />
										</View>
										<View style={styles.status}>
											<Text style={styles.statusText}>Upload Document</Text>
										</View>
									</TouchableOpacity>



									<View style={styles.ViewContract}>
										<View style={styles.ViewContractBox}></View>
										<View style={styles.heading}>
											<Text style={styles.statusText}>View Contract</Text>
										</View>
									</View>
								</KeyboardAvoidingView>
							</ScrollView>
							<View style={{ flex: 1, justifyContent: 'flex-end', width: '100%', }}>
								{/* <TouchableOpacity  onPress={() => this._OnSubmit()}>
									<CTA ctaText={'UPDATE DETAILS'} />
								</TouchableOpacity> */}
								<View style={{ flexDirection: 'row', width: '100%', marginTop: 30 }}>
									<TouchableOpacity onPress={() => Actions.pop()} style={{ width: '20%', backgroundColor: '#222223', height: 50, justifyContent: "center", alignItems: 'center' }}>
										<Back />
									</TouchableOpacity>
									<TouchableOpacity onPress={() => this._OnSubmit()} style={{ width: '75%', marginHorizontal: 20 }}>
										<CTA ctaText={'UPDATE DETAILS'} />
									</TouchableOpacity>
								</View>
							</View>
						</View>
					</SafeAreaView>
				</LinearGradient>
			</Fragment>
		);
	}
}



const styles = StyleSheet.create({
	screen: {
		flex: 10,
		backgroundColor: 'transparent',
	},
	body: {
		flex: 10,
	},
	linearGradient: {
		flex: 1,
	},
	container: {
		flex: 1,
		paddingLeft: 20,
		paddingRight: 20
	},
	header: {
		color: '#FFFFFF',
		fontSize: 20,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
	},
	headerContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: 50
	},
	labelInput: {
		color: '#7D7D7D',
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
	},
	formInput: {
		fontSize: 18,
		borderBottomWidth: 1,
		borderColor: '#333',
		color: '#FFFFFF',
		fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
	},
	input: {
		borderWidth: 0,
		color: '#FFFFFF',
		fontSize: 14,
	},
	uploadedBox: {
		height: 90,
		width: 70,
		// borderColor:'red',
		// borderWidth:1,
		borderRadius: 5,
		backgroundColor: '#fff'
	},
	status: {
		marginHorizontal: 20
	},
	statusText: {
		color: '#FF8B00',
		fontSize: 16,
		fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',

	},
	doucumentUpload: {
		flexDirection: 'row',
		marginTop: 20,
		alignItems: 'center'
	},
	uploadBox: {
		height: 90,
		width: 70,
		// borderColor:'red',
		// borderWidth:1,
		borderRadius: 5,
		backgroundColor: '#4A4A4A',
		justifyContent: 'center',
		alignItems: 'center'
	},
	ViewContract: {
		marginTop: 20,
	},
	heading: {
		paddingTop: 10
	},
	ViewContractBox: {
		height: 90,
		width: 70,
		// borderColor:'red',
		// borderWidth:1,
		borderRadius: 5,
		backgroundColor: '#fff'
	}

})