/**
* This is the Home Screen
**/

// React native and others libraries imports
import React, { Component, Fragment } from 'react';
import { Image, SafeAreaView, Alert, StyleSheet, Dimensions, Linking, Platform, StatusBar, Text, TouchableOpacity, PixelRatio, View, ScrollView, Button, TextInput, KeyboardAvoidingView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import HeaderComp from "../component/HeaderComp"
import LinearGradient from 'react-native-linear-gradient';
import RNBootSplash from "react-native-bootsplash";
import AppIntroSlider from 'react-native-app-intro-slider';
import LottieView from 'lottie-react-native';
import API from '../utils/API'

screenWidth = Dimensions.get('window').width,
screenHeight = Dimensions.get('window').height


const slides = [
	{
		key: 'index1',
		subtitle: 'Why become a partner?',
		text: 'List idle equipment on Fleetzi for free',
		image: require('../../assets/Images/Image.png'),
		colorText: '',
		lastText:''
	},
	{
		key: 'index2',
		subtitle: 'Why become a partner?',
		text: 'Get business leads from across KSA',
		image: require('../../assets/Images/Image.png'),
		colorText: '',
		lastText:''
	},
	{
		key: 'index3',
		subtitle: 'Why become a partner?',
		text: 'On-time payments guaranteed',
		image: require('../../assets/Images/Image.png'),
		colorText: '',
		lastText:''
	}
];

const arabislides = [
	{
		key: 'index1',
		subtitle: 'أي شيء ، أي حجم ، في أي مكان',
		text: 'الحصول على أسطول صنع عصام',
		image: require('../../assets/Images/Image.png'),
		colorText: 'تسي',
		lastText:'!'
	},
	{
		key: 'index2',
		subtitle: 'أي شيء ، أي حجم ، في أي مكان',
		text: 'الحصول على أسطول صنع عصام',
		image: require('../../assets/Images/Image.png'),
		colorText: 'تسي',
		lastText:'!'
	},
	{
		key: 'index3',
		subtitle: 'أي شيء ، أي حجم ، في أي مكان',
		text: 'الحصول على أسطول صنع عصام',
		image: require('../../assets/Images/Image.png'),
		colorText: 'تسي',
		lastText:'!'
	}
];


export default class onBoardingHome extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showRealApp: false,
			applang:'english',
			displayLang:'عربى',
			signUpbuttonText:'SIGN UP',
			logInbuttonText:'LOG IN',
			HeavyText:'HEAVY',
			displayslides:slides,
			company_data:[],
		};
	}

	async componentDidMount() {
		RNBootSplash.hide({ duration: 250 });
		try {
			// Load async data from an inexistent endpoint.
			let signUp_company_data = await API.get("/management/companies/partner/onboarding/");
			if (signUp_company_data.data.status === "success") {
				const result = signUp_company_data.data.data.find(e => e.placeholder === "Company Size");
				this.setState({company_data:result.data})	
			}
		} catch (e) {
			console.log(`😱 Axios request failed: ${e}`);
		}
		
	}


	_renderItem = (item) => {
		return (
			<>
				{/* <HeaderComp/> */}
				<View style={styles.upperView}>
					<Text style={styles.anythingText}>{item.item.subtitle}</Text>
					<Text style={styles.mainHeading}>{item.item.text}
						<Text style={styles.mainHeadingcolored}>{item.item.colorText}</Text>
						<Text style={styles.mainHeading}>{item.item.lastText}</Text>
					</Text>
				</View>
				<View style={{position: 'absolute', top: 0, left: '10%', right: 0, bottom:'5%', justifyContent: 'center', alignItems: 'center' }}>
					<Image style={styles.mainImage} source={item.item.image} />
				</View>
			</>
		);
	}

	_onDone = () => {
		// User finished the introduction. Show real app through
		// navigation or simply by controlling state
		this.setState({ showRealApp: true });
	}
	_onChangeLang = () => {
		var currentlang = this.state.applang
		if (currentlang === 'english') {
			this.setState(
			{
				applang:'عربى',
				displayLang:'English',
				displayslides:arabislides,
				signUpbuttonText:'سجل',
				logInbuttonText:'تسجيل الدخول',
				HeavyText:'ثقيل'
			})
			AsyncStorage.setItem('AppLang', 'عربى');
		}
		if (currentlang === 'عربى') {
			this.setState(
				{
					applang:'english',
					displayLang:'عربى', 
					displayslides:slides,
					signUpbuttonText:'SIGN UP',
					logInbuttonText:'LOG IN',
					HeavyText:'HEAVY',
				})
			AsyncStorage.setItem('AppLang', 'english');
		}
	}


	render() {
		if (this.state.showRealApp) {
			return <App />;
		} else {
			return (
				<Fragment>
					<LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}>
						<StatusBar translucent backgroundColor="transparent" />
						<SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
						<SafeAreaView style={styles.screen}>
							<View style={styles.body}>
								<View style={{ position: 'absolute', top: '45%', left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }}>
									<Text zIndex={1} style={styles.heavyText}>{this.state.HeavyText}</Text>
								</View>
								<View style={{ position: 'absolute', top: '20%', left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }} >
									<LottieView source={require('../../assets/lottifiles/onboarding.json')} autoPlay loop />
								</View>
								<AppIntroSlider renderItem={this._renderItem}
									showNextButton={false}
									slides={this.state.displayslides}
									onDone={this._onDone}
									paginationStyle={{ position: 'absolute', top: (screenHeight / 5) + 60 }}
									showDoneButton={false}
									dotStyle={{ backgroundColor: '#303030' }}
									activeDotStyle={{ backgroundColor: '#FFB049', width: 25, height: 8 }}
								/>
								<View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: 8, }}>
									<TouchableOpacity style={styles.signUp} onPress={() => Actions.onBoardingRegistration({company_data:this.state.company_data})}>
										<Text style={styles.signupText}>{this.state.signUpbuttonText}</Text>
									</TouchableOpacity>

									<TouchableOpacity style={styles.logIn} onPress={() => Actions.loginviaotp()}>
										<Text style={styles.logInText}>{this.state.logInbuttonText}</Text>
									</TouchableOpacity>
								</View>
								<View>
									<TouchableOpacity onPress={this._onChangeLang} style={{marginBottom: '20%'}}>
										<Text style={styles.urduLang}>{this.state.displayLang}</Text>
									</TouchableOpacity>
								</View>
							</View>
						</SafeAreaView>
					</LinearGradient>
				</Fragment>
			)
		}
	}


}

const styles = StyleSheet.create({
	screen: {
		flex: 10,
		backgroundColor: 'transparent'
	},
	body: {
		flex: 10,
	},
	linearGradient: {
		flex: 1
	},
	upperView: {
		marginTop: '20%',
		paddingLeft: 20,
		paddingRight: 20,
	},
	anythingText: {
		color: '#FFFFFF',
		fontSize: 16,
		fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
		textAlign: 'center',
		opacity: 0.5,
		letterSpacing: 1
	},
	mainHeading: {
		color: '#FFFFFF',
		fontSize: 30,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
		textAlign: 'center',
		marginTop:20
	},
	mainHeadingcolored: {
		color: '#FF9800',
		fontSize: 30,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
		textAlign: 'center',
	},

	outercircle: {
		borderColor: '#FFB049',
		borderWidth: .1,
		borderRadius: 200,
		width: screenWidth * .8,
		height: screenWidth * .8,


	},
	outercircle1: {
		borderColor: '#FFB049',
		borderWidth: .3,
		borderRadius: 200,
		width: screenWidth * .8 - 70,
		height: screenWidth * .8 - 70,
		margin: 33,


	},
	outercircle2: {
		borderColor: '#FFB049',
		borderWidth: .8,
		borderRadius: 200,
		width: screenWidth * .8 - 140,
		height: screenWidth * .8 - 140,
		margin: 33,


	},
	outercircleInner: {
		borderColor: '#FFB049',
		borderWidth: 1,
		borderRadius: 200,
		width: screenWidth * .8 - 210,
		height: screenWidth * .8 - 210,
		margin: 33

	},
	mainImage: {
		height: 150,
		width: 210,
		position: 'absolute',
		bottom: 30,
		left: 50

	},
	urduLang: {
		color: '#FFFFFF',
		fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
		textAlign: 'center',
		textDecorationLine: 'underline',
		fontSize:20,
		paddingBottom:10
	},
	signUp: {
		backgroundColor: '#323232',
		width: '45%',
		height: 55,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 2
	},
	signupText: {
		color: '#FFFFFF',
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
		fontSize:14,
	},
	logIn: {
		backgroundColor: '#FF8B00',
		width: '45%',
		height: 55,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 2,
	},
	logInText: {
		color: '#261400',
		fontSize:14,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',

	},
	heavyText: {
		color: '#FFFFFF',
		fontSize: 70,
		fontFamily: 'AvenirNextHeavy',
		opacity: .05,
		position: 'absolute',
		textAlign: 'center',
		marginBottom: 25
	}
});