/**
 * This is the Home Screen
 **/

// React native and others libraries imports
import React, {Component, Fragment} from 'react';
import {
  Image,
  SafeAreaView,
  Alert,
  StyleSheet,
  Dimensions,
  Picker,
  Platform,
  StatusBar,
  Text,
  TouchableOpacity,
  PixelRatio,
  View,
  ScrollView,
  Button,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import HeaderComp from '../component/HeaderComp';
import LinearGradient from 'react-native-linear-gradient';
import RNBootSplash from 'react-native-bootsplash';
import defaultConfig from '../config';
import FloatingLabel from 'react-native-floating-labels';
import EyeSvg from '../component/svg/EyeSvg';

(screenwidth = Dimensions.get('window').width),
  (screenheight = Dimensions.get('window').height);

export default class onBoardingLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      ACCESSTOKEN: '',
      phone_number: '',
      password: '',
      country_code: '+91',
      hasError: false,
      errorText: '',
      phoneInputFouse: false,
      passwordInputFouse: false,
    };
  }

  componentDidMount() {
    RNBootSplash.hide({duration: 250});
  }

  onFocus(input) {
    if (input === 'phoneInputFouse') {
      this.setState({phoneInputFouse: true, passwordInputFouse: false});
    }
    if (input === 'passwordInputFouse') {
      this.setState({phoneInputFouse: false, passwordInputFouse: true});
    }
  }
  _onsubmit() {
    console.log(this.state);
    if (!!this.state.phone_number && this.state.password) {
      this.setState({isLoading: true});
      let formdata = new FormData();
      formdata.append('country_code', this.state.country_code);
      formdata.append('number', this.state.phone_number);
      formdata.append('password', this.state.password);
      formdata.append('platform', 'part');
      console.log(formdata);
      fetch('http://34.87.55.15/api/v1/token/login/password/?format=json', {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formdata,
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log(responseJson);
          if (responseJson.status == 'success') {
            AsyncStorage.setItem('USERDATA', JSON.stringify(responseJson.data));
            AsyncStorage.setItem(
              'ACCESSTOKEN',
              JSON.stringify(responseJson.data.access_token),
            );
            this.setState({isLoading: false});
            Actions.onBoardingOptionspage();
          } else {
            this.setState({
              isLoading: false,
              hasError: true,
              errorText: responseJson.message,
            });
          }
        })
        .catch(error => {
          console.error(error);
          console.log('somthing error');
          this.setState({isLoading: false});
        });
    } else {
      this.setState({
        isLoading: false,
        hasError: true,
        errorText: 'Enter your phone no. and password!',
      });
    }
  }

  ChangeInputFocus(input) {
    if (input === 'phoneInputFouse') {
      this.setState({phoneInputFouse: true, passwordInputFouse: false});
    }
    if (input === 'passwordInputFouse') {
      this.setState({phoneInputFouse: false, passwordInputFouse: true});
    }
  }

  onBlur() {
    console.log('#####: onBlur');
    this.setState({phoneInputFouse: false, passwordInputFouse: false});
  }

  render() {
    return (
      <Fragment>
        <LinearGradient
          colors={['#323232', '#14181A', '#000000']}
          style={styles.linearGradient}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}>
          <StatusBar translucent backgroundColor="transparent" />
          <SafeAreaView style={{flex: 0, backgroundColor: 'transparent'}} />
          <SafeAreaView style={styles.screen}>
            <ScrollView style={styles.body}>
              <HeaderComp />
              {/* <View style={{ height: '20%' }}></View> */}

              <View style={{paddingLeft: 20, paddingRight: 20}}>
                <KeyboardAvoidingView>
                  <Text
                    style={{
                      fontFamily:
                        Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
                      color: 'white',
                      fontSize: 45,
                      paddingBottom: 10,
                    }}>
                    login
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      color: 'white',
                      opacity: 0.4,
                      lineHeight: 25,
                      fontFamily:
                        Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
                    }}>
                    Please submit your information and a Fleetzi representative
                    will get in touch with you.
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      color: 'white',
                      opacity: 0.4,
                      letterSpacing: 0.3,
                      lineHeight: 25,
                      margin: 0,
                      fontFamily:
                        Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
                    }}>
                    We'll verify your email and phone no. to create your
                    account.
                  </Text>

                  <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    style={[
                      styles.formInput,
                      {
                        borderColor: this.state.phoneInputFouse
                          ? '#FFFFFF'
                          : '#333',
                      },
                    ]}
                    value={this.state.phone_number}
                    onFocus={() => this.onFocus('phoneInputFouse')}
                    onEndEditing={() => this.onBlur()}
                    onChangeText={number =>
                      this.setState({phone_number: number})
                    }>
                    Phone number
                  </FloatingLabel>
                  <View
                    onFocus={() => this.onFocus('passwordInputFouse')}
                    onEndEditing={() => this.onBlur()}
                    style={[
                      styles.formInput,
                      {
                        borderColor: this.state.passwordInputFouse
                          ? '#FFFFFF'
                          : '#333',
                        width: '100%',
                        flexDirection: 'row',
                      },
                    ]}>
                    <View style={{width: '90%'}}>
                      <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        value={this.state.password}
                        onFocus={() => this.onFocus('passwordInputFouse')}
                        onEndEditing={() => this.onBlur()}
                        onChangeText={text => this.setState({password: text})}>
                        Password
                      </FloatingLabel>
                    </View>
                    <View
                      style={{
                        marginTop: screenheight * 0.02,
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: '10%',
                      }}>
                      <EyeSvg />
                    </View>
                  </View>

                  {this.state.hasError ? (
                    <Text style={{paddingTop: 10, color: 'red'}}>
                      {this.state.errorText}
                    </Text>
                  ) : (
                    <Text></Text>
                  )}

                  <View style={{paddingTop: 40}}>
                    <TouchableOpacity
                      onPress={() => Actions.onBoardingForgotPassword()}
                      style={{
                        width: screenwidth*.33,
                      }}>
                      <Text
                        style={{
                          fontFamily:
                            Platform.OS === 'ios'
                              ? 'NexaRegular'
                              : 'Nexa Regular',
                          color: 'white',
                          fontSize: 13,
                        }}>
                        Forgot password?
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => Actions.loginviaotp({phone_number:this.state.phone_number})}
                      style={{
                        marginTop: 30,
                        width: screenwidth*.40,
                      }}>
                      <Text
                        style={{
                          fontFamily:
                            Platform.OS === 'ios'
                              ? 'NexaRegular'
                              : 'Nexa Regular',
                          color: 'white',
                          fontSize: 17,
                        }}>
                        <Text style={{color: '#6b6d6e'}}> or </Text>
                        Login via OTP
                      </Text>
                    </TouchableOpacity>
                  </View>

                  <Text>{'\n'}</Text>
                  <TouchableOpacity
                    onPress={() => this._onsubmit()}
                    style={{width: '40%'}}>
                    <LinearGradient
                      colors={['#FFAC00', '#FF8B00']}
                      style={styles.ButtonView}
                      start={{x: 0, y: 0}}
                      end={{x: 1, y: 1}}>
                      <Text style={styles.buttomText}>NEXT</Text>
                    </LinearGradient>
                  </TouchableOpacity>
                </KeyboardAvoidingView>
              </View>
            </ScrollView>
          </SafeAreaView>
        </LinearGradient>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 10,
    backgroundColor: 'transparent',
  },
  body: {
    flex: 10,
  },
  linearGradient: {
    flex: 1,
  },
  labelInput: {
    color: '#7D7D7D',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
  },
  formInput: {
    borderBottomWidth: 1,
  },
  input: {
    borderWidth: 0,
    color: '#FFFFFF',
    fontSize: 14,
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },
  ButtonView: {
    backgroundColor: '#FF8B00',
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#FFFFFF',
    borderBottomWidth: 5,
    opacity: 0.85,
  },
  buttomText: {
    color: '#261400',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
    fontSize: 14,
    marginTop: screenheight * 0.01,
  },
});
