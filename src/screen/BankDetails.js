import React, { Component, Fragment } from 'react';
import {
	Image,
	SafeAreaView,
	Alert,
	StyleSheet,
	Dimensions,
	Picker,
	Platform,
	StatusBar,
	Text,
	TouchableOpacity,
	PixelRatio,
	View,
	ScrollView,
	Button,
	TextInput,
	KeyboardAvoidingView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import HeaderComp from '../component/HeaderComp';
import LinearGradient from 'react-native-linear-gradient';
import RNBootSplash from 'react-native-bootsplash';
import defaultConfig from '../config';
import FloatingLabelInput from '../component/FloatingLabelInput';
import FloatingLabel from 'react-native-floating-labels';
import Pen from '../component/svg/Pen'
import Back from '../component/svg/back'
import Cross from '../component/svg/Cross';
import CTA from '../component/CTA'
import Spinner from '../component/Spinner';
// import FloatingLabel from 'react-native-floating-labels';


(screenwidth = Dimensions.get('window').width),
	(screenheight = Dimensions.get('window').height);
export default class BankDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			bank_name: '',
			account_holder_name: '',
			iban_no: '',
			swift_code: '',
			uid: '',
			Error_Text: '',
			error_filed: '',
			ACCESSTOKEN: '',

		};
	}

	componentDidMount() {
		this._retrieveData();
		this.setState({
			uid: this.props.companyuid
		})
	}

	_retrieveData = async () => {
		try {
			const value = await AsyncStorage.getItem('ACCESSTOKEN');
			if (value !== null) {
				// We have data!!
				var eventstring = new String();
				eventstring = value.replace(/"/g, "");
				this.setState({ ACCESSTOKEN: eventstring })
				this._getBankDetails()
				console.log(eventstring);
			}
		} catch (error) {
			// Error retrieving data
		}
	};

	_getBankDetails() {
		var myHeaders = new Headers();
		myHeaders.append("Authorization", "Token " + this.state.ACCESSTOKEN);
		var requestOptions = {
			method: 'GET',
			headers: myHeaders,
		};

		fetch("http://api.fleetzi.com/v1/management/companies/" + this.state.uid, requestOptions)
			.then((response) => response.json())
			.then((responseJson) => {
				console.log(responseJson.data)
				this.setState({
					bank_name: responseJson.data.bank,
					account_holder_name: responseJson.data.account_name,
					iban_no: responseJson.data.iban,
					swift_code: responseJson.data.swift_code,
					isLoading: false
				})
			})
			.catch(error => console.log('error', error));
	}

	_OnSubmit() {
		if (!!this.state.bank_name &&
			this.state.account_holder_name &&
			this.state.iban_no &&
			this.state.swift_code) {
			//
			this.setState({ isLoading: true })
			var myHeaders = new Headers();
			myHeaders.append("Authorization", "Token " + this.state.ACCESSTOKEN);
			var formdata = new FormData();
			formdata.append("account_name", this.state.account_holder_name);
			formdata.append("iban", this.state.iban_no);
			formdata.append("swift_code", this.state.swift_code);
			formdata.append("bank", this.state.bank_name);
			console.log(formdata)
			var requestOptions = {
				method: 'PUT',
				headers: myHeaders,
				body: formdata,
			};
			fetch("http://api.fleetzi.com/v1/management/companies/" + this.state.uid + "/bank-details", requestOptions)
				.then((response) => response.json())
				.then((responseJson) => {
					console.log(responseJson)
					this.setState({ isLoading: false })
					console.log("ab run hoga actions")
					Actions.Tabbar()
				})
				.catch(error => console.log('error', error));
		} else {
			console.log("error")
			// this._Error_handel()
		}

	}




	render() {
		return (
			<Fragment>
				<LinearGradient
					colors={['#323232', '#14181A', '#000000']}
					style={styles.linearGradient}
					start={{ x: 0, y: 0 }}
					end={{ x: 1, y: 1 }}>
					<StatusBar translucent backgroundColor="transparent" />
					<SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
					<SafeAreaView style={styles.screen}>
						<Spinner visible={this.state.isLoading} />
						<View style={styles.container}>
							<View style={styles.headerContainer}>
								<View>
									<Text style={styles.header}>
										Enter bank details
								</Text>
								</View>
								{/* <TouchableOpacity onPress={() => Actions.pop()}>
									<Cross />
								</TouchableOpacity> */}
							</View>
							<View style={{ height: '70%' }}>
								<KeyboardAvoidingView>
									<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
										<View style={{ width: '100%' }}>
											<FloatingLabelInput
												label="Bank Name"
												// labelStyle={styles.labelInput}
												// inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.bank_name}
												returnKeyType={'next'}
												blurOnSubmit={false}
												onSubmitEditing={() => this.refholdername.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ bank_name: text })
												}
											>
											</FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>
									<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, }}>
										<View style={{ width: '100%' }}>

											<FloatingLabelInput
												label="Account Holder Name"
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.account_holder_name}
												returnKeyType={'next'}
												blurOnSubmit={false}
												ref={r => (this.refholdername = r)}
												onSubmitEditing={() => this.refiban.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ account_holder_name: text })
												}
											></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>
									<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
										<View style={{ width: '100%' }}>
											<FloatingLabelInput
												label="IBAN Number"
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.iban_no}
												blurOnSubmit={false}
												returnKeyType={'next'}
												ref={r => (this.refiban = r)}
												onSubmitEditing={() => this.refswift.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ iban_no: text })
												}
											></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>

									<View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
										<View style={{ width: '100%' }}>
											<FloatingLabelInput
												label="Swift Code"
												labelStyle={styles.labelInput}
												inputStyle={styles.input}
												style={styles.formInput}
												value={this.state.swift_code}
												blurOnSubmit={false}
												returnKeyType={'done'}
												ref={r => (this.refswift = r)}
												// onSubmitEditing={() => this.refstate.getInnerRef().focus()}
												onChangeText={text =>
													this.setState({ swift_code: text })
												}
											></FloatingLabelInput>
										</View>
										<View style={{ width: '8%', position: 'absolute', right: 0 }}><Pen /></View>
									</View>

								</KeyboardAvoidingView>
							</View>

							{/* <View style={{ flexDirection: 'row', width: '100%', marginTop: 30 }}>
                  <TouchableOpacity onPress={() => Actions.pop()} style={{ width: '20%', backgroundColor: '#222223', height: 50, justifyContent: "center", alignItems: 'center' }}>
                    <Back />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.veryfy_otp()} style={{ width: '75%', marginHorizontal: 20 }}>
                    <CTA ctaText={'VERIFY & LOGIN'} />
                  </TouchableOpacity>
                </View> */}

							<View style={{ justifyContent: 'flex-end', flex: 1, paddingTop: 10 }}>
								<View style={{ marginTop: 50 }}>
									{/* <TouchableOpacity onPress={() => this._OnSubmit()} >
										<CTA ctaText={'UPDATE DETAILS'} />
									</TouchableOpacity> */}

							<View style={{ flexDirection: 'row', width: '100%', marginTop: 30 }}>
                  <TouchableOpacity onPress={() => Actions.pop()} style={{ width: '20%', backgroundColor: '#222223', height: 50, justifyContent: "center", alignItems: 'center' }}>
                    <Back />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this._OnSubmit()}  style={{ width: '75%', marginHorizontal: 20 }}>
                    <CTA ctaText={'UPDATE DETAILS'} />
                  </TouchableOpacity>
                </View>
								</View>
							</View>

						</View>
					</SafeAreaView>
				</LinearGradient>
			</Fragment>
		);
	}
}



const styles = StyleSheet.create({
	screen: {
		flex: 10,
		backgroundColor: 'transparent',
	},
	body: {
		flex: 10,
	},
	linearGradient: {
		flex: 1,
	},
	container: {
		flex: 1,
		paddingLeft: 20,
		paddingRight: 20
	},
	header: {
		color: '#FFFFFF',
		fontSize: 20,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
	},
	headerContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: 50
	},
	labelInput: {
		color: '#7D7D7D',
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
	},
	formInput: {
		fontSize: 18,
		borderBottomWidth: 1,
		borderColor: '#333',
		color: '#FFFFFF',
		fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
		width: '100%'
	},
	input: {
		borderWidth: 0,
		color: '#FFFFFF',
		fontSize: 14,
	},
})