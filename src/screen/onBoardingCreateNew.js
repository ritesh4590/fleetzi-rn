/**
 * This is the Home Screen
 **/

// React native and others libraries imports
import React, {Component, Fragment} from 'react';
import {
  Image,
  SafeAreaView,
  Alert,
  StyleSheet,
  Dimensions,
  Picker,
  Platform,
  StatusBar,
  Text,
  TouchableOpacity,
  PixelRatio,
  View,
  ScrollView,
  Button,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import HeaderComp from '../component/HeaderComp';
import LinearGradient from 'react-native-linear-gradient';
import RNBootSplash from 'react-native-bootsplash';
import defaultConfig from '../config';
import FloatingLabelInput from '../component/FloatingLabelInput';
import FloatingLabel from 'react-native-floating-labels';

import EyeSvg from '../component/svg/EyeSvg';

(screenwidth = Dimensions.get('window').width),
  (screenheight = Dimensions.get('window').height);
export default class onBoardingRegistration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      ACCESSTOKEN: '',
      password: '',
      confirm_password: '',
      phoneInputFouse: false,
      passwordInputFouse: false,
    };
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('ACCESSTOKEN');
      if (value !== null) {
        // We have data!!
        var eventstring = new String();
        eventstring = value.replace(/"/g, '');
        this.setState({ACCESSTOKEN: eventstring});
        console.log(eventstring);
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  componentDidMount() {
    RNBootSplash.hide({duration: 250});
    this._retrieveData();
  }

  _onsubmit() {
    if (!!this.state.password && this.state.confirm_password) {
      this.setState({isLoading: true});
      let formdata = new FormData();
      formdata.append('password', this.state.password);
      formdata.append('confirm_password', this.state.confirm_password);
      fetch(
        'http://34.87.55.15/api/v1/management/user/password/reset/?format=json',
        {
          method: 'POST',
          headers: {
            Authorization: 'Token ' + this.state.ACCESSTOKEN,
            'Content-Type': 'multipart/form-data',
          },
          body: formdata,
        },
      )
        .then(response => response.json())
        .then(responseJson => {
          console.log(responseJson);
          if (responseJson.status == 'success') {
            this.setState({isLoading: false});
            Alert.alert(responseJson.message);
            Actions.managelisting();
          } else {
            this.setState({isLoading: false});
            Alert.alert(responseJson.message);
            console.log('somthing error');
          }
        })
        .catch(error => {
          console.error(error);
          console.log('somthing error');
          this.setState({isLoading: false});
        });
    } else {
      this.setState({isLoading: false});
      Alert.alert('All fields are mandatory');
    }
  }

  onFocus(input) {
    if (input === 'phoneInputFouse') {
      this.setState({phoneInputFouse: true, passwordInputFouse: false});
    }
    if (input === 'passwordInputFouse') {
      this.setState({phoneInputFouse: false, passwordInputFouse: true});
    }
  }
  ChangeInputFocus(input) {
    if (input === 'phoneInputFouse') {
      this.setState({phoneInputFouse: true, passwordInputFouse: false});
    }
    if (input === 'passwordInputFouse') {
      this.setState({phoneInputFouse: false, passwordInputFouse: true});
    }
  }
  onBlur() {
    console.log('#####: onBlur');
    this.setState({phoneInputFouse: false, passwordInputFouse: false});
  }

  render() {
    return (
      <Fragment>
        <LinearGradient
          colors={['#323232', '#14181A', '#000000']}
          style={styles.linearGradient}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}>
          <StatusBar translucent backgroundColor="transparent" />
          <SafeAreaView style={{flex: 0, backgroundColor: 'transparent'}} />
          <SafeAreaView style={styles.screen}>
            <ScrollView style={styles.body}>
              {/* <HeaderComp /> */}
              {/* <View style={{ height: '20%' }}></View> */}

              <View style={{paddingLeft: 20, paddingRight: 20,marginTop:screenHeight * 0.15,}}>
                <KeyboardAvoidingView>
                  <Text
                    style={{
                      fontFamily:
                        Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
                      color: 'white',
                      fontSize: 45,
                      paddingBottom: 10,
                    }}>
                    create a new{'\n'}password
                  </Text>
                  <View
                    onEndEditing={() => this.onBlur()}
                    onFocus={() => this.onFocus('phoneInputFouse')}
                    style={[
                      styles.formInput,
                      {
                        borderColor: this.state.phoneInputFouse
                          ? '#FFFFFF'
                          : '#333',
                        width: '100%',
                        flexDirection: 'row',
                        marginTop:10
                      },
                    ]}>
                    <View style={{width: '90%'}}>
                      <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        value={this.state.password}
                        returnKeyType={'next'}
                        onChangeText={password => this.setState({password})}
                        onSubmitEditing={() => this.reffn.getInnerRef().focus()}
                        onChangeText={text => this.setState({password: text})}>
                        Enter new password
                      </FloatingLabel>
                    </View>
                    <View
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: '10%',
                        marginTop:screenheight*.02,
                      }}>
                      <EyeSvg />
                    </View>
                  </View>
                  <View
                    onFocus={() => this.onFocus('passwordInputFouse')}
                    onEndEditing={() => this.onBlur()}
                    style={[
                      styles.formInput,
                      {
                        borderColor: this.state.passwordInputFouse
                          ? '#FFFFFF'
                          : '#333',
                        width: '100%',
                        flexDirection: 'row',
                      },
                    ]}>
                    <View style={{width: '90%'}}>
                      <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        value={this.state.confirm_password}
                        returnKeyType={'next'}
                        ref={r => (this.reffn = r)}
                        onSubmitEditing={() => this.refln.getInnerRef().focus()}
                        onChangeText={text =>
                          this.setState({confirm_password: text})
                        }>
                        Confirm password
                      </FloatingLabel>
                    </View>
                    <View
                      style={{
                        marginTop:screenheight*.02,
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: '10%',
                      }}>
                      <EyeSvg />
                    </View>
                  </View>

                  <Text>{'\n'}</Text>
                  <TouchableOpacity onPress={() => this._onsubmit()} style={{width:'40%',marginTop:screenHeight * 0.07,}}>
                    <LinearGradient
                      colors={['#FFAC00', '#FF8B00']}
                      style={styles.ButtonView}
                      start={{x: 0, y: 0}}
                      end={{x: 1, y: 1}}>
                      <Text style={styles.buttomText}>NEXT</Text>
                    </LinearGradient>
                  </TouchableOpacity>
                </KeyboardAvoidingView>
              </View>
            </ScrollView>
          </SafeAreaView>
        </LinearGradient>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 10,
    backgroundColor: 'transparent',
  },
  body: {
    flex: 10,
  },
  linearGradient: {
    flex: 1,
  },
  labelInput: {
    color: '#7D7D7D',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
    // fontSize:22
  },
  formInput: {
    borderBottomWidth: 1,
    color: '#FFFFFF',
    // width:'115%'
  },
  input: {
    borderWidth: 0,
    // fontSize: 12,
    color: '#FFFFFF',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },
  ButtonView: {
    backgroundColor: '#FF8B00',
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#FFFFFF',
    borderBottomWidth: 5,
    opacity: 0.85,
  },
  buttomText: {
    color: '#261400',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
    fontSize: 14,
    marginTop: screenheight * 0.01,
  },
  eyeSvg: {
    position: 'absolute',
    top: screenheight * 0.21,
    right: 0,
  },
  eyeSvg2: {
    position: 'absolute',
    top: screenheight * 0.31,
    right: 0,
  },
});
