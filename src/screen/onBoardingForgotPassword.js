/**
 * This is the Home Screen
 **/

// React native and others libraries imports
import React, {Component, Fragment} from 'react';
import {
  Image,
  SafeAreaView,
  Alert,
  StyleSheet,
  Dimensions,
  Picker,
  Platform,
  StatusBar,
  Text,
  TouchableOpacity,
  PixelRatio,
  View,
  ScrollView,
  Button,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import HeaderComp from '../component/HeaderComp';
import LinearGradient from 'react-native-linear-gradient';
import RNBootSplash from 'react-native-bootsplash';
import defaultConfig from '../config';
import FloatingLabel from 'react-native-floating-labels';

export default class onBoardingForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      ACCESSTOKEN: '',
      full_name: '',
      phone_number: '',
      hidePassword: true,
      password: '',
      timer: 30,
      country_code: '+91',
      hasError: false,
      errorText: '',
      resendotp: false,
      submitbutoon: false,
      hash: '',
      phoneotp: '',
      phoneInputFouse: false,
      passwordInputFouse: false,
    };
  }

  componentDidMount() {
    RNBootSplash.hide({duration: 250});
  }

  componentDidUpdate() {
    if (this.state.timer === 1) {
      this.setState({timer: 30, resendotp: true});
      clearInterval(this.interval);
    }
  }

  RunTimer() {
    this.interval = setInterval(
      () => this.setState(prevState => ({timer: prevState.timer - 1})),
      1000,
    );
  }
  onFocus(input) {
    if (input === 'phoneInputFouse') {
      this.setState({phoneInputFouse: true, passwordInputFouse: false});
    }
    if (input === 'passwordInputFouse') {
      this.setState({phoneInputFouse: false, passwordInputFouse: true});
    }
  }
  onBlur() {
    console.log('#####: onBlur');
    this.setState({phoneInputFouse: false, passwordInputFouse: false});
  }
  get_otp() {
    if (!!this.state.phone_number && this.state.country_code) {
      this.RunTimer();
      this.setState({isLoading: true});
      let formdata = new FormData();
      formdata.append('country_code', this.state.country_code);
      formdata.append('number', this.state.phone_number);
      formdata.append('platform', 'part');
      fetch(
        'http://34.87.55.15/api/v1/token/login/otp/request-sms/?format=json',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'multipart/form-data',
          },
          body: formdata,
        },
      )
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.status == 'success') {
            this.setState({
              isLoading: false,
              hash: responseJson.data.hash,
              submitbutoon: true,
            });
          } else {
            this.setState({isLoading: false});
            console.log('somthing error');
          }
        })
        .catch(error => {
          console.error(error);
          console.log('somthing error');
          this.setState({isLoading: false});
        });
    } else {
      this.setState({
        isLoading: false,
        hasError: true,
        errorText: 'Enter your phone no.',
      });
    }
  }
  ChangeInputFocus(input) {
    if (input === 'phoneInputFouse') {
      this.setState({phoneInputFouse: true, passwordInputFouse: false});
    }
    if (input === 'passwordInputFouse') {
      this.setState({phoneInputFouse: false, passwordInputFouse: true});
    }
  }
  veryfy_otp() {
    if (!!this.state.phoneotp) {
      this.setState({isLoading: true});
      let formdata = new FormData();
      formdata.append('country_code', this.state.country_code);
      formdata.append('number', this.state.phone_number);
      formdata.append('hash', this.state.hash);
      formdata.append('otp', this.state.phoneotp);
      console.log(formdata);
      fetch('http://34.87.55.15/api/v1/token/login/otp/verify/?format=json', {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formdata,
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log(responseJson);
          if (responseJson.status == 'success') {
            AsyncStorage.setItem('USERDATA', JSON.stringify(responseJson.data));
            AsyncStorage.setItem(
              'ACCESSTOKEN',
              JSON.stringify(responseJson.data.access_token),
            );
            this.setState({isLoading: false});
            Actions.onBoardingCreateNew();
          } else {
            this.setState({
              isLoading: false,
              hasError: true,
              errorText: responseJson.message,
            });
            Alert.alert(responseJson.message);
            console.log('somthing error');
          }
        })
        .catch(error => {
          console.error(error);
          console.log('somthing error');
          this.setState({isLoading: false});
        });
    } else {
      this.setState({
        isLoading: false,
        hasError: true,
        errorText: 'Enter your OTP.!',
      });
    }
  }

  setPasswordVisibility = () => {
    this.setState({hidePassword: !this.state.hidePassword});
  };

  render() {
    return (
      <Fragment>
        <LinearGradient
          colors={['#323232', '#14181A', '#000000']}
          style={styles.linearGradient}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}>
          <StatusBar translucent backgroundColor="transparent" />
          <SafeAreaView style={{flex: 0, backgroundColor: 'transparent'}} />
          <SafeAreaView style={styles.screen}>
            <ScrollView style={styles.body}>
              <HeaderComp />
              {/* <View style={{ height: '20%' }}></View> */}

              <View style={{paddingLeft: 20, paddingRight: 20}}>
                <KeyboardAvoidingView
                  behavior="position"
                  keyboardVerticalOffset={30}>
                  <Text
                    style={{
                      lineHeight: 57,
                      fontFamily:
                        Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
                      color: 'white',
                      fontSize: 45,
                      paddingBottom: 10,
                    }}>
                    forgot{'\n'}password
                  </Text>

                  <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    style={[
                      styles.formInput,
                      {
                        borderColor: this.state.phoneInputFouse
                          ? '#FFFFFF'
                          : '#333',
                      },
                    ]}
                    value={this.state.phone_number}
                    onEndEditing={() => this.onBlur()}
                    onFocus={() => this.onFocus('phoneInputFouse')}
                    onChangeText={number =>
                      this.setState({phone_number: number})
                    }>
                    Phone number
                  </FloatingLabel>
                  <FloatingLabel
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    style={[
                      styles.formInput,
                      {
                        borderColor: this.state.passwordInputFouse
                          ? '#FFFFFF'
                          : '#333',
                      },
                    ]}
                    value={this.state.phoneotp}
                    onEndEditing={() => this.onBlur()}
                    keyboardType="number-pad"
                    onFocus={() => this.onFocus('passwordInputFouse')}
                    onChangeText={text => this.setState({phoneotp: text})}>
                    OTP
                  </FloatingLabel>
                  {this.state.hasError ? (
                    <Text style={{paddingTop: 10, color: 'red'}}>
                      {this.state.errorText}
                    </Text>
                  ) : (
                    <Text></Text>
                  )}

                  <View
                    style={{
                      paddingTop: 40,
                      flex: 1,
                      justifyContent: 'space-between',
                      flexDirection: 'row',
                    }}>
                    <Text
                      style={{
                        fontFamily:
                          Platform.OS === 'ios'
                            ? 'NexaRegular'
                            : 'Nexa Regular',
                        color: '#FF9800',
                      }}>
                      00:{this.state.timer}
                    </Text>
                    {this.state.resendotp ? (
                      <TouchableOpacity onPress={() => this.get_otp()}>
                        <Text
                          style={{
                            fontFamily:
                              Platform.OS === 'ios'
                                ? 'NexaRegular'
                                : 'Nexa Regular',
                            color: 'white',
                          }}>
                          Resend OTP
                        </Text>
                      </TouchableOpacity>
                    ) : (
                      <Text
                        style={{
                          fontFamily:
                            Platform.OS === 'ios'
                              ? 'NexaRegular'
                              : 'Nexa Regular',
                          color: 'white',
                        }}>
                        Resend OTP
                      </Text>
                    )}
                  </View>

                  <Text>{'\n'}</Text>
                  {this.state.submitbutoon ? (
                    <TouchableOpacity onPress={() => this.veryfy_otp()} style={{width:'60%'}}>
                      <LinearGradient
                        colors={['#FFAC00', '#FF8B00']}
                        style={styles.ButtonView}
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 1}}>
                        <Text style={styles.buttomText}>CREATE PASSWORD</Text>
                      </LinearGradient>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity onPress={() => this.get_otp()} style={{width:'60%'}}>
                      <LinearGradient
                        colors={['#FFAC00', '#FF8B00']}
                        style={styles.ButtonView}
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 1}}>
                        <Text style={styles.buttomText}>GET OTP</Text>
                      </LinearGradient>
                    </TouchableOpacity>
                  )}
                </KeyboardAvoidingView>
              </View>
            </ScrollView>
          </SafeAreaView>
        </LinearGradient>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 10,
    backgroundColor: 'transparent',
  },
  body: {
    flex: 10,
  },
  linearGradient: {
    flex: 1,
  },
  labelInput: {
    color: '#7D7D7D',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
  },
  formInput: {
    borderBottomWidth: 1,
  },
  input: {
    borderWidth: 0,
    color: '#FFFFFF',
    fontSize: 14,
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },
  ButtonView: {
    backgroundColor: '#FF8B00',
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#FFFFFF',
    borderBottomWidth: 5,
    opacity: 0.85,
  },
  buttomText: {
    color: '#261400',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
    fontSize: 14,
    marginTop: screenheight * 0.01,
  },
});
