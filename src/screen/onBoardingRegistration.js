/**
 * This is the Home Screen
 **/

// React native and others libraries imports
import React, { Component, Fragment } from 'react';
import {
  Image,
  SafeAreaView,
  Alert,
  StyleSheet,
  Dimensions,
  Picker,
  Platform,
  StatusBar,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  View,
  ScrollView,
  Button,
  Keyboard,
  Modal,
  KeyboardAvoidingView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import HeaderComp from '../component/HeaderComp';
import LinearGradient from 'react-native-linear-gradient';
import RNBootSplash from 'react-native-bootsplash';
import defaultConfig from '../config';
import FloatingLabelInput from '../component/FloatingLabelInput';
import CTA from '../component/CTA'
import Back from '../component/svg/back'
import API from '../utils/API'
import Spinner from '../component/Spinner';
import IncorrectOTP from '../component/svg/incorrectOTP'

(screenwidth = Dimensions.get('window').width),
  (screenheight = Dimensions.get('window').height);

export default class onBoardingRegistration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      Alert_Visibility:false,
      ACCESSTOKEN: '',
      org_name: '',
      company_size: '',
      email: '',
      phone: '',
      reason: '',
      first_name: '',
      last_name: '',
      country_code: '+91',
      display_country_code:'Ind +91',
      company_data: [],
      error_message: '',
      fcmToken:'',
    };
  }


  componentDidMount() {
    RNBootSplash.hide({ duration: 250 });
    this.setState({ company_data: this.props.company_data })
    this.checkPermission();
  }

    ////////////////////// Add these methods //////////////////////
  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  //3
  async getToken() {
    // console.log("=================================================");
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    // console.log(fcmToken);
    this.setState({ fcmToken: fcmToken })
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      // console.log("=================================================");
      if (fcmToken) {
        // console.log("=================================================");
        console.log(fcmToken);
        this.setState({ fcmToken: fcmToken })
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }


  _onChangeCountryCode = () => {
    var current_country = this.state.display_country_code
    if (current_country === 'Ind +91') {
      this.setState({display_country_code:'KSA +966', country_code:'+966'})
    }
    if (current_country === 'KSA +966') {
      this.setState({display_country_code:'Ind +91', country_code:'+91'})
    }
  }

  _handelBlankError = () => {
    if (this.state.first_name == '') {
      this.setState({ isLoading: false, error_message:'Please Enter Full Name', Alert_Visibility:true });
    } else if (this.state.company_size == '') {
      this.setState({ isLoading: false, error_message:'Please Select Company Size', Alert_Visibility:true });
    }  else if (this.state.email == '') {
      this.setState({ isLoading: false, error_message:'Please Enter Your Email', Alert_Visibility:true });
    }  else if (this.state.phone == '') {
      this.setState({ isLoading: false, error_message:'Please Enter Your Phone no.', Alert_Visibility:true });
    }  else if (this.state.org_name == '') {
      this.setState({ isLoading: false, error_message:'Please Enter Your Organisation Name', Alert_Visibility:true });
    } else {
      this.setState({ isLoading: false, error_message:'Server Error', Alert_Visibility:true });
    }
  }


  _onsubmit() {
    if (
      !!this.state.first_name &&
      this.state.company_size &&
      this.state.email &&
      this.state.phone &&
      this.state.org_name
    ) {
      this.setState({ isLoading: true });
      API.post('/onboarding/', {
        name:this.state.first_name,
        country_code:this.state.country_code,
        company_name:this.state.org_name,
        size:this.state.company_size,
        email:this.state.email,
        phone:this.state.phone,
        type:'part',
        device_id:this.state.fcmToken
      }, )
      .then(response => { 
        console.log(response)
        console.log(response.data.status)
        if (response.data.status === "success") {
          this.setState({ isLoading: false })
          Actions.onBoardingOtp({country_code: this.state.country_code,number: this.state.phone,hash: response.data.data.hash,email: this.state.email})
        } else {
          console.log('somthing error');
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
        if (error.response.data.message === "user already signed up") {
          this.setState({ isLoading: false });
          Actions.loginviaotp({phone:this.state.phone, country_code:this.state.country_code})
        } else {
          this.setState({ isLoading: false, error_message:error.response.data.message, Alert_Visibility:true });
          console.log(error.response)
        }
      });
    } else {
      this._handelBlankError()
    }
  }

  render() {
    return (
      <Fragment>
        <LinearGradient
          colors={['#323232', '#14181A', '#000000']}
          style={styles.linearGradient}
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 1 }}>
          <StatusBar translucent backgroundColor="#000000" />
          <SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
          <SafeAreaView style={styles.screen}>
          <Spinner visible={this.state.isLoading} />
            <HeaderComp />
            <View style={styles.body}>


              <Text
                style={{
                  fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
                  color: 'white',
                  fontSize: 45,
                  paddingBottom: 10,
                }}>
                Register
                    {/* <Text
                      style={{
                        color: '#FF9800',
                        fontFamily:
                          Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
                      }}>
                      zi
                    </Text> */}
                {/* Captain */}
              </Text>
              <KeyboardAvoidingView>
                <Text
                  style={{
                    fontSize: 14,
                    letterSpacing: 0.243,
                    color: 'white',
                    opacity: 0.4,
                    lineHeight: 25,
                    paddingBottom: 15,
                    fontFamily:
                      Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
                  }}>
                  Please submit your information and a Fleetzi representative
                  will get in touch with you.
                  </Text>


                <FloatingLabelInput
                  label="Full Name"
                  labelStyle={styles.labelInput}
                  inputStyle={styles.input}
                  style={styles.formInput}
                  value={this.state.first_name}
                  returnKeyType={'next'}
                  blurOnSubmit={false}
                  onSubmitEditing={() => this.refln.getInnerRef().focus()}
                  onChangeText={text =>
                    this.setState({ first_name: text })
                  }></FloatingLabelInput>
                {/* <FloatingLabelInput
                    label="Last Name"
                    labelStyle={styles.labelInput}
                    inputStyle={styles.input}
                    style={styles.formInput}
                    value={this.state.last_name}
                    returnKeyType={'next'}
                    blurOnSubmit={false}
                    ref={r => (this.reffn = r)}
                    onSubmitEditing={() => this.refln.getInnerRef().focus()}
                    onChangeText={text =>
                      this.setState({last_name: text})
                    }></FloatingLabelInput> */}
                <FloatingLabelInput
                  label="Organisation"
                  labelStyle={styles.labelInput}
                  inputStyle={styles.input}
                  style={styles.formInput}
                  value={this.state.org_name}
                  blurOnSubmit={false}
                  returnKeyType={'next'}
                  ref={r => (this.refln = r)}
                  onSubmitEditing={() => this.reforg.getInnerRef().focus()}
                  onChangeText={text =>
                    this.setState({ org_name: text })
                  }></FloatingLabelInput>
                <FloatingLabelInput
                  label="Email Address"
                  labelStyle={styles.labelInput}
                  inputStyle={styles.input}
                  style={styles.formInput}
                  value={this.state.email}
                  blurOnSubmit={false}
                  returnKeyType={'next'}
                  ref={r => (this.reforg = r)}
                  onSubmitEditing={() => this.refemail.getInnerRef().focus()}
                  onChangeText={text =>
                    this.setState({ email: text })
                  }></FloatingLabelInput>
                <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'flex-end'}}>
                  <TouchableOpacity onPress={this._onChangeCountryCode}  style={{borderBottomColor:'#7D7D7D',borderBottomWidth:1,width:'22%',height:40}}>
                    <Text style={{
                      fontSize: 14,
                      color:'#FFFFFF',
                      marginTop:5,
                      fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
                    }}>{this.state.display_country_code}
                    </Text>
                  </TouchableOpacity>
                  <View style={{width:'75%'}}>
                    <FloatingLabelInput
                      label="Phone Number"
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      keyboardType="number-pad"
                      value={this.state.phone}
                      returnKeyType={'done'}
                      blurOnSubmit={false}
                      ref={r => (this.refemail = r)}
                      onChangeText={text =>
                        this.setState({ phone: text })
                      }></FloatingLabelInput>
                  </View>
                </View>



                <View style={{ borderBottomWidth: 1, borderColor: '#333' }}>
                  <Picker
                    selectedValue={this.state.company_size}
                    style={{
                      color: '#6a6b6c',

                      fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
                    }}
                    onValueChange={(itemValue, itemIndex) =>
                      itemValue !== 0 ?
                        this.setState({ company_size: itemValue })
                        : this.setState({ company_size: '' })
                    }
                    itemStyle={{ backgroundColor: "grey", color: "blue", fontFamily: "Nexa Regular" }}>
                    <Picker.Item label='Please select company size' value='0' />
                    {
                      this.state.company_data.map((data) => {
                        return (
                          <Picker.Item key={data.id} label={data.name} value={data.id} />
                        )
                      })
                    }
                  </Picker>
                </View>

              </KeyboardAvoidingView>
              <View style={{ flex: 1, justifyContent: 'flex-end', paddingBottom: 20 }}>
                <View style={{ flexDirection: 'row', width: '100%', }}>
                  <TouchableOpacity onPress={() => Actions.pop()} style={{ width: '20%', backgroundColor: '#222223', height: 50, justifyContent: "center", alignItems: 'center' }}>
                    <Back />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this._onsubmit()} style={{ width: '75%', marginHorizontal: 20 }}>
                    <CTA ctaText={'Sign Up'} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <Modal
                visible={this.state.Alert_Visibility}
                transparent={true}
                animationType={"fade"}
                onRequestClose={() => {this.setState({ Alert_Visibility: false })}} >
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.7)' }}>
                  <View style={styles.MainAlertView}>
                    <View style={{ width: '85%', marginBottom: 50, flexDirection: 'row',alignItems:'center',paddingRight:3}} >
                      <IncorrectOTP />
                      <Text style={{ color: '#000', fontSize: 16, fontWeight: '700',marginHorizontal:10 }}>{this.state.error_message}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', width: '80%', justifyContent: 'flex-end', marginTop: 10 }}>
                      <TouchableOpacity
                        onPress={() => {
                          this.setState({Alert_Visibility:false})
                        }}>
                        <Text style={{
                          fontWeight: '700', fontSize: 16,
                          fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
                        }}>
                          Close
                      </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </Modal>
          </SafeAreaView>
        </LinearGradient>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 10,
    backgroundColor: 'transparent',
  },
  body: {
    flex: 10,
    paddingLeft: 20,
    paddingRight: 20,


  },
  linearGradient: {
    flex: 1,
  },
  labelInput: {
    color: '#7D7D7D',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
  },
  formInput: {
    fontSize: 18,
    borderBottomWidth: 1,
    borderColor: '#333',
    color: '#FFFFFF',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },
  input: {
    borderWidth: 0,
    color: '#FFFFFF',
    fontSize: 14,
  },
  ButtonView: {
    backgroundColor: '#FF8B00',
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#FFEED9',
    borderBottomWidth: 5,
    marginBottom: screenheight * 0.05,
  },
  buttomText: {
    color: '#261400',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
    fontSize: 13,
    marginTop: screenheight * 0.005,
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  MainAlertView: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#FFFFFF",
    height: 200,
    width: '80%',
    borderRadius: 10,
    // borderColor:'red',
    // borderWidth:1
  },
  AlertTitle: {
    fontSize: 18,
    color: "#5ECA94",
    textAlign: 'center',
    padding: 15,
    height: '22%',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular'
  },
  AlertMessage: {
    fontSize: 12,
    color: "#999999",
    textAlign: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    height: '40%',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
    lineHeight: 20,

  },
});
