import React, { Component, Fragment } from 'react';
import {
	Image,
	SafeAreaView,
	Alert,
	StyleSheet,
	Dimensions,
	Picker,
	Platform,
	StatusBar,
	Text,
	TouchableOpacity,
	PixelRatio,
	View,
	ScrollView,
	Button,
	TextInput,
	KeyboardAvoidingView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import HeaderComp from '../component/HeaderComp';
import LinearGradient from 'react-native-linear-gradient';
import RNBootSplash from 'react-native-bootsplash';
import defaultConfig from '../config';
import FloatingLabelInput from '../component/FloatingLabelInput';
import FloatingLabel from 'react-native-floating-labels';
import Pen from '../component/svg/Pen'
import Cross from '../component/svg/Cross';
import CTA from '../component/CTA'
import Plus from '../component/svg/Plus'

(screenwidth = Dimensions.get('window').width),
	(screenheight = Dimensions.get('window').height);
	
export default class Contract extends Component {
	constructor(props) {
		super(props);
		this.state = {
			
		};
	}
	render() {
		return (
			<Fragment>
				<LinearGradient
					colors={['#323232', '#14181A', '#000000']}
					style={styles.linearGradient}
					start={{ x: 0, y: 0 }}
					end={{ x: 1, y: 1 }}>
					<StatusBar translucent backgroundColor="transparent" />
					<SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
					<SafeAreaView style={styles.screen}>
						<View style={styles.container}>
							<View style={styles.headerContainer}>
								<View style={{}}>
									<Text style={styles.header}>
									Contract
								</Text>
								</View>
								<TouchableOpacity onPress={() => Actions.pop()}>
									<Cross />
								</TouchableOpacity>
							</View>
							<ScrollView>
								
							</ScrollView>
							{/* <View style={{ flex: 1, justifyContent: 'flex-end', width: '100%', marginBottom: 10 }}>
								<TouchableOpacity >
									<CTA ctaText={'UPDATE DETAILS'} />
								</TouchableOpacity>
							</View> */}
						</View>
					</SafeAreaView>
				</LinearGradient>
			</Fragment>
		);
	}
}



const styles = StyleSheet.create({
	screen: {
		flex: 10,
		backgroundColor: 'transparent',
	},
	body: {
		flex: 10,
	},
	linearGradient: {
		flex: 1,
	},
	container: {
		flex: 1,
		paddingLeft: 20,
		paddingRight: 20
	},
	header: {
		color: '#FFFFFF',
		fontSize: 20,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
	},
	headerContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: 50,
		width:'70%',
		alignSelf:'flex-end'
	},
	labelInput: {
		color: '#7D7D7D',
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
	},
	formInput: {
		fontSize: 18,
		borderBottomWidth: 1,
		borderColor: '#333',
		color: '#FFFFFF',
		fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
	},
	input: {
		borderWidth: 0,
		color: '#FFFFFF',
		fontSize: 14,
	},
	uploadedBox: {
		height: 90,
		width: 70,
		// borderColor:'red',
		// borderWidth:1,
		borderRadius: 5,
		backgroundColor: '#fff'
	},
	status: {
		marginHorizontal: 20
	},
	statusText: {
		color: '#FF8B00',
		fontSize: 16,
		fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',

	},
	doucumentUpload: {
		flexDirection: 'row',
		// marginTop:20,
		alignItems: 'center'
	},
	uploadBox: {
		height: 90,
		width: 70,
		// borderColor:'red',
		// borderWidth:1,
		borderRadius: 5,
		backgroundColor: '#4A4A4A',
		justifyContent: 'center',
		alignItems: 'center'
	},
	ViewContract: {

	},
	heading: {
		paddingTop: 10
	},
	ViewContractBox: {
		height: 90,
		width: 70,
		// borderColor:'red',
		// borderWidth:1,
		borderRadius: 5,
		backgroundColor: '#fff'
	}

})