/**
* This is the Home Screen
**/

// React native and others libraries imports
import React, { Component, Fragment } from 'react';
import { BackHandler, Image, SafeAreaView, Alert, StyleSheet, Dimensions, Linking, Platform,ActivityIndicator, StatusBar, Text, TouchableOpacity, PixelRatio, View, ToastAndroid } from 'react-native';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Our custom files and classes import
import Header from '../component/Header'
import Footer from '../component/Footer'
import FlatList from '../component/FlatList'
import RNBootSplash from "react-native-bootsplash";
import HeaderComp from "../component/HeaderComp"
import DashboardTwo from "../component/DashboardTwo"
import HomeFooter from '../component/svg/Home'
import Payments from '../component/svg/Payments'
import ListingSVG from '../component/svg/Listing'
import Order from '../component/svg/Order'
import LinearGradient from 'react-native-linear-gradient';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.backCount = 0;
    this.onHandleBackButton = this.handleBackButton.bind(this);
    this.state = {
      isLoading: true,
      ACCESSTOKEN: '',
      h1:'',
      error:'',
      companyuid:''
    };

}

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onHandleBackButton);
    RNBootSplash.hide({ duration: 250 });
    this._retrieveData();
    this.checkPermission();
    this.createNotificationListeners(); //add this line
  }


  _storeData = async (key, data) => {
    try {
      await AsyncStorage.setItem(key, data);
    } catch (error) {
      console.log(error)
      // Error saving data
    }
  };



  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('ACCESSTOKEN');
      if (value !== null) {
        // We have data!!
        var eventstring = new String();
        eventstring = value.replace(/"/g, "");
        this.setState({ ACCESSTOKEN: eventstring })
        this.get_home_data()
      }
    } catch (error) {
      // Error retrieving data
    }
  };


  get_home_data() {
    console.log(this.state.ACCESSTOKEN)
    fetch("http://api.fleetzi.com/v1/", {
      method: 'GET',
      headers: {
        'Authorization': 'Token ' + this.state.ACCESSTOKEN,
        'Content-Type': 'multipart/form-data',
      }
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if (responseJson.status == 'success') {
          this.setState({  isLoading: false, home_data: responseJson.data, companyuid:responseJson.meta.company });
          this._storeData('@companyuid', responseJson.meta.company)
        } else {
          this.setState({ isLoading: false });
          console.log("somthing error");
        }
      })
      .catch((error) => {
        console.error(error);
        console.log("somthing error");
        this.setState({ isLoading: false, error:error.message });
      });
  }

  handleBackButton() {
    console.log(this.props.title)
    if (this.backCount == 1) {
        BackHandler.exitApp();
        this.backCount = 2;
        return; // exit
    }
    else if (this.backCount == 0) {

      // ToastAndroid.show("Press again to exit", ToastAndroid.SHORT);
        this.backCount++;

        setTimeout(() => {
            if (this.backCount != 2)
                this.backCount = 0;
        }, 2000)

        return true;
    }
}


  ////////////////////// Add these methods //////////////////////
  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  //3
  async getToken() {
    // console.log("=================================================");
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    // console.log(fcmToken);
    this.setState({ fcmToken: fcmToken })
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      // console.log("=================================================");
      if (fcmToken) {
        // console.log("=================================================");
        console.log(fcmToken);
        this.setState({ fcmToken: fcmToken })
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }


  //Remove listeners allocated in createNotificationListeners()
  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
    this.messageListener();
    Linking.removeEventListener('url', this.handleOpenURL);
  }

  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const { title, body } = notification;
      // this.showAlert(title, body);
    });

    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      // console.log("app is in background")
      const { title, body } = notificationOpen.notification;
      // this.showAlert(title, body);
    });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    const data = Platform.OS === 'android' ? notificationOpen.notification.data.customDataKeyName : notificationOpen.customDataKeyName;
    if (notificationOpen) {
      const notification = notificationOpen.notification;
      // console.log("ye han actinon")
      console.log(notification)
    }
    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      console.log("app is in payload ")
      console.log(JSON.stringify(message));
    });
  }
  // handel URL Deeplinking
  handleOpenURL(event) {
    this.navigate(event.url);
  }

  // Handel Deeplink navigation
  navigate = (url) => { // E
    if (url) {
      const route = url.replace(/.*?:\/\//g, '');
      console.log(route)
    }
  }

  showAlert(title, body) {
    Alert.alert(
      title, body,
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false },
    );
  }

  render() {
    if (this.state.isLoading) {
      return (
        <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}>
        {/* <View style={styles.container}> */}
          <ActivityIndicator
            color='#bc2b78'
            size="large"
            style={styles.activityIndicator} />
        {/* </View> */}
        </LinearGradient>
      )
    }
    return (
      <Fragment>
        <StatusBar translucent backgroundColor="transparent" />
        <SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
        <SafeAreaView style={styles.screen}>
            {/* <Header text='Title/Logo'>
                <Text style={{color:'red'}}>Hellop</Text>
            </Header> */}
            <View style={styles.body}>
                {/* <FlatList /> */}
                <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{x:0, y: 0}}
          end={{x: 1, y: 1}}>
             <HeaderComp />  
                <DashboardTwo home_data={this.state.home_data} companyuid={this.state.companyuid}  />
          </LinearGradient>
              
            </View>
            {/* <Footer index='home'/> */}
          
        </SafeAreaView>
        </Fragment>
    );
  }


}

const styles = StyleSheet.create({
    screen: {
      flex: 10,
    },
    body: {
      flex: 10
    },
    linearGradient:{
      flex:1
    },
    activityIndicator: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      height: 80
    },
  });