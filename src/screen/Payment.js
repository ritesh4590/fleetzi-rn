/**
* This is the Listing Home  Screen
**/

// React native and others libraries imports
import React, { Component, Fragment } from 'react';
import { Image, SafeAreaView, Alert, StyleSheet, Dimensions, Linking, Platform, StatusBar, Text, TouchableOpacity, PixelRatio, View } from 'react-native';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';

// Our custom files and classes import
import Header from '../component/Header'
import Footer from '../component/Footer'
import FlatList from '../component/FlatList'
import ListingMain from "../component/ListingMain";
import HeaderComp from "../component/HeaderComp"
import Payment from "../component/Payment"
import HomeFooter from '../component/svg/Home'
import Payments from '../component/svg/Payments'
import ListingSVG from '../component/svg/Listing'
import Order from '../component/svg/Order'
import LinearGradient from 'react-native-linear-gradient';

export default class Listing extends Component {


  render() {
    return (
      <Fragment>
        <StatusBar translucent backgroundColor="transparent" />
        <SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
        <SafeAreaView style={styles.screen}>
            <View style={styles.body}>
                
                <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{x:0, y: 0}}
                  end={{x: 1, y: 1}}>
                <HeaderComp />  
                <Payment 
                  monthText='Month'
                  monthYear='June 2020'
                  upperText='Total Amount Received for 3 equipment   '
                  price='9500'
                  priceUnit='SAR'
                  bottomText='Click to see the amount receivable per item'
                />
          </LinearGradient>
              
            </View>
            {/* <Footer index='payment'/> */}
                 
        </SafeAreaView>
        </Fragment>
    );
  }


}

const styles = StyleSheet.create({
    screen: {
      flex: 10,
   
    },
    body: {
      flex: 10
    },
    linearGradient:{
        height:'100%'
    }
  });