/**
* This is the Listing Home  Screen
**/

// React native and others libraries imports
import React, { Component, Fragment } from 'react';
import { BackHandler,Image, SafeAreaView, Alert, StyleSheet, Dimensions, Linking, Platform, StatusBar, Text, TouchableOpacity, PixelRatio, View } from 'react-native';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';

// Our custom files and classes import
import Header from '../component/Header'
import Footer from '../component/Footer'
import FlatList from '../component/FlatList'
import ListingMain from "../component/ListingMain";
import HeaderComp from "../component/HeaderComp"
import DashboardTwo from "../component/DashboardTwo"
import HomeFooter from '../component/svg/Home'
import Payments from '../component/svg/Payments'
import ListingSVG from '../component/svg/Listing'
import Order from '../component/svg/Order'
import LinearGradient from 'react-native-linear-gradient';

export default class Listing extends Component {
  constructor(props) {
    super(props);
    this.onHandleBackButton = this.handleBackButton.bind(this);
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onHandleBackButton);
  }

  handleBackButton() {
    Actions.pop()
}

  render() {
    return (
      <Fragment>
        <StatusBar translucent backgroundColor="transparent" />
        <SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
        <SafeAreaView style={styles.screen}>
            <View style={styles.body}>
                <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{x:0, y: 0}}
          end={{x: 1, y: 1}}>
                <HeaderComp />  
                <ListingMain/>
          </LinearGradient>
              
            </View>
            {/* <Footer index='listing' /> */}
            
        </SafeAreaView>
        </Fragment>
    );
  }


}

const styles = StyleSheet.create({
  linearGradient:{
    // height:'100%'
    flex:1
    },
    screen: {
      flex: 10,
      // backgroundColor:'#3e3e3e'
    },
    body: {
      flex: 10
    },
    iconBox:{
      backgroundColor:'#1A1D1C',
      height:80,
      width:70,
      paddingTop:15,
      borderRadius:5
    }
  });