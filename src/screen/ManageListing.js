/**
* This is the Create Listing Home  Screen
**/

// React native and others libraries imports
import React, { Component, Fragment } from 'react';
import { BackHandler,Image, SafeAreaView, Alert, StyleSheet, ScrollView, Linking, Platform, StatusBar, Text, TouchableOpacity, ActivityIndicator, View } from 'react-native';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';

// Our custom files and classes import
import Footer from '../component/Footer'
import ManageListingComp from "../component/ManageListingComp";
import HeaderComp from "../component/HeaderComp"
import LinearGradient from 'react-native-linear-gradient';
import Back from '../component/svg/back'


export default class ManageListing extends Component {
  constructor(props) {
    super(props);
    this.backCount = 0;
    this.onHandleBackButton = this.handleBackButton.bind(this);
    this.state = {
      isLoading: true,
      ACCESSTOKEN: '',
      AllData: [],
      defaultImage:''
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onHandleBackButton);
    this._retrieveData();
  }

  handleBackButton() {
    Actions.listing()
}

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('ACCESSTOKEN');
      if (value !== null) {
        // We have data!!
        var eventstring = new String();
        eventstring = value.replace(/"/g, "");
        this.setState({ ACCESSTOKEN: eventstring })
        this.get_managelist_data()
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  get_managelist_data() {
    fetch("http://34.87.55.15/api/v1/marketplace/listing/?format=json", {
      method: 'GET',
      headers: {
        'Authorization': 'Token ' + this.state.ACCESSTOKEN,
        'Content-Type': 'multipart/form-data',
      }
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson.data)
        if (responseJson.status == 'success') {
          this.setState({ AllData: responseJson.data, isLoading: false });
        } else {
          this.setState({ isLoading: false });
          console.log("somthing error");
        }
      })
      .catch((error) => {
        console.error(error);
        console.log("somthing error");
        this.setState({ isLoading: false });
      });
  }

  _renderdata() {
    return this.state.AllData.map((data) => {
      return (
        <ManageListingComp data={data} />
      )
    })
  }


  render() {
    var listdata = this.state.AllData
    console.log(listdata.length)
    if (this.state.isLoading) {
      return (
        <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}>
        {/* <View style={styles.container}> */}
          <ActivityIndicator
            color='#bc2b78'
            size="large"
            style={styles.activityIndicator} />
        {/* </View> */}
        </LinearGradient>
      )
    }
    return (
      <Fragment>
        {/* <StatusBar translucent backgroundColor="transparent" /> */}
        <SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
        <SafeAreaView style={styles.screen}>
          <View style={styles.body}>
            <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 1 }}>
              {/* <HeaderComp />  */}
              {/* <View style={{ marginTop: '15%' }}>   */}
              <View style={styles.createNewListingDiv}>
                <TouchableOpacity onPress={() => Actions.listing()}><Back /></TouchableOpacity>
                <Text style={styles.newListingText}>Manage Listing</Text>
              </View>
              <ScrollView >
                {
                  listdata.length <= 0 ?
                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                      <Text style={{ color: 'white', fontSize: 20 }}>Opps No Data Found</Text>
                    </View>
                    :
                    this._renderdata()
                  //   <ManageListingComp />
                }
              </ScrollView>
              {/* </View> */}
            </LinearGradient>

          </View>
          {/* <Footer index='listing'/> */}

        </SafeAreaView>
      </Fragment>
    );
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 70,
    backgroundColor:'#333'
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80
  },
  linearGradient: {
    flex: 1
  },
  screen: {
    flex: 10,
  },
  body: {
    flex: 10
  },
  createNewListingDiv: {
    flexDirection: 'row',
    marginBottom:20,
    paddingLeft:20,
    paddingRight:20,
    marginTop:50
  },
  newListingText: {
    color: '#FFFFFF',
    fontSize:20,
    marginHorizontal: 10,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'

  },
});