/**
* This is the Login Page
**/

// React native and others libraries imports
import React, { Component, Fragment } from 'react';
import { Actions } from 'react-native-router-flux';
import { ActivityIndicator, StyleSheet, Dimensions, Modal, Text, View, TouchableHighlight, SafeAreaView, StatusBar, Image, TouchableWithoutFeedback, TouchableOpacity, Picker,TextInput, Button} from 'react-native';
import { Col, Grid, Row } from "react-native-easy-grid";
import RNBootSplash from "react-native-bootsplash";
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
const DeviceWidth = Dimensions.get('window').width;

export default class Login extends Component {
  constructor(props) {
      super(props);
      this.state = {
        isLoading: false,
        username: '',
        password: '',
        hasError: false,
        errorText: '',
        screenroute: '',
        phoneNumber: '',
        country_code: '+91',
        CountryData:[],
        currentFlag:"🇮🇳",
        modalVisible:false,
      };
  }
  

  componentWillMount() {
    this.setState({screenroute: this.props.screenroute});
  }


  onChangeText(key, value) {
    this.setState({
      [key]: value
    })
  }

  onValueChange2(value: string) {
    this.setState({
      selected2: value
    });
  }

  SelectCountry(item){
      // console.log(item)
      this.setState({
        country_code: item.dial_code,
        currentFlag:item.flag,
        codeModal: false
      })
  }

  onSearchChange(event){
    // console.log(event);
    let results = Country.filter(Country => Country['name'].toLowerCase().startsWith(event.toLowerCase()));
    this.setState({
      CountryData: results,
    })
    
  }
//   Function To Hide The Modal
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  componentDidMount(){
    RNBootSplash.hide({ duration: 250 });
  }


  render() {
    // console.log(Country)
    return(
        <Fragment>
         <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{x:0, y: 0}}
          end={{x: 1, y: 1}}>
        <StatusBar translucent backgroundColor="transparent" />
        <SafeAreaView style={{ flex: 0, backgroundColor: 'transparent' }} />
        <SafeAreaView style={styles.screen}>
            
            <View style={styles.body}>
                {/* <View style={{marginTop:40, height:'50%'}}>
                    <View style={{marginRight:20}}>
                      <Image source={require('../../assets/Images/Logo.png')} />
                    </View>
                </View> */}
                <View style={{paddingLeft:20, paddingRight:20}}>
                  <Image source={require('../../assets/Images/Logo.png')} resizeMode="contain" style={{width:DeviceWidth-50,}} />
                </View>
              
                <View style={styles.mobLoginFormClass}>
                    <Grid>
                    <Col size={30}>
                        <TouchableOpacity onPress={() => this.setState({ modalVisible: true })}>
                        <View  style={styles.code_style}>
                        <Text style={{fontSize:20,textAlign:'center',fontFamily: 'SourceSansPro-Regular', color:'white'}}>{this.state.currentFlag} {this.state.country_code}</Text>
                        {/* <Icon active name='ios-arrow-down' style={{color: "#333",fontSize:12,paddingTop:5,paddingLeft:5}} /> */}
                        </View>
                        </TouchableOpacity>
                    </Col>
                    <Col size={70}>
                        <View>
                        <TextInput  
                                placeholder='phone number'
                                placeholderTextColor='#adb4bc'
                                keyboardType={'phone-pad'}
                                returnKeyType='done'
                                autoCapitalize='none'
                                autoCorrect={false}
                                secureTextEntry={false}
                                style={styles.inputStyle}     
                                placeholderStyle={styles.inputStyle}
                                onChangeText={(text) => this.setState({phoneNumber: text})}              
                            />
                        
                    
                        <TextInput
                            inlineImageLeft='search_icon'
                            keyboardType={'phone-pad'}
                            />
                        </View>
                    </Col>
                    </Grid>
                    <Modal transparent={true}
                        animationType="slide"
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            Alert.alert('Modal has been closed.');
                        }}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'center',
                            alignItems: 'center'}}>
                            <View style={{height: 200, width:DeviceWidth, backgroundColor:'rgba(52, 52, 52, 0.8)', position:'absolute', bottom:0}}>
                                <Grid>
                                    <Col size={80} style={{marginTop:10, marginLeft:15}}>
                                        <TouchableHighlight
                                            onPress={() => {
                                                this.setState({
                                                    country_code:'+91',
                                                    currentFlag:"🇮🇳",
                                                    modalVisible:false
                                                })
                                            }}>
                                            <View style={{flexDirection:'row', marginTop:10}}>
                                                <Text style={{fontSize:20}}>🇮🇳</Text>
                                                <Text style={{fontSize:25, paddingLeft:10, color:'white'}}>+91</Text>
                                            </View>
                                        </TouchableHighlight>
                                        <TouchableHighlight
                                            onPress={() => {
                                                this.setState({
                                                    country_code:'+966',
                                                    currentFlag:"🇸🇦",
                                                    modalVisible:false
                                                })
                                            }}>

                                            <View style={{flexDirection:'row'}}>
                                                <Text style={{fontSize:20}}>🇸🇦</Text>
                                                <Text style={{fontSize:25, paddingLeft:10, color:'white'}}>+966</Text>
                                            </View>
                                            </TouchableHighlight>
                                    </Col>
                                    <Col size={20} style={{marginTop:10}}>
                                        <TouchableHighlight
                                            onPress={() => {
                                            this.setModalVisible(!this.state.modalVisible);
                                            }}>
                                            <Text style={{color:'white'}}>Close Modal</Text>
                                        </TouchableHighlight>
                                    </Col>
                                </Grid>
                            </View>
                        </View>
                    </Modal>
                </View> 
                <View style={{justifyContent:'center', alignContent:'center', alignItems:'center'}}>
                  {
                    !this.state.isLoading ?
                    <Button
                      title="Get OTP"
                      onPress={() => this.get_otp()}
                      color='#FFAC00'
                      style={{width:100}}
                    />
                    :
                    <View style={{marginTop: 20}}>
                      <ActivityIndicator size="large" color="#FFAC00" />
                    </View>
                  }
                </View>
            </View>
        </SafeAreaView>
        </LinearGradient>
        </Fragment>             
    );
  }

  get_otp(){
    if (!!this.state.phoneNumber && this.state.country_code) {
      this.setState({isLoading: true});
      let formdata = new FormData();
      formdata.append("country_code", this.state.country_code);
      formdata.append("number", this.state.phoneNumber);
      formdata.append("platform", 'part');
      fetch("http://34.87.55.15/api/v1/token/login/otp/request-sms/?format=json", {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formdata
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if (responseJson.status == 'success') {
          this.setState({isLoading: false});
          Actions.onBoardingOTPpage({country_code:this.state.country_code, number:this.state.phoneNumber, hash:responseJson.data.hash})
        } else {
          this.setState({isLoading: false});
          console.log("somthing error");
        }
      })
      .catch((error) => {
        console.error(error);
        console.log("somthing error");
        this.setState({isLoading: false});
      });
     } else {
      this.setState({isLoading: false, hasError: true, errorText: 'Enter your phone no.!'});
     }

  }

}

const styles = StyleSheet.create({
    screen: {
        flex: 10,
        backgroundColor:'transparent'
      },
      body: {
        flex: 10,
        justifyContent:'center',
      },
      linearGradient:{
        flex:1
        },
  enterNumberClass: {
    paddingTop: 20,
    paddingRight: 0,
    paddingBottom: 20,
    paddingLeft: 0,
    color: "#0d5435",
    textAlign: "center"
  },
  mobLoginFormClass: {
    // position: 'absolute',
    left: 0,
    right: 0,
    // height: 200,
    // bottom: 250,
    // paddingTop: '30%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
    marginTop:15
  },
  footer: {
    backgroundColor: '#006340',
    width: DeviceWidth,
    height: 60,
    justifyContent: 'center', alignItems: 'center'
  },
  activeTabStyle: {
    borderRadius: 3,
    backgroundColor: '#c0b394',
  },
  footericontext: {
    color: 'white',
    fontFamily: 'SourceSansPro-Regular',
    fontSize: 18,
    marginTop: 0,
  },
  codecontent: {
    backgroundColor: 'white',
    height: '60%',
    width: '100%',
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  code_style:{
    borderWidth:1,
    borderBottomColor:'#c0b394',
    borderLeftWidth:0,
    borderTopWidth:0,
    borderRightWidth:0,
    height:50,
    marginRight:5,
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center'
  },
  inputStyle:{
    borderWidth:1,
    borderBottomColor:'#c0b394',
    borderLeftWidth:0,
    borderTopWidth:0,
    borderRightWidth:0,
    height:50,
    paddingLeft:5,
    fontFamily: 'SourceSansPro-Regular',
    fontSize:16,
    color: 'white'
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  },
});
