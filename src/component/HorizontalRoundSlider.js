/**
* This is the Horizontal Round Slider Component
**/

// React native and others libraries imports
import React from 'react';
import { View, Text, StyleSheet, Platform, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';


const Footer = (props) => {

  return (
    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ marginBottom: 10, marginTop: 10 }} >
        <View key={index} style={{ justifyContent: 'space-between', marginLeft: 15 }}>
          <TouchableOpacity activeOpacity={0.9} onPress={this._onPress.bind(this, item.slug, item.name)} >
            <View style={index == 0 ? styles.papathumbstyle : styles.papathumbstyle2}>
                <Image source={{uri:"https://d1n359tvru89v5.cloudfront.net/310x455/tj-pco-01-166-small_image.png"}} style={{width:20, height:20}} /> 
              {/* <Thumbnail style={index == 0 ? styles.thumbstyle : styles.thumbstyle2} circle={true} source={{ uri: image }} /> */}
              <ProgressiveImage
                // thumbnailSource={{ uri: `https://d1n359tvru89v5.cloudfront.net/310x455/tj-pco-01-166-small_image.png` }}
                source={{ uri: image }}
                style={index == 0 ? styles.thumbstyle : styles.thumbstyle2}
                resizeMode="cover"
                radius={50}
              />
            </View>
            <Text style={styles.text}>{item.name}</Text>
          </TouchableOpacity>
        </View>
    </ScrollView>
  );

}

const styles = StyleSheet.create({
    footer: {
        flex: Platform.OS === 'ios' ? 1 : 1.5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft:10,
        paddingRight:10
        // backgroundColor: '#3e3e3e'
      },
  });
  

export default Footer;