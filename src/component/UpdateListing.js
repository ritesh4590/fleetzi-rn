import React from 'react';
import { Col, Row, Grid } from "react-native-easy-grid";
import { View, Text, StyleSheet, Platform, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import HeaderComp from './HeaderComp'
import Back from "./svg/back"
import Pen from './svg/Pen'
import Dropdown from './svg/Dropdown'
import Plus from "./svg/Plus"

class UpdateListing extends React.Component{
    render(){
      return (
        <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{x:0, y: 0}}
          end={{x: 1, y: 1}}>
          <View style={styles.container}>
          <HeaderComp/>
          <View style={styles.createNewListingDiv}>
           <View style={{marginTop:5}}><Back/></View> 
            <Text style={styles.newListingText}>Update Listing</Text>
          </View>
          <Text>{'\n'}</Text>
            <ScrollView>
              <View style={styles.topHeading}>
                <Text style={styles.listingOne}>Listing 1</Text>
                <View>
                <Pen/>
                </View>
              </View>
              <View style={styles.borderBottomListing}></View>
              <View style={styles.div1}>
              <View style={styles.Innerdiv1}>
                <Text style={styles.title}>Select Brand</Text>
                <View style={styles.TextAndDropdown}>
                  <Text style={styles.boldText}>CAT</Text>
                  <View><Dropdown/></View>
                </View>
              </View>
              </View>
              <View style={styles.div1}>
              <View style={styles.Innerdiv1}>
                <Text style={styles.title}>Select Make and model</Text>
                <View style={styles.TextAndDropdown}>
                  <Text style={styles.boldText} numberOfLines={1}>740 B Dump Trunckttttttttt </Text>
                  <View><Dropdown/></View>
                </View>
              </View>
              </View>
              <View style={styles.div1}>
              <View style={styles.Innerdiv1}>
                <Text style={styles.title}>Add specs</Text>
                <View style={styles.TextAndDropdown}>
                  <Text style={styles.boldText}>40 Ton </Text>
                  <View><Dropdown/></View>
                </View>
              </View>
              </View>
              <View style={styles.div2}>
              <View style={styles.Innerdiv2}>
              <Text style={styles.title}>Upload Pictures</Text>
              <View style={styles.row1}>
                <View  style={styles.col1}></View>
                <View  style={styles.col1}></View>
                <View  style={styles.col1}></View>
                <View  style={styles.col1}></View>
                <View  style={styles.col1}></View>
                
                <View  style={styles.col1Dark}>
                  <Plus/>
                </View>
                
              </View>
             
              </View>
              </View>
              <View style={styles.div2}>
              <View style={styles.Innerdiv2}>
              <Text style={styles.title}>Upload Documents</Text>
              <View style={styles.row1}>
                <View  style={styles.col1}></View>
                <View  style={styles.col1}></View>
                <View  style={styles.col1}></View>
                <View  style={styles.col1}></View>
              
                
                <View  style={styles.col1Dark}>
                  <Plus/>
                </View>
                
              </View>
             
              </View>
              </View>
              
            </ScrollView>
          </View>
          </LinearGradient>
      )}
}

export default UpdateListing


const styles = StyleSheet.create({
    linearGradient:{
        height:'100%'
        },
         container:{
            width:'90%',
            height:'100%',
            marginTop: 0,
            marginRight: 'auto',
            marginBottom: 0,
            marginLeft: 'auto',
        },
        createNewListingDiv:{
            flexDirection:'row',
            // marginHorizontal:20
            // justifyContent:'space-between',
            // width:'70%',
           
        },
        newListingText:{
            color:'white',
            fontSize:23,
            marginHorizontal:10

        },
        topHeading:{
          flexDirection:'row',
          justifyContent:'space-between'
        },
        listingOne:{
          color:'white',
          fontSize:14
        },
        borderBottomListing:{
          borderBottomColor:'#595959',
          borderBottomWidth:1,
          marginTop:10,
          opacity:.5
        },
        div1:{
          height:85,
          backgroundColor:'#28292B',
          marginTop:15,
          borderRadius:3
        },
        TextAndDropdown:{
          justifyContent:'space-between',
          flexDirection:'row',
        },
        title:{
          color:'#FF8B00'
        },
        boldText:{
          width:'85%',
          fontSize:26,
          color:'white',
          overflow:'hidden',
          
        },
        Innerdiv1:{
          paddingTop:15,
          paddingRight:20,
          paddingLeft:20
          // padding:10
        },
        Innerdiv2:{
          width:'100%',
          paddingTop:15,
          paddingRight:20,
          paddingLeft:20
          // padding:10
        },
        div2:{
          backgroundColor:'#28292B',
          marginTop:15,
          borderRadius:3,
          paddingBottom:15
        },
        row1:{
          marginTop:10,
          flexDirection:'row',
          flexWrap:'wrap'
        },
        col1:{
          height:70,
          width:60,
          backgroundColor:'#D8D8D8',
          borderRadius:3,
          margin:5
        },
        col1Dark:{
          height:70,
          width:60,
          backgroundColor:'#979797',
          borderRadius:3,
          margin:5,
          justifyContent: 'center',
          alignItems: 'center',
        },
        // plusSVG:{
        //   flex:1,
        //   justifyContent: 'center',
        // alignItems: 'center',
        // }
    })