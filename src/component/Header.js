import React from 'react';
import { View, Text, StyleSheet, Platform } from 'react-native';

const Header = (props) => {

  return (
    <View style={styles.header}>
      <Text style={styles.header_text}>{props.text}</Text>
      { props.children }
    </View>
  );

}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // marginTop: 0,
        // paddingTop: 10,
        backgroundColor: 'transparent'
      },
      header_text: {
        fontWeight: 'bold',
        color: '#FFF',
        fontSize: 17,
        textAlign: 'center',
        padding: 10
      }
  });
  

export default Header;