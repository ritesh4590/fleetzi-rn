import React from 'react'
import { View, Text, StyleSheet, Platform, Dimensions, TouchableOpacity } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
(screenwidth = Dimensions.get('window').width),
(screenheight = Dimensions.get('window').height);

class CTA extends React.Component {
	render() {
		return (
				<View>
					<LinearGradient
						colors={['#FFAC00', '#FF8B00']}
						style={styles.ButtonView}
						start={{ x: 0, y: 0 }}
						end={{ x: 1, y: 1 }}>
						<Text style={styles.buttomText}>{this.props.ctaText}</Text>
					</LinearGradient>
				</View>
		)
	}
}
export default CTA

const styles = StyleSheet.create({
	ButtonView: {
		backgroundColor: '#FF8B00',
		width: '100%',
		height: 50,
		justifyContent: 'center',
		alignItems: 'center',
		opacity: 0.85,
	},
	buttomText: {
		color: '#261400',
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
		fontSize: 14,
	},

})

