import React from 'react'
import { View, Text, StyleSheet, Image, Dimensions, ScrollView, ImageBackground, TouchableOpacity } from 'react-native'
import { Col, Row, Grid } from "react-native-easy-grid";
import Svg, { SvgUri, G, Path, Def, Circle, Stop, Rect } from 'react-native-svg';
import LinearGradient from 'react-native-linear-gradient';
import { Actions } from 'react-native-router-flux';
import HeaderComp from "./HeaderComp"
import Bell from './svg/bell'
import Calendar from './svg/calendar'
import Chevron from './svg/chevron'
import Icon1 from './svg/icon1'
import Icon2 from './svg/icon2'
import Icon3 from './svg/icon3'
import Icon4 from './svg/icon4'
import InactiveSvg from '../component/svg/InactiveSvg'
import PendingComp from '../component/svg/Pending'
import Verify from '../component/svg/verify'
import ApprovalWaiting from '../component/svg/ApprovalWaiting'
import PhoneNoSvg from '../component/svg/PhoneNoSvg'
import NewListingSvg from '../component/svg/NewListingSvg'
import CreatePasswordSvg from '../component/svg/CreatePasswordSvg';
import BankDetailsSvg from '../component/svg/BankDetailsSvg';
import BusinessSvg from '../component/svg/BusinessSvg';
import VatSvg from '../component/svg/VatSvg';
import PendingApproval from '../component/svg/pendingApproval'
import CompanyAddress from '../component/svg/CompanyAddress'
import CompanyDetails from '../component/svg/CompanyDetails'


screenWidth = Dimensions.get('window').width,
screenHeight = Dimensions.get('window').height
class DashboardTwo extends React.Component {
  rendersvg(index) {
    if (index === 1) {
      return (
        <Icon1 />
      )
    }
    if (index === 2) {
      return (
        <Icon2 />
      )
    }
    if (index === 3) {
      return (
        <Icon3 />
      )
    }
    if (index === 4) {
      return (
        <Icon4 />
      )
    }
    if (index === 5) {
      return (
        <Icon2 />
      )
    }
    if (index === 6) {
      return (
        <Icon1 />
      )
    }
    if (index === 7) {
      return (
        <Icon4 />
      )
    }
  }

  _getlink(data) {
    if ((data.action === "address") && (data.status === "pending" || data.status === "pending-approval")) {
      Actions.CompanyAddress({companyuid:this.props.companyuid})
    } else if ((data.action === "company-info") && (data.status === "pending" || data.status === "pending-approval")) {
      Actions.CompanyDetails({companyuid:this.props.companyuid})
    } else if ((data.action === "bank-detail") && (data.status === "pending" || data.status === "pending-approval")) {
      Actions.BankDetails({companyuid:this.props.companyuid})
    } else if ((data.action === "doc-upload") && (data.status === "pending" || data.status === "pending-approval")) {
      Actions.BusinessCertificate({companyuid:this.props.companyuid})
    } else {
      return({})
    }
  }

  _getIcon(data){
    if (data.icon === "dollar") {
      return(<BankDetailsSvg />);
    } else if(data.icon === "postcard") {
      return(<PhoneNoSvg />);
    } else if(data.icon === "info") {
      return(<CompanyDetails />);
    } else if(data.icon === "doc") {
      return(<BusinessSvg />);
    } else {
      return(<CompanyDetails />);
    }
  }

  _setData(data, index) {
    if (data.widget === 'h1') {
      return(<Text key={index} style={styles.heading_text}>{data.data}</Text>)
    }
    if (data.widget === 'verification-widget') {
      return(
        <TouchableOpacity style={styles.passwordView} key={index}
          onPress={() => this._getlink(data.data)} >
          <View style={{flexDirection:'row',alignItems:'center'}}>
            {this._getIcon(data.data)}
            <Text style={styles.passwordText}>{data.data.text}</Text>
          </View>
          <View>
          {data.data.status==='verified'? <Verify /> : data.action === "pending" ? <PendingComp/> : <PendingApproval />}
          </View>
        </TouchableOpacity>
      )
    }
    if (data.widget === "rm-widget") {
      return(
          <TouchableOpacity style={styles.outerView} onPress={() => Actions.onBoardingOptionspage()}  key={index}>
            <ImageBackground style={styles.backgroundImageSty} resizeMode='stretch' source={require('../../assets/Images/Rectangle.png')}>
              <View style={styles.representativeViewMain}>
                <View style={styles.do_you_know_text}>
                  <Text style={styles.small_text_do}>
                    A Fleetzi partner manager will call you shortly for activating your account.
            </Text>
                </View>

                <View style={styles.representativeView}>

                  <View style={{ width: '60%' }}>
                    <Text style={styles.representativeManager} >Partner Manager</Text>
                    <Text style={styles.representativeName} >Mr. Abdullah Kareem  </Text>

                  </View>
                  <View style={{}} >
                    <Image source={require('../../assets/Images/ankit.jpeg')} style={styles.cirularImage} />
                  </View>

                </View>

                <View style={{ marginTop: screenHeight * .04 }}>
                  <View style={{}}>
                    <Text style={styles.representativeManager}>Phone Number</Text>
                    <View style={{ flexDirection: 'row', alignItems: "center" }}>
                      <Text style={styles.representativeName}>{data.data.phone}</Text>
                      <View style={{ marginLeft: 10 }}>
                        <PhoneNoSvg />
                      </View>
                    </View>
                  </View>
                </View>
                <View style={styles.contractSigningView}>
                  <Text style={styles.contractSigning} >Account Status</Text>
                  <InactiveSvg />
                </View>
              </View>
            </ImageBackground>
          </TouchableOpacity>
      )
    }
    if (data.widget === "create-listing") {
      return (
        <TouchableOpacity key={index} style={{ backgroundColor: '#181C1D', width: '100%', padding: 10, paddingTop: 15, paddingBottom: 15, borderRadius: 4 }} onPress={() => Actions.createlisting()} >
            <Grid style={{ alignItems: 'center' }}>
              <Col size={87} style={styles.col1}>
                <Text style={styles.mainHeading}>Create your first listing</Text>

                <Text style={styles.subHeading}>To start using Dashboard</Text>
              </Col>
              <Col size={13} style={styles.col2}>
                <View>
                  <View style={styles.chevronStyle}></View>
                  <View style={styles.chevronStyleSVG}><Chevron /></View>
                </View>

              </Col>

            </Grid>
          </TouchableOpacity>
      )
    }
  }


  render() {
    const WidgetData = this.props.home_data
    const DATA = [
      {
        heading: 'Tjori_BOX1',
        subHeading: 30000,
        unit: 'SAR',
        key: 1,
      },
      {
        heading: 'Tjori_BOX2',
        subHeading: 30000,
        unit: 'SAR',
        key: 2,
      },
      {
        heading: 'Tjori_BOX3',
        subHeading: 30000,
        unit: 'SAR',
        key: 3
        ,
      },
      {
        heading: 'Tjori_BOX4',
        subHeading: 30000,
        unit: 'SAR',
        key: 4,
      },
      {
        heading: 'Tjori_BOX5',
        subHeading: 30000,
        unit: 'SAR',
        key: 5,
      },
      {
        heading: 'Tjori_BOX6',
        subHeading: 30000,
        unit: 'SAR',
        key: 6,
      },
    ];
    return (
      <ScrollView>
        <View style={styles.body_inner_contaioner}>
        {WidgetData.map((data, index) => { return ( this._setData(data, index))}) }
          {/* <TouchableOpacity
            style={styles.passwordView}
            onPress={() => Actions.CompanyDetails()}>
            <View style={{flexDirection:'row',alignItems:'center'}}>
              <CompanyDetails/>
              <Text style={styles.passwordText}>Update Company Details</Text>
            </View>
            <View>
            <Verify />
            </View>
          </TouchableOpacity> */}
          {/* <TouchableOpacity
            style={styles.passwordView}
            onPress={() => Actions.CompanyAddress()}>
            <View style={{flexDirection:'row',alignItems:'center'}}>
              <CompanyAddress />
              <Text style={styles.passwordText}>Update Company Address</Text>
            </View>
            <View>
            <ApprovalWaiting/>
            </View>
          </TouchableOpacity> */}

          {/* <TouchableOpacity style={styles.passwordView}>
            <View style={{flexDirection:'row',alignItems:'center'}}>
            <BusinessSvg />
            <Text style={styles.passwordText}>Upload business certificate</Text>
            </View>
            <View>
            <PendingComp/>
            </View>
          </TouchableOpacity> */}

          {/* <TouchableOpacity
            style={styles.passwordView}
            onPress={() => Actions.BankDetails()}>
            <View style={{flexDirection:'row',alignItems:'center'}}>
              <BankDetailsSvg />
              <Text style={styles.passwordText}>Upload bank details</Text>
            </View>
            <View>
            <PendingComp/>
            </View>
          </TouchableOpacity> */}

          {/* <TouchableOpacity style={styles.outerView} onPress={() => Actions.onBoardingOptionspage()}>
            <ImageBackground style={styles.backgroundImageSty} resizeMode='stretch' source={require('../../assets/Images/Rectangle.png')}>
              <View style={styles.representativeViewMain}>
                <View style={styles.do_you_know_text}>
                  <Text style={styles.small_text_do}>
                    A Fleetzi partner manager will call you shortly for activating your account.
            </Text>
                </View>

                <View style={styles.representativeView}>

                  <View style={{ width: '60%' }}>
                    <Text style={styles.representativeManager} >Partner Manager</Text>
                    <Text style={styles.representativeName} >Mr. Abdullah Kareem  </Text>

                  </View>
                  <View style={{}} >
                    <Image source={require('../../assets/Images/ankit.jpeg')} style={styles.cirularImage} />
                  </View>

                </View>

                <View style={{ marginTop: screenHeight * .04 }}>
                  <View style={{}}>
                    <Text style={styles.representativeManager}>Phone Number</Text>
                    <View style={{ flexDirection: 'row', alignItems: "center" }}>
                      <Text style={styles.representativeName}>55 335 6988</Text>
                      <View style={{ marginLeft: 10 }}>
                        <PhoneNoSvg />
                      </View>
                    </View>
                  </View>
                </View>
                <View style={styles.contractSigningView}>
                  <Text style={styles.contractSigning} >Account Status</Text>
                  <InactiveSvg />
                </View>
              </View>
            </ImageBackground>

          </TouchableOpacity> */}
          {/* <TouchableOpacity style={{ backgroundColor: '#181C1D', width: '100%', padding: 10, paddingTop: 15, paddingBottom: 15, borderRadius: 4 }} onPress={() => Actions.createlisting()} >
            <Grid style={{ alignItems: 'center' }}>
              <Col size={87} style={styles.col1}>
                <Text style={styles.mainHeading}>Create your first listing</Text>

                <Text style={styles.subHeading}>To start using Dashboard</Text>
              </Col>
              <Col size={13} style={styles.col2}>
                <View>
                  <View style={styles.chevronStyle}></View>
                  <View style={styles.chevronStyleSVG}><Chevron /></View>
                </View>

              </Col>

            </Grid>
          </TouchableOpacity> */}
          <View style={styles.main_dashboard_container}>
            <View style={styles.dashboard_inner_container}>
              <View style={styles.dashboard_div}>
                <Text style={styles.dashboard}>Dashboard</Text>
                <Text style={styles.month_yr}>January 2020</Text>
              </View>
              <View style={styles.wallet_svg_import}>
                <Calendar />
              </View>
            </View>

            <View style={styles.row1}>
              <View style={styles.div1}>
                <View><Icon1 /></View>
                <Text style={styles.headingColoredText}>Total Earning   </Text>
                <Text style={styles.boldText}>00 </Text>
                <Text style={styles.unitText}>SAR</Text>
              </View>
              <View style={styles.div1}>
                <View><Icon2 /></View>
                <Text style={styles.headingColoredText}>Total Listing</Text>
                <Text style={styles.boldText}>00</Text>
                <Text style={styles.unitText}>Equipments</Text>
              </View>
            </View>
            <View style={styles.row1}>
              <View style={styles.div1}>
                <View><Icon3 /></View>
                <Text style={styles.headingColoredText} numberOfLines={1}>Enquiries received</Text>
                <Text style={styles.boldText}>00</Text>
                <Text style={styles.unitText}>Answered</Text>
              </View>
              <View style={styles.div1}>
                <View><Icon4 /></View>
                <Text style={styles.headingColoredText}>Total Rented</Text>
                <Text style={styles.boldText}>00</Text>
                <Text style={styles.unitText}>Rented</Text>
              </View>
            </View>




            {/* <View style={styles.row1}>
              <View style={styles.div1}>
                <View><Icon1 /></View>
                <Text style={styles.headingColoredText}>Total Earning   </Text>
                <Text style={styles.boldText}>30000 </Text>
                <Text style={styles.unitText}>SAR</Text>
              </View>
              <View style={styles.div1}>
                <View><Icon2 /></View>
                <Text style={styles.headingColoredText}>Total Listing</Text>
                <Text style={styles.boldText}>30000</Text>
                <Text style={styles.unitText}>Equipments</Text>
              </View>
            </View>
            <View style={styles.row1}>
              <View style={styles.div1}>
                <View><Icon3 /></View>
                <Text style={styles.headingColoredText} numberOfLines={1}>Enquiries received</Text>
                <Text style={styles.boldText}>30000</Text>
                <Text style={styles.unitText}>Answered</Text>
              </View>
              <View style={styles.div1}>
                <View><Icon4 /></View>
                <Text style={styles.headingColoredText}>Total Rented</Text>
                <Text style={styles.boldText}>30000</Text>
                <Text style={styles.unitText}>Rented</Text>
              </View>
            </View>
            <View style={styles.row1}>
              <View style={styles.div1}>
                <View><Icon1 /></View>
                <Text style={styles.headingColoredText}>Total Earning   </Text>
                <Text style={styles.boldText}>30000 </Text>
                <Text style={styles.unitText}>SAR</Text>
              </View>
              <View style={styles.div1}>
                <View><Icon2 /></View>
                <Text style={styles.headingColoredText}>Total Listing</Text>
                <Text style={styles.boldText}>30000</Text>
                <Text style={styles.unitText}>Equipments</Text>
              </View>
            </View> */}




          </View>
        </View>

      </ScrollView>

    )
  }
}
export default DashboardTwo


const styles = StyleSheet.create({

  body_inner_contaioner: {
    width: '90%',
    marginTop: 0,
    marginRight: 'auto',
    marginBottom: 0,
    marginLeft: 'auto',
  },
  heading_text: {
    color: 'white',
    fontSize: 30,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
    opacity: .2,
    marginBottom:5
  },
  no_of_items: {
    color: '#C18042',
    fontSize: 16,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'
  },
  do_you_know_text: {
    width: '100%',
    // marginTop: 20,
    // borderColor:'red',borderWidth:1,
    color: '#2D2F30',
  },
  small_text_do: {
    color: '#C8C8C9',
    fontSize: 16,
    lineHeight: 22,
    letterSpacing: .15,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'
  },
  main_dashboard_container: {
    marginTop: 30,
    backgroundColor: '#1D2122',
    borderRadius: 5,
    paddingBottom: 8,
  },
  dashboard_inner_container: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignContent: 'center',
    padding: 15,
    paddingRight: 10
  },
  dashboard: {
    color: 'white',
    fontSize: 18,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'

  },
  month_yr: {
    color: '#6D7172',
    fontSize: 15,
    letterSpacing: .5,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'
  },
  chevron_right_svg: {
    height: 30,
    width: 30,
    backgroundColor: '#37383A',
    justifyContent: "center",
    alignItems: 'center',
  },
  wallet_svg_import: {
    paddingRight: 15,
    paddingTop: 5
  },
  div1: {
    width: '45%',
    backgroundColor: '#212224',
    margin: 5,
    borderRadius: 3,
    padding: 20
  },
  row1: {
    flexDirection: 'row',
    marginTop: 0,
    marginRight: 'auto',
    marginBottom: 0,
    marginLeft: 'auto',
  },
  headingColoredText: {
    marginTop: 15,
    color: '#C18042',
    fontSize: 16,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'
  },
  boldText: {
    fontSize: 30,
    color: 'white',
    fontWeight: '500',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
  },
  unitText: {
    color: 'white',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
  },
  more_items_main: {
    width: '60%',
    height: 50,
    backgroundColor: '#2D2F30',
    marginTop: 20,
    borderRadius: 3,
    padding: 10
  },
  outerView: {
    marginTop: screenHeight * .04,
  },
  backgroundImageSty: {
    width: "100%",
  },
  representativeViewMain: {
    padding: 15
  },
  representativeView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 15
  },
  representativeText: {
    color: '#FFFFFF',
    fontSize: 14,
    marginRight: 15,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
  },
  cirularImage: {
    height: 50,
    width: 50,
    borderRadius: 25,
    borderWidth:1,
    borderColor:'grey'
  },
  title: {
    width: '55%',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
    color: 'rgba(255, 255, 255, 0.400896)'
  },
  representativeName: {
    fontSize: 16,
    marginTop: 5,
    color: '#FFFFFF',
    letterSpacing: .1,
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },
  representativeManager: {
    color: '#FF9800',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },
  horizontalLine: {
    borderWidth: 1,
    borderColor: '#2C2C2C',
    marginTop: screenHeight * .015
  },
  contractSigningView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: screenHeight * .015,
    justifyContent: 'space-between'
  },
  contractSigning: {
    color: '#FFFFFF',
    opacity: .3,
    fontSize: 14,
    marginRight: 15,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
  },
  mainHeading: {
    color: '#E6E7E7',
    fontSize: 18,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
  },
  subHeading: {
    color: '#6D6F6F',
    fontSize: 14,
    marginTop: 5,
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
  },
  chevronStyle: {
    height: 35,
    width: 35,
    backgroundColor: 'white',
    opacity: .05,
  },
  chevronStyleSVG: {
    marginTop: 7,
    marginLeft: 8,
    position: 'absolute',
  },
  passwordView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: screenHeight * .025,
    justifyContent: "space-between",
    // borderColor:'red',borderWidth:1,
    height:30
  },
  passwordText: {
    color: '#FFFFFF',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
    marginHorizontal: screenWidth * 0.03,
    margin: 5,
    fontSize: 15,
    marginRight:0
  },
})



