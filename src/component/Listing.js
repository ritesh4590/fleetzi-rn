import React from 'react';
import { Col, Row, Grid } from "react-native-easy-grid";
import { View, Text, StyleSheet, Platform, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import HeaderComp from './HeaderComp'
import ListingImage from './svg/ListingImage'
import Chevron from './svg/chevron'

class ListingMain extends React.Component {
	render() {
		return (
			<LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{ x: 0, y: 0 }}
				end={{ x: 1, y: 1 }}>
				<View style={styles.container}>
					<HeaderComp />
					<ScrollView>
						<View style={styles.BodyContainer}>

							<ListingImage />

							<View style={styles.subOuterContainer}>

								<Grid>
									<Col size={85} style={styles.col1}>
										<Text style={styles.mainHeading}>Create New Listing</Text>
										{/* <Text style={styles.subHeading}>Lorem ipsum dolor sit amet</Text> */}
									</Col>
									<Col size={15} style={styles.col2}>
										<View>
											<View style={styles.chevronStyle}></View>
											<View style={styles.chevronStyleSVG}><Chevron /></View>
										</View>

									</Col>

								</Grid>

								<View style={styles.borderBottom}></View>


							</View>

							<View style={styles.subOuterContainer}>

								<Grid>
									<Col size={85} style={styles.col1}>
										<Text style={styles.mainHeading}>Manage Listings</Text>

										{/* <Text style={styles.subHeading}>Lorem ipsum dolor sit amet</Text> */}
									</Col>
									<Col size={15} style={styles.col2}>
										<View>
											<View style={styles.chevronStyle}></View>
											<View style={styles.chevronStyleSVG}><Chevron /></View>
										</View>

									</Col>

								</Grid>

								<View style={styles.borderBottom}></View>


							</View>

							<View style={styles.subOuterContainer}>

								<Grid>
									<Col size={85} style={styles.col1}>
										<Text style={styles.mainHeading}>Add Operators</Text>

										{/* <Text style={styles.subHeading}>Lorem ipsum dolor sit amet</Text> */}
									</Col>
									<Col size={15} style={styles.col2}>
										<View>
											<View style={styles.chevronStyle}></View>
											<View style={styles.chevronStyleSVG}><Chevron /></View>
										</View>

									</Col>

								</Grid>

								<View style={styles.borderBottom}></View>


							</View>

							<View style={styles.subOuterContainer}>

								<Grid>
									<Col size={85} style={styles.col1}>
										<Text style={styles.mainHeading}>Create New Listing</Text>

										<Text style={styles.subHeading}>Lorem ipsum dolor sit amet</Text>
									</Col>
									<Col size={15} style={styles.col2}>
										<View>
											<View style={styles.chevronStyle}></View>
											<View style={styles.chevronStyleSVG}><Chevron /></View>
										</View>

									</Col>

								</Grid>

								<View style={styles.borderBottom}></View>


							</View>

							<View style={styles.subOuterContainer}>

								<Grid>
									<Col size={85} style={styles.col1}>
										<Text style={styles.mainHeading}>Create New Listing</Text>

										<Text style={styles.subHeading}>Lorem ipsum dolor sit amet</Text>
									</Col>
									<Col size={15} style={styles.col2}>
										<View>
											<View style={styles.chevronStyle}></View>
											<View style={styles.chevronStyleSVG}><Chevron /></View>
										</View>

									</Col>

								</Grid>



							</View>

						</View>

					</ScrollView>
				</View>
			</LinearGradient>
		)
	}
}

export default ListingMain

const styles = StyleSheet.create({
	linearGradient: {
		height: '100%'
	},
	container: {
		width: '90%',
		flex: 1,
		// height:'100%',
		marginTop: 0,
		marginRight: 'auto',
		marginBottom: 0,
		marginLeft: 'auto',
	},
	BodyContainer: {
		width: '100%'
	},

	subOuterContainer: {
		width: '95%',
		height: 130,
		padding: 8,
		marginRight: 'auto',
		marginBottom: 10,
		marginLeft: 'auto',
	},

	chevronStyle: {
		height: 35,
		width: 35,
		backgroundColor: 'white',
		opacity: .05,
	},
	chevronStyleSVG: {
		marginTop: 7,
		marginLeft: 8,
		position: 'absolute',
	},
	mainHeading: {
		color: '#E6E7E7',
		fontSize: 20
	},
	subHeading: {
		color: '#6D6F6F',
		fontSize: 14,
		marginTop: 5
	},
	borderBottom: {
		borderBottomWidth: 2,
		borderBottomColor: '#595959',
	},
	col1: {
		marginTop: 15,
	},
	col2: {
		marginTop: 30,
	}

})