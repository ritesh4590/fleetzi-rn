/**
* This is the Footer Component
**/

// React native and others libraries imports
import React, { Component } from 'react';
import { FlatList, Text, StyleSheet } from 'react-native';

const rows = [
  { id: 0, text: 'View' },
  { id: 1, text: 'Text' },
  { id: 2, text: 'Image' },
  { id: 3, text: 'ScrollView' },
  { id: 4, text: 'ListView' },
  { id: 5, text: 'ListView' },
  { id: 6, text: 'ListView' },
  { id: 7, text: 'ListView' },
  { id: 8, text: 'ListView' },
  { id: 9, text: 'ListView' },
  { id: 10, text: 'ListView' },
  { id: 11, text: 'ListView' },
  { id: 12, text: 'ListView' },
  { id: 13, text: 'ListView' },
  { id: 14, text: 'ListView' },
  { id: 15, text: 'ListView' },
  { id: 16, text: 'ListView' },
  { id: 18, text: 'ListView' },
  { id: 19, text: 'ListView' },
  { id: 20, text: 'ListView' },

]

const extractKey = ({ id }) => id

export default class App extends Component {

  renderItem = ({ item }) => {
    return (
      <Text style={styles.row}>
        {item.text}
      </Text>
    )
  }

  render() {
    return (
      <FlatList
        style={styles.container}
        data={rows}
        renderItem={this.renderItem}
        keyExtractor={extractKey}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    padding: 15,
    marginBottom: 5,
    backgroundColor: 'skyblue',
  },
})