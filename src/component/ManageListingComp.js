import React from 'react';
import { Col, Row, Grid } from "react-native-easy-grid";
import { View, Text, StyleSheet, Platform, ScrollView, Image, TextInput, Button } from 'react-native';
import IconImage from '../component/svg/IconImage'
import Star from '../component/svg/Star'
import Growth from '../component/svg/Growth'
import LinearGradient from 'react-native-linear-gradient';
import Switch from 'react-native-customisable-switch';


class ManageListingComp extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			switchOneValue: false,
			switchTwoValue: false,

			switchThreeValue: true,
		};
	}

	_getimage(name) {
		switch(name) {
		  case 'CAT':
			// code block
			var Icon = <Image style={{ width: '100%', height: '100%' }} source={require('../../assets/Images/Dump-Truck.png')} />
			break;
		  case 'HYUNDAI':
			// code block
			var Icon = <Image style={{ width: '100%', height: '100%' }} source={require('../../assets/Images/Grader.png')} />
			break;
		  case 'KOMATSU':
			// code block
			var Icon = <Image style={{ width: '100%', height: '100%' }} source={require('../../assets/Images/Excavator.png')} />
			break;
		  case 'VOLVO':
			// code block
			var Icon = <Image style={{ width: '100%', height: '100%' }} source={require('../../assets/Images/Forklift.png')} />
			break;
		  case 'BOMAG':
			// code block
			var Icon = <Image style={{ width: '100%', height: '100%' }} source={require('../../assets/Images/Crane.png')} />
			break;
		  case 'JCB':
			// code block
			var Icon = <Image style={{ width: '100%', height: '100%' }} source={require('../../assets/Images/Dozer.png')} />
			break;
		  default:
			// code block
			var Icon = <Image style={{ width: '100%', height: '100%' }} source={require('../../assets/Images/Boom-Truck-.png')} />
		}
		return(Icon	)
	  }

	render() {
		const {

			switchTwoValue,


		} = this.state;



		return (
			<View style={styles.container}>
				<LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}>
					<Grid>

						<Col size={40} style={styles.col1ML}>
							{/* <IconImage /> */}
							{this._getimage(this.props.data.brand)}
							{/* <Image
								style={{ width: '100%', height: '100%' }}
								source={require('../../assets/Images/Boom-Truck-.png')} /> */}
						</Col>

						<Col size={60} style={styles.col2ML}>
							{/* <View style={styles.listing}>
                                <View>
                                    <Text style={styles.listingText}>{this.props.data.name}</Text>
                                </View>
                                <View>
                                    <Star />
                                </View>

                            </View> */}
							<Text style={styles.title} numberOfLines={2}>{this.props.data.brand}</Text>
							<Text style={styles.capacityText}>{this.props.data.make_and_model}</Text>
							<Text style={styles.itemName}>{this.props.data.spec}</Text>
							<View style={styles.priceAndUnit}>
								<View style={{ flexDirection: 'row', alignItems: 'baseline' }}>
									<Text style={styles.price}>{this.props.data.price}</Text>
									<Text style={styles.PriceUnit}>SAR</Text>
								</View>
								<View>
									<Growth />
								</View>
							</View>
						</Col>
					</Grid>

					<View style={styles.toggleContainer}>
						<Grid style={{ alignItems: "center" }}>
							<Col size={80}>
								<Text style={styles.boldText}>Activate your listing</Text>
								{/* <Text style={styles.Description}>lorem ipsum doler sit amet </Text> */}
							</Col>
							<Col size={20} style={styles.toggleCol}>
								<Switch
									value={switchTwoValue}
									onChangeValue={() => this.setState({ switchTwoValue: !switchTwoValue })}
									activeText={''}
									inactiveText={''}
									fontSize={16}
									activeTextColor={'rgba(255, 255, 255, 1)'}
									inactiveTextColor={'rgba(255, 255, 255, 1)'}
									activeBackgroundColor={'#223331'}
									inactiveBackgroundColor={'#2B2C2E'}
									activeButtonBackgroundColor={'#00BC90'}
									inactiveButtonBackgroundColor={'#3B3C3E'}
									switchWidth={50}
									switchHeight={20}
									switchBorderRadius={0}
									switchBorderColor={'rgba(0, 0, 0, 1)'}
									switchBorderWidth={0}
									buttonWidth={18}
									buttonHeight={18}
									buttonBorderRadius={0}
									buttonBorderColor={'rgba(0, 0, 0, 1)'}
									buttonBorderWidth={0}
									animationTime={150}
									padding={true}
									switchBorderRadius={5}
									buttonBorderRadius={3}
								/>



							</Col>
						</Grid>
					</View>
					{/* <View style={styles.toggleContainerTwo}>
                <Grid>
                    <Col size={80}>
                        <Text style={styles.boldText}>Boost your listing</Text>
                        <Text style={styles.Description}>lorem ipsum doler sit amet </Text>
                    </Col>
                    <Col size={20} style={styles.toggleCol} >
                    <Switch
                      value={switchTwoValue}
                      onChangeValue={() => this.setState({ switchTwoValue: !switchTwoValue })}
                      activeText={''}
                      inactiveText={''}
                      fontSize={16}
                      activeTextColor={'rgba(255, 255, 255, 1)'}
                      inactiveTextColor={'rgba(255, 255, 255, 1)'}
                      activeBackgroundColor={'#223331'}
                      inactiveBackgroundColor={'#2B2C2E'}
                      activeButtonBackgroundColor={'#00BC90'}
                      inactiveButtonBackgroundColor={'#3B3C3E'}
                      switchWidth={50}
                      switchHeight={20}
                      switchBorderRadius={0}
                      switchBorderColor={'rgba(0, 0, 0, 1)'}
                      switchBorderWidth={0}
                      buttonWidth={18}
                      buttonHeight={18}
                      buttonBorderRadius={0}
                      buttonBorderColor={'rgba(0, 0, 0, 1)'}
                      buttonBorderWidth={0}
                      animationTime={150}
                      padding={true}
                      switchBorderRadius={5}
                      buttonBorderRadius={3}
                    />
                    </Col>
                </Grid>
                </View> */}
					{/* <Text>{'\n'}</Text> */}
				</LinearGradient>

			</View>
		)
	}
}

export default ManageListingComp


const styles = StyleSheet.create({

	container: {
		width: '90%',
		marginTop: 0,
		marginBottom: 0,
		marginLeft: 'auto',
		marginRight: 'auto',
	},
	col1ML: {
		alignItems: 'center',
		justifyContent: 'center',
		// backgroundColor:'#f4f4f4',
		height: '100%'
		// padding:15
	},
	col2ML: {
		padding: 15
	},
	listing: {
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	listingText: {
		color: '#FF8B00',
		fontSize: 14,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'

	},
	title: {
		color: '#FFFFFF',
		fontSize: 20,
		width: '70%',
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'

		// fontWeight:'bolds'
	},
	capacityText: {
		color: '#FFFFFF',
		marginTop: 5
	},
	itemName: {
		color: "#7D7E7E",
		marginTop: 5,
		fontSize: 12
	},
	priceAndUnit: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: 5,
		fontSize: 20,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
	},
	price: {
		color: '#E8E9E9',
		fontSize: 20,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
		alignItems: 'baseline'
	},
	PriceUnit: {
		color: '#E8E9E9',
		// marginTop: 10,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
		fontSize: 12,
		paddingBottom: 2
	},
	boldText: {
		fontSize: 16,
		color: '#FFFFFF',
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',

	},
	Description: {
		marginTop: 5,
		color: '#7D7E7E',
		fontSize: 12,
		fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
	},
	toggleContainer: {
		height: 50,
		backgroundColor: '#202427',
		padding: 20,

	},
	toggleContainerTwo: {
		height: 100,
		backgroundColor: '#171B1C',
		padding: 20,
	},

	linearGradient: {
		marginBottom: 20
	}
})