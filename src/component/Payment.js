import React from 'react';
import { Col, Row, Grid } from "react-native-easy-grid";
import { View, Text, StyleSheet, Platform, ScrollView, Image, TextInput, Button } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import HeaderComp from './HeaderComp'
import Back from "./svg/back"
import WhitehouseRightick from './svg/WhitehouseRightick'
import WhitehouseUp from './svg/WhitehouseUp'
import WhitehouseDown from './svg/WhitehouseDown'
import Dropdown from './svg/Dropdown'

class Payment extends React.Component {
  render() {
    return (

      <View style={styles.container}>
        <ScrollView>
          <Text style={styles.monthText}>{this.props.monthText}</Text>
          <View style={styles.monthAndDropdown}>
            <View>
              <Text style={styles.dateYear}>{this.props.monthYear}</Text>
            </View>
            <View>
              <Dropdown />
            </View>
          </View>

          <View style={styles.BoxContainer}>

            <Text style={styles.smallText}  >
              {this.props.upperText}
            </Text>

            <View style={styles.priceAndWhitehouse}>
              <View style={styles.priceAndUnit}>
                <Text style={styles.price}>{this.props.price}</Text>
                <Text style={styles.unitPayments}>{this.props.priceUnit}</Text>
              </View>
              <WhitehouseRightick />
            </View>
            <View>

              <Text style={styles.bottomText}>
                {this.props.bottomText}
              </Text>
            </View>

          </View>

          {/* <View style={styles.BoxContainer}>
            <Text style={styles.smallTextcolored}>
              {this.props.upperText}
            </Text>

            <View style={styles.priceAndWhitehouse}>
              <View style={styles.priceAndUnit}>
                <Text style={styles.price}>{this.props.price}</Text>
                <Text style={styles.unitPayments}>{this.props.priceUnit}</Text>
              </View>
              <WhitehouseUp />
            </View>
            <View>

              <Text style={styles.bottomText}>
                {this.props.bottomText}


              </Text>
            </View>

          </View> */}

          {/* <View style={styles.BoxContainer}>
            <Text style={styles.smallText}>
              {this.props.upperText}

            </Text>

            <View style={styles.priceAndWhitehouse}>
              <View style={styles.priceAndUnit}>
                <Text style={styles.price}>{this.props.price}</Text>
                <Text style={styles.unitPayments}>{this.props.priceUnit}</Text>
              </View>
              <WhitehouseDown />
            </View>
            <View>

              <Text style={styles.bottomText}>
                {this.props.bottomText}
              </Text>
            </View>

          </View> */}

          {/* <View style={styles.BoxContainer}>
            <Text style={styles.smallText}>
              {this.props.upperText}

            </Text>

            <View style={styles.priceAndWhitehouse}>
              <View style={styles.priceAndUnit}>
                <Text style={styles.price}>{this.props.price}</Text>
                <Text style={styles.unitPayments}>{this.props.priceUnit}</Text>
              </View>
              <WhitehouseDown />
            </View>
            <View>

              <Text style={styles.bottomText}>
                {this.props.bottomText}
              </Text>
            </View>

          </View> */}
          {/* <View style={styles.BoxContainer}>
            <Text style={styles.smallText}>
              {this.props.upperText}

            </Text>

            <View style={styles.priceAndWhitehouse}>
              <View style={styles.priceAndUnit}>
                <Text style={styles.price}>{this.props.price}</Text>
                <Text style={styles.unitPayments}>{this.props.priceUnit}</Text>
              </View>
              <WhitehouseDown />
            </View>
            <View>

              <Text style={styles.bottomText}>
                {this.props.bottomText}
              </Text>
            </View>

          </View> */}
          {/* <View style={styles.BoxContainer}>
            <Text style={styles.smallText}>
              {this.props.upperText}

            </Text>

            <View style={styles.priceAndWhitehouse}>
              <View style={styles.priceAndUnit}>
                <Text style={styles.price}>{this.props.price}</Text>
                <Text style={styles.unitPayments}>{this.props.priceUnit}</Text>
              </View>
              <WhitehouseDown />
            </View>
            <View>

              <Text style={styles.bottomText}>
                {this.props.bottomText}
              </Text>
            </View>

          </View> */}
          {/* <View style={styles.BoxContainer}>
            <Text style={styles.smallText}>
              {this.props.upperText}

            </Text>

            <View style={styles.priceAndWhitehouse}>
              <View style={styles.priceAndUnit}>
                <Text style={styles.price}>{this.props.price}</Text>
                <Text style={styles.unitPayments}>{this.props.priceUnit}</Text>
              </View>
              <WhitehouseDown />
            </View>
            <View>
              <Text style={styles.bottomText}>
                {this.props.bottomText}
              </Text>
            </View>

          </View> */}
          {/* <View style={styles.BoxContainer}>
            <Text style={styles.smallTextcolored}>
              {this.props.upperText}
            </Text>

            <View style={styles.priceAndWhitehouse}>
              <View style={styles.priceAndUnit}>
                <Text style={styles.price}>{this.props.price}</Text>
                <Text style={styles.unitPayments}>{this.props.priceUnit}</Text>
              </View>
              <WhitehouseUp />
            </View>
            <View>

              <Text style={styles.bottomText}>
                {this.props.bottomText}


              </Text>
            </View>

          </View> */}
          {/* <View style={styles.BoxContainer}>
        <Text style={styles.smallTextcolored}>
        {this.props.upperText}
        </Text>
         
        <View style={styles.priceAndWhitehouse}>
          <View style={styles.priceAndUnit}>
          <Text style={styles.price}>{this.props.price}</Text>
          <Text style={styles.unitPayments}>{this.props.priceUnit}</Text>
          </View>
          <WhitehouseUp/>
        </View>
      <View>
         
        <Text style={styles.bottomText}>
        {this.props.bottomText}


        </Text>
      </View>

        </View>  */}
        </ScrollView>

      </View>


    )
  }
}

export default Payment

const styles = StyleSheet.create({
  container: {
    width: '90%',
    marginTop: 0,
    marginRight: 'auto',
    marginBottom: 0,
    marginLeft: 'auto',
    flex: 1
  },


  monthText: {
    color: '#FF8B00',
    fontSize: 14,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'

  },
  monthAndDropdown: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  dateYear: {
    color: '#FFFFFF',
    fontSize: 20,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'

  },
  BoxContainer: {
    backgroundColor: '#202425',
    borderRadius: 3,
    marginTop: 20,
    padding: 15
  },
  smallText: {
    width: '70%',
    color: '#00BC90',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'


  },
  smallTextcolored: {
    width: '60%',
    color: '#FF8B00',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'

  },
  priceAndWhitehouse: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',

  },
  priceAndUnit: {
    flexDirection: 'row',
    alignItems: 'baseline',
    width: '80%',

  },
  price: {
    color: '#E8E9E9',
    fontSize: 20,
    fontWeight: '700',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
  },
  unitPayments: {
    color: 'white',
    fontSize: 14,
    marginHorizontal: 3,
    // paddingBottom: 3,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'

  },
  bottomText: {
    color: '#7D7E7E',
    fontSize: 12,
    marginTop: 30,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'

  },

})    