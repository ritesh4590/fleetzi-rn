import React from 'react';
import { Col, Row, Grid } from "react-native-easy-grid";
import { View, Text, StyleSheet, Platform, ScrollView, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import HeaderComp from './HeaderComp'
import ListingImage from './svg/ListingImage'
import Chevron from './svg/chevron'

class ListingMain extends React.Component{
    render(){
      return (
          <View style={styles.container}>
            {/* <ScrollView> */}
            <View style={styles.BodyContainer}>
            <TouchableOpacity onPress={() => Actions.createlisting()} style={styles.subOuterContainer}>
                {/* <Grid>
                    <Col size={85} style={styles.col1}>
                    <Text style={styles.mainHeading}>Create New Listing</Text>
                    
                    <Text style={styles.subHeading}>Lorem ipsum dolor sit amet</Text> 
                    </Col>
                    <Col size={15} style={styles.col2}>
                    <View>
                        <View style={styles.chevronStyle}></View>
                        <View style={styles.chevronStyleSVG}><Chevron/></View>
                    </View>
                   
                    </Col>

                </Grid> */}
                <View style={{flexDirection:'row' }}>
                    <View style={{width:'85%'}}>
                    <Text style={styles.mainHeading}>Create New Listing</Text>
                    </View>
                    <View style={{width:'15%'}}>
                        <View style={styles.chevronStyle}></View>
                        <View style={styles.chevronStyleSVG}><Chevron/></View>
                    </View>
                </View>
            <View style={styles.borderBottom}></View>        
            </TouchableOpacity>

            
            <TouchableOpacity onPress={() => Actions.managelisting()} style={styles.subOuterContainer}>
             {/* <Grid>
                 <Col size={85} style={styles.col1}>
                 <Text style={styles.mainHeading}>Manage Listings</Text>
                 
                 <Text style={styles.subHeading}>Lorem ipsum dolor sit amet</Text> 
                 </Col>
                 <Col size={15} style={styles.col2}>
                 <View>
                     <View style={styles.chevronStyle}></View>
                     <View style={styles.chevronStyleSVG}><Chevron/></View>
                 </View>
                
                 </Col>

             </Grid> */}
             <View style={{flexDirection:'row',marginTop:20 }}>
                    <View style={{width:'85%'}}>
                    <Text style={styles.mainHeading}>Manage Listings</Text>
                    </View>
                    <View style={{width:'15%'}}>
                        <View style={styles.chevronStyle}></View>
                        <View style={styles.chevronStyleSVG}><Chevron/></View>
                    </View>
                </View>
            <View style={styles.borderBottom}></View>
            </TouchableOpacity>
                         
         

         <View style={styles.subOuterContainer}>
                {/* <Grid>
                    <Col size={85} style={styles.col1}>
                    <Text style={styles.mainHeading}>Add Operators</Text>
                    
                    <Text style={styles.subHeading}>Lorem ipsum dolor sit amet</Text> 
                    </Col>
                    <Col size={15} style={styles.col2}>
                    <View>
                        <View style={styles.chevronStyle}></View>
                        <View style={styles.chevronStyleSVG}><Chevron/></View>
                    </View>
                   
                    </Col>

                </Grid>
                 */}
                  <View style={{flexDirection:'row',marginTop:40 }}>
                    <View style={{width:'85%'}}>
                    <Text style={styles.mainHeading}>Add Operators</Text>
                    </View>
                    <View style={{width:'15%'}}>
                        <View style={styles.chevronStyle}></View>
                        <View style={styles.chevronStyleSVG}><Chevron/></View>
                    </View>
                </View>
               <View style={styles.borderBottom}></View>                            
            </View>
          </View>
            
            {/* </ScrollView> */}
          </View>
      )}
}

export default ListingMain

const styles = StyleSheet.create({
    linearGradient:{
        height:'100%'
        },
         container:{
            width:'95%',
            flex:1,
            // height:'100%',
            marginTop: 0,
            marginRight: 'auto',
            marginBottom: 0,
            marginLeft: 'auto',
        },
   

        subOuterContainer:{
            width:'95%',
            height:60,
            padding:8,
            marginRight: 'auto',
            marginBottom: 10,
            marginLeft: 'auto',
         
        },
         
        chevronStyle:{
            height:35,
            width:35,
            backgroundColor:'#FFFFFF',
            opacity:.05,
        },
        chevronStyleSVG:{
            marginTop:7,
            marginLeft:8,
            position:'absolute',
        },
        mainHeading:{
            color:'#E6E7E7',
            fontSize:20,
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular',
            fontWeight:'600'
        },
        subHeading:{
            color:'#6D6F6F',
            fontSize:14,
            marginTop:5,
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular',
            textTransform:'capitalize'
        },
        borderBottom:{
            borderBottomWidth: 2,
            borderBottomColor:'#595959',
            // paddingRight:40,
            // paddingLeft:40
            marginTop:25
        },
        col1:{
            marginTop:15,
        },
        col2:{
            marginTop:30,
        }
        
})