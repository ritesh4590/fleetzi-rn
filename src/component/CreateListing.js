import React from 'react';
import { Col, Row, Grid } from "react-native-easy-grid";
import { BackHandler,View, Text, StyleSheet, Platform, ScrollView, TouchableOpacity, FlatList, TextInput, Image, Dimensions, Button, Alert, ActivityIndicator, Modal } from 'react-native';
import { Actions } from 'react-native-router-flux';
import ImagePicker from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';
import HeaderComp from './HeaderComp'
import Back from "./svg/back"
import Cross from './svg/Cross'
import Dropdown from './svg/Dropdown'
import Plus from "./svg/Plus"
import RightTick from "./svg/RightTick"
import Successfull from "./svg/Successfull"
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import Spinner from './Spinner';
import CTA from './CTA'
(screenwidth = Dimensions.get('window').width),
  (screenheight = Dimensions.get('window').height);
// More info on all the options is below in the API Reference... just some common use cases shown here
const options = {
  title: 'Select Avatar',
  // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

var allModel = [
  '740B',
  '725c2',
  '730c2',
  '625c1',
  '3 Axle 725c2',
  '730EJ',
  '740CG',
  '745',

];
var addsepecsdata = [
  '40 Ton',
  '50 Ton',
  '60 Ton',
  '70 Ton',
  '80 Ton',
  '90 Ton',
  '100 Ton',
  '110 Ton',
  '120 Ton',
  '130 Ton',

];
const otherBrand = [
  'CAT',
  'Catterpilar',
  'Sany',
  'Ingersoll',

]

const AllCategory = [
  'Air compressor',
  'Backhoe Loader',
  'Boom Truck',
  'Crane',
  'Dozer',
  'Dump Truck',
  'Excavator',
  'Forklift',
  'Generator',
  'Grader',
  'Man Basket',
  'Manlift',
  'Road Sweeper',
  'Roller Compactor',
  'Scissor Lift',
  'Skid Loader',
  ' Tower Light',
  'Welding Machine',
  'Wheel Loader',

]

const AllEquipmentType = [
  'All Terrain',
  'Articulated',
  'Electrical',
  'Four Pivot Balance Weight Type',
  'mini',
  'Mobile',
  'Normal',
  'Rough Terrain',
  'RT terex',
  'Telehandler',
  'Telesco pic',
  'Truck Mounted',

]


class CreateListingMain extends React.Component {

  constructor(props) {
    super(props);
    this.onHandleBackButton = this.handleBackButton.bind(this);
    this.state = {
      isLoading: false,
      makeyearmodalVisible: false,
      modelmodalVisible: false,
      addspecsmodalVisible: false,
      selectbrandmodalVisible: false,
      selectcategorymodalVisible: false,
      selectequipmenttypemodalVisible: false,
      Alert_Visibility: false,
      allYears: [],
      fliterYears: [],
      allModeldata: allModel,
      alladdspecdata: addsepecsdata,
      addotherBrand: otherBrand,
      AllCategoryState: AllCategory,
      AllEquipmentTypeState: AllEquipmentType,
      selectedMakeYear: 2020,
      listing_name: ' Listing 1',
      cat_name: 'CAT',
      make_and_model: '740B',
      category: 'Truck',
      equipment_type: 'Name Type',
      spec: '40 Ton',
      price: '700',
      ACCESSTOKEN: '',
      front_image: '',
      back_image: '',
      left_image: '',
      right_image: '',
      side_image: '',
      singleFileOBJ: '',

    };
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onHandleBackButton);
    this._retrieveData();
    let minOffset = 0,
      maxOffset = 20;
    let thisYear = new Date().getFullYear();
    let allYears = [];
    for (let x = 0; x <= maxOffset; x++) {
      allYears.push(thisYear - x);
    }
    this.setState({ allYears: allYears, fliterYears: allYears });
  }

  handleBackButton() {
    Actions.pop()
}

  setModalVisible(visible) {
    this.setState({ makeyearmodalVisible: visible });
  }


  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('ACCESSTOKEN');
      if (value !== null) {
        // We have data!!
        var eventstring = new String();
        eventstring = value.replace(/"/g, "");
        this.setState({ ACCESSTOKEN: eventstring })
        console.log(eventstring);
      }
    } catch (error) {
      // Error retrieving data
    }
  };



  _onsubmit() {
    if (!!this.state.listing_name && this.state.cat_name && this.state.make_and_model && this.state.spec && this.state.price) {
      this.setState({ isLoading: true });
      let formdata = new FormData();
      formdata.append("name", this.state.listing_name);
      formdata.append("brand", this.state.cat_name);
      formdata.append("make_and_model", this.state.make_and_model);
      formdata.append("spec", this.state.spec);
      formdata.append("price", this.state.price);
      fetch("http://34.87.55.15/api/v1/marketplace/listing/?format=json", {
        method: 'POST',
        headers: {
          'Authorization': 'Token ' + this.state.ACCESSTOKEN,
          'Content-Type': 'multipart/form-data',
        },
        body: formdata
      })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          if (responseJson.status == 'success') {
            this.setState({ isLoading: false, Alert_Visibility: true });
            // Actions.managelisting()
          } else {
            this.setState({ isLoading: false });
            console.log("somthing error");
          }
        })
        .catch((error) => {
          console.error(error);
          console.log("somthing error");
          this.setState({ isLoading: false });
        });
    } else {
      this.setState({ isLoading: false });
      Alert.alert("All fields are mandatory")
    }
  }



  onSearchChange(event) {
    const allYears = this.state.fliterYears
    let results = allYears.filter((item) => new RegExp(event).test(item))
    this.setState({
      allYears: results,
    })

  }

  onSearchChangeModel(event) {
    // console.log(event);
    let results = allModel.filter((item) => item.toLowerCase().startsWith(event.toLowerCase()));
    this.setState({
      allModeldata: results,
    })

  }

  onSearchChangespecs(event) {
    // console.log(event);
    let results = addsepecsdata.filter((item) => item.toLowerCase().startsWith(event.toLowerCase()));
    this.setState({
      alladdspecdata: results,
    })

  }

  onSearchChangeBrand(event) {
    // console.log(event);
    let results = otherBrand.filter((item) => item.toLowerCase().startsWith(event.toLowerCase()));
    console.log(results)
    this.setState({
      addotherBrand: results,
    })

  }

  onSearchCategory(event) {
    // console.log(event);
    let results = AllCategory.filter((item) => item.toLowerCase().startsWith(event.toLowerCase()));
    console.log(results)
    this.setState({
      AllCategoryState: results
    })

  }

  onSearchEquipmenttype(event) {
    // console.log(event);
    let results = AllEquipmentType.filter((item) => item.toLowerCase().startsWith(event.toLowerCase()));
    console.log(results)
    this.setState({
      AllEquipmentTypeState: results
    })

  }


  async SingleFilePicker() {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],

      });

      this.setState({ singleFileOBJ: res });

    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        Alert.alert('Canceled');
      } else {
        Alert.alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  }

  render_FlatList_header = () => {
    var header_View = (
      <>
        <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between' }}>
          <TouchableOpacity onPress={() => { this.setState({ selectbrandmodalVisible: false, cat_name: 'JCB' }) }}>
            <Image
              style={{ width: 100, height: 100, }}
              source={require('../../assets/Images/Group.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.setState({ selectbrandmodalVisible: false, cat_name: 'CAT' }) }}>
            <Image
              style={{ width: 100, height: 100, }}
              source={require('../../assets/Images/Group2.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.setState({ selectbrandmodalVisible: false, cat_name: 'BOMAG' }) }}>
            <Image
              style={{ width: 100, height: 100, }}
              source={require('../../assets/Images/Group3.png')}
            />
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between', marginTop: 20 }}>
          <TouchableOpacity onPress={() => { this.setState({ selectbrandmodalVisible: false, cat_name: 'VOLVO' }) }}>
            <Image
              style={{ width: 100, height: 100, }}
              source={require('../../assets/Images/Group4.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.setState({ selectbrandmodalVisible: false, cat_name: 'KOMATSU' }) }}>
            <Image
              style={{ width: 100, height: 100, }}
              source={require('../../assets/Images/Group5.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.setState({ selectbrandmodalVisible: false, cat_name: 'HYUNDAI' }) }}>
            <Image
              style={{ width: 100, height: 100, }}
              source={require('../../assets/Images/Group6.png')}
            />
          </TouchableOpacity>
        </View>
        <View>
          <Text style={{ color: '#D87C2A', fontFamily: 'Nexa Bold', marginTop: 20 }}>others</Text>
        </View>
      </>
    );
    return header_View;
  };

  _TakePicture(type) {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
        if (type === 'Front') {
          this.setState({
            front_image: source
          })
        }
        if (type === 'Back') {
          this.setState({
            back_image: source
          })
        }
        if (type === 'Left') {
          this.setState({
            left_image: source
          })
        }
        if (type === 'Right') {
          this.setState({
            right_image: source
          })
        }
        if (type === 'Side') {
          this.setState({
            side_image: source
          })
        }

      }
    });
  }


  alertaction() {
    this.setState({ Alert_Visibility: false })
    Actions.managelisting()
  }




  render() {
    console.log(this.state.singleFileOBJ)
    return (
      <>
        <View style={styles.container}>
          <Spinner visible={this.state.isLoading} />
          <View style={styles.createNewListingDiv}>
            <TouchableOpacity onPress={() => Actions.pop()}><Back /></TouchableOpacity>
            <Text style={styles.newListingText}>Create New Listing</Text>
          </View>
          {/* <Text>{'\n'}</Text> */}
          <View style={{ marginTop: 10 }}>
            <ScrollView >
              <View>
                {/* <View style={styles.topHeading}>
              <TextInput
                style={styles.listingOne}
                onChangeText={text => this.setState({listing_name:text})}
                value={this.state.listing_name} />
                <View>
                     <Pen/>
                </View>
           
              </View> */}
                {/* <View style={styles.borderBottomListing}></View> */}
              </View>
              <TouchableOpacity style={styles.div1} onPress={() => { this.setState({ selectcategorymodalVisible: true }) }}>
                <Text style={styles.title}>Category</Text>
                <View style={styles.TextAndDropdown}>
                  <Text style={styles.boldText}>{this.state.category}</Text>
                  <View><Dropdown /></View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={styles.div1} onPress={() => { this.setState({ selectbrandmodalVisible: true }) }}>
                <Text style={styles.title}>Select Brand</Text>
                {/* <Picker
                  selectedValue={this.state.cat_name}
     
                  style={[styles.TextAndDropdown, {color:'white'}]}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({cat_name: itemValue})
                  }>
                  <Picker.Item label="CAT1"  value="cat1" />
                  <Picker.Item label="CAT2" value="cat2" />
                </Picker> */}
                <View style={styles.TextAndDropdown}>
                  <Text style={styles.boldText}>{this.state.cat_name}</Text>
                  <View><Dropdown /></View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={styles.div1} onPress={() => { this.setState({ selectequipmenttypemodalVisible: true }) }}>
                <Text style={styles.title}>Equipment Type</Text>
                <View style={styles.TextAndDropdown}>
                  <Text style={styles.boldText}>{this.state.equipment_type}</Text>
                  <View><Dropdown /></View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={styles.div1} onPress={() => { this.setState({ makeyearmodalVisible: true }) }}>
                <Text style={styles.title}>Select Make Year</Text>
                {/* <Picker
                  selectedValue={this.state.cat_name}
     
                  style={[styles.TextAndDropdown, {color:'white'}]}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({cat_name: itemValue})
                  }>
                  <Picker.Item label="CAT1"  value="cat1" />
                  <Picker.Item label="CAT2" value="cat2" />
                </Picker> */}
                <View style={styles.TextAndDropdown}>
                  <Text style={styles.boldText}>{this.state.selectedMakeYear}</Text>
                  <View><Dropdown /></View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={styles.div1} onPress={() => { this.setState({ addspecsmodalVisible: true }) }}>
                <Text style={styles.title}>Add specs</Text>
                {/* <Picker
                  selectedValue={this.state.spec}
     
                  style={[styles.TextAndDropdown, {color:'white'}]}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({spec: itemValue})
                  }>
                  <Picker.Item label="40 Ton"  value="40 Ton" />
                  <Picker.Item label="10 Ton" value="10 Ton" />
                </Picker> */}
                <View style={styles.TextAndDropdown}>
                  <Text style={styles.boldText}>{this.state.spec}</Text>
                  <View><Dropdown /></View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={styles.div1} onPress={() => { this.setState({ modelmodalVisible: true }) }}>
                <Text style={styles.title}>Select Model</Text>
                {/* <Picker
                  selectedValue={this.state.make_and_model}
     
                  style={[styles.TextAndDropdown, {color:'white'}]}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({make_and_model: itemValue})
                  }>
                  <Picker.Item label="740 B Dump "  value="740 B Dump" />
                  <Picker.Item label="74 A Dump" value="74 A Dump" />
                </Picker> */}
                <View style={styles.TextAndDropdown}>
                  <Text style={styles.boldText} numberOfLines={1}>{this.state.make_and_model}</Text>
                  <View><Dropdown /></View>
                </View>
              </TouchableOpacity>
              {/* <View style={styles.div1}>
                <Text style={styles.title}>Add Price</Text>
                <View style={styles.TextAndDropdown}>
                  <Text style={styles.boldText}>40 Ton </Text>
                  <View><Dropdown/></View>
                </View>
              </View> */}


              <View style={styles.uploadPicturediv}>
                <Text style={styles.title}>Upload Pictures</Text>
                <View style={styles.row1}>
                  {
                    this.state.front_image === '' ?
                      <TouchableOpacity style={styles.col1} onPress={() => this._TakePicture('Front')} >
                        <Text style={styles.picText}>Front</Text>
                      </TouchableOpacity>
                      :
                      <TouchableOpacity style={styles.col1} onPress={() => this._TakePicture('Front')} >
                        <Image style={styles.col1} source={{ uri: this.state.front_image.uri }} />
                      </TouchableOpacity>
                  }
                  {
                    this.state.back_image === '' ?
                      <TouchableOpacity style={styles.col1} onPress={() => this._TakePicture('Back')} >
                        <Text style={styles.picText}>Back</Text>
                      </TouchableOpacity>
                      :
                      <TouchableOpacity style={styles.col1} onPress={() => this._TakePicture('Back')} >
                        <Image style={styles.col1} source={{ uri: this.state.back_image.uri }} />
                      </TouchableOpacity>
                  }
                  {
                    this.state.left_image === '' ?
                      <TouchableOpacity style={styles.col1} onPress={() => this._TakePicture('Left')} >
                        <Text style={styles.picText}>Left</Text>
                      </TouchableOpacity>
                      :
                      <TouchableOpacity style={styles.col1} onPress={() => this._TakePicture('Left')} >
                        <Image style={styles.col1} source={{ uri: this.state.left_image.uri }} />
                      </TouchableOpacity>
                  }
                  {
                    this.state.right_image === '' ?
                      <TouchableOpacity style={styles.col1} onPress={() => this._TakePicture('Right')} >
                        <Text style={styles.picText}>Right</Text>
                      </TouchableOpacity>
                      :
                      <TouchableOpacity style={styles.col1} onPress={() => this._TakePicture('Right')} >
                        <Image style={styles.col1} source={{ uri: this.state.right_image.uri }} />
                      </TouchableOpacity>
                  }
                  {
                    this.state.side_image === '' ?
                      <TouchableOpacity style={styles.col1} onPress={() => this._TakePicture('Side')} >
                        <Text style={styles.picText}>Side</Text>
                      </TouchableOpacity>
                      :
                      <TouchableOpacity style={styles.col1} onPress={() => this._TakePicture('Side')} >
                        <Image style={styles.col1} source={{ uri: this.state.side_image.uri }} />
                      </TouchableOpacity>
                  }


                </View>

              </View>
              <View style={styles.uploadDocumentsdiv}>
                <Text style={styles.title}>Upload Documents</Text>
                {
                  this.state.singleFileOBJ === '' ?
                    <>
                      <View style={styles.row1} >
                        <TouchableOpacity style={styles.uploadDocuments} onPress={this.SingleFilePicker.bind(this)}>
                          <Plus />
                        </TouchableOpacity>

                      </View>

                    </>
                    :
                    <>
                      <View style={styles.row1} >
                        <TouchableOpacity style={styles.uploadDocuments} onPress={this.SingleFilePicker.bind(this)}>
                          <RightTick />
                        </TouchableOpacity>
                      </View>
                      <Text style={{ color: 'white', fontFamily: 'Nexa Bold', paddingLeft: 5 }}>{this.state.singleFileOBJ.name}</Text>

                    </>
                }
                {/* <TouchableOpacity style={styles.row1} onPress={this.SingleFilePicker.bind(this)}> */}
                {/* <View  style={styles.uploadDocumentscol1}></TouchableOpacity> */}
                {/* <View  style={styles.col1}></View>
                  <View  style={styles.col1}></View>
                  <View  style={styles.col1}></View> */}


                {/* <View  style={styles.uploadDocuments}>
                    <Plus/>
                  </View>
                  
                </TouchableOpacity> */}
              </View>
              {/* <View style={styles.div1}>
              <Button
                title="SUBMIT"
                color="#FF8B00"
                style={{backgroundColor: '#FF8B00',}}
                onPress={() => this._onsubmit()}
                />
              </View> */}

            </ScrollView>
          </View>

          <TouchableOpacity activeOpacity={1} style={{ position: 'absolute', bottom: 20, left: 20, right: 20 }} onPress={() => this._onsubmit()}>
            <CTA ctaText={'SUBMIT'} />
          </TouchableOpacity>

          {/* <TouchableOpacity activeOpacity={1} onPress={() => this._onsubmit()} style={styles.buttondiv}>
          <LinearGradient
            colors={['#FFAC00', '#FF8B00']}
            style={styles.ButtonView}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 1 }}>
            <Text style={styles.buttomText}>SUBMIT</Text>
          </LinearGradient>
        </TouchableOpacity> */}
          {/* <View style={styles.buttondiv}>
          <Button
            title="SUBMIT"
            color="#FF8B00"
            style={{ backgroundColor: '#FF8B00', }}
            onPress={() => this._onsubmit()}
          />
        </View> */}
          {/* Make Year Modal */}
          <View style={{ marginTop: 22 }}>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.makeyearmodalVisible}
              onRequestClose={() => {
                this.setModalVisible(!this.state.makeyearmodalVisible);

              }}>
              <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 1 }}>
                <View style={{ marginTop: 22, paddingLeft: 20, paddingRight: 20, }}>

                  <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                    <View><Text style={{ color: 'white', fontFamily: 'Nexa Bold', fontSize: 20 }}>Select Make Year</Text></View>
                    <TouchableOpacity
                      onPress={() => {
                        this.setModalVisible(!this.state.makeyearmodalVisible);
                      }}>
                      <Cross />
                    </TouchableOpacity>
                  </View>
                  <TextInput
                    placeholder='Enter Here...'
                    placeholderTextColor='#FFFFFF'
                    style={{ height: 40, borderBottomColor: 'gray', borderBottomWidth: 1, marginTop: 20, fontFamily: 'Nexa Bold', color: '#FFFFFF' }}
                    onChangeText={text => this.onSearchChange(text)}
                    keyboardType='numeric'
                  // onChangeText={text => onChangeText(text)}
                  // value={value}
                  />
                  <Text style={{ color: '#D87C2A', fontFamily: 'Nexa Bold', marginTop: 10 }}>or Select Year</Text>
                  {/* <View style={{borderColor:'red',borderWidth:1}}> */}
                  <FlatList
                    style={{ marginTop: 20, marginBottom: 20, height: '80%' }}
                    data={this.state.allYears}
                    renderItem={({ item }) => (
                      <TouchableOpacity
                        onPress={() => {
                          this.setState({ makeyearmodalVisible: false, selectedMakeYear: item })
                        }}>
                        <Text key={item} style={{ fontSize: 20, color: '#FFFFFF', fontFamily: 'Nexa Bold', marginBottom: 30 }}>{item}</Text>
                      </TouchableOpacity>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                  />
                  {/* </View> */}


                </View>
              </LinearGradient>
            </Modal>
          </View>
          {/* End Make Year Modal */}
          {/* Select Model Modal */}
          <View style={{ marginTop: 22 }}>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modelmodalVisible}
              onRequestClose={() => { this.setState({ modelmodalVisible: false }) }}>
              <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 1 }}>
                <View style={{ marginTop: 22, paddingLeft: 20, paddingRight: 20, }}>

                  <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                    <View><Text style={{ color: 'white', fontFamily: 'Nexa Bold', fontSize: 20 }}>Select Model</Text></View>
                    <TouchableOpacity onPress={() => { this.setState({ modelmodalVisible: false }) }}>
                      <Cross />
                    </TouchableOpacity>
                  </View>
                  <TextInput
                    placeholder='Enter Here...'
                    placeholderTextColor='#FFFFFF'
                    style={{ height: 40, borderBottomColor: 'gray', borderBottomWidth: 1, marginTop: 20, fontFamily: 'Nexa Bold', color: '#FFFFFF' }}
                    onChangeText={text => this.onSearchChangeModel(text)}
                  // onChangeText={text => onChangeText(text)}
                  // value={value}
                  />
                  <Text style={{ color: '#D87C2A', fontFamily: 'Nexa Bold', marginTop: 20 }}>or Select Model</Text>
                  <FlatList
                    style={{ marginTop: 20, marginBottom: 20, height: '80%' }}
                    data={this.state.allModeldata}
                    renderItem={({ item }) => (
                      <TouchableOpacity
                        onPress={() => {
                          this.setState({ modelmodalVisible: false, make_and_model: item })
                        }}>
                        <Text key={item} style={{ fontSize: 20, color: 'white', fontFamily: 'Nexa Bold', marginBottom: 30 }}>{item}</Text>
                      </TouchableOpacity>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                  />



                </View>
              </LinearGradient>
            </Modal>
          </View>
          {/* End Select Model Modal */}
          {/* Add Specs Modal */}
          <View style={{ marginTop: 22 }}>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.addspecsmodalVisible}
              onRequestClose={() => { this.setState({ addspecsmodalVisible: false }) }}>
              <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 1 }}>
                <View style={{ marginTop: 22, paddingLeft: 20, paddingRight: 20, }}>

                  <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                    <View><Text style={{ color: 'white', fontFamily: 'Nexa Bold', fontSize: 20 }}>Select Specs</Text></View>
                    <TouchableOpacity onPress={() => { this.setState({ addspecsmodalVisible: false }) }}>
                      <Cross />
                    </TouchableOpacity>
                  </View>
                  <TextInput
                    placeholder='Enter Here...'
                    placeholderTextColor='#FFFFFF'
                    style={{ height: 40, borderBottomColor: 'gray', borderBottomWidth: 1, marginTop: 20, fontFamily: 'Nexa Bold', color: '#FFFFFF' }}
                    onChangeText={text => this.onSearchChangespecs(text)}
                  // onChangeText={text => onChangeText(text)}
                  // value={value}
                  />
                  <Text style={{ color: '#D87C2A', fontFamily: 'Nexa Bold', marginTop: 20 }}>or Select Specs</Text>
                  <FlatList
                    style={{ marginTop: 20, marginBottom: 20, height: '80%' }}
                    data={this.state.alladdspecdata}
                    renderItem={({ item }) => (
                      <TouchableOpacity
                        onPress={() => {
                          this.setState({ addspecsmodalVisible: false, spec: item })
                        }}>
                        <Text key={item} style={{ fontSize: 20, color: 'white', fontFamily: 'Nexa Bold', marginBottom: 30 }}>{item}</Text>
                      </TouchableOpacity>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                  />



                </View>
              </LinearGradient>
            </Modal>
          </View>
          {/* End Add Specs Modal */}
          {/* Select Brand Modal */}
          <View style={{ marginTop: 22 }}>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.selectbrandmodalVisible}
              onRequestClose={() => { this.setState({ selectbrandmodalVisible: false }) }}>
              <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 1 }}>
                <View style={{ marginTop: 22, paddingLeft: 20, paddingRight: 20, }}>
                  <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                    <View><Text style={{ color: 'white', fontFamily: 'Nexa Bold', fontSize: 20 }}>Select Brand</Text></View>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ selectbrandmodalVisible: false })
                      }}>
                      <Cross />
                    </TouchableOpacity>
                  </View>
                  <TextInput
                    placeholder='Enter Here...'
                    placeholderTextColor='#FFFFFF'
                    style={{ height: 40, borderBottomColor: 'gray', borderBottomWidth: 1, marginTop: 20, fontFamily: 'Nexa Bold', color: '#FFFFFF' }}
                    onChangeText={text => this.onSearchChangeBrand(text)}
                  // onChangeText={text => onChangeText(text)}
                  // value={value}
                  />
                  <Text style={{ color: '#D87C2A', fontFamily: 'Nexa Bold', marginTop: 20 }}>Popular Brand</Text>
                  <FlatList
                    style={{ marginTop: 20, marginBottom: 20, height: '80%' }}
                    data={this.state.addotherBrand}
                    ListHeaderComponent={this.render_FlatList_header}
                    renderItem={({ item }) => (
                      <TouchableOpacity
                        onPress={() => {
                          this.setState({ selectbrandmodalVisible: false, cat_name: item })
                        }}>
                        <Text key={item} style={{ fontSize: 20, color: 'white', fontFamily: 'Nexa Bold', marginBottom: 30 }}>{item}</Text>
                      </TouchableOpacity>
                    )}
                    // ListFooterComponent={this.render_FlatList_footer}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </View>
              </LinearGradient>
            </Modal>
          </View>
          {/* End Select Brand Modal */}
          {/* Select Category Modal */}
          <View style={{ marginTop: 22 }}>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.selectcategorymodalVisible}
              onRequestClose={() => { this.setState({ selectcategorymodalVisible: false }) }}>
              <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 1 }}>
                <View style={{ marginTop: 22, paddingLeft: 20, paddingRight: 20, }}>

                  <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                    <View><Text style={{ color: 'white', fontFamily: 'Nexa Bold', fontSize: 20 }}>Select Specs</Text></View>
                    <TouchableOpacity onPress={() => { this.setState({ selectcategorymodalVisible: false }) }}>
                      <Cross />
                    </TouchableOpacity>
                  </View>
                  <TextInput
                    placeholder='Enter Here...'
                    placeholderTextColor='#FFFFFF'
                    style={{ height: 40, borderBottomColor: 'gray', borderBottomWidth: 1, marginTop: 20, fontFamily: 'Nexa Bold', color: '#FFFFFF' }}
                    onChangeText={text => this.onSearchCategory(text)}
                  // onChangeText={text => onChangeText(text)}
                  // value={value}
                  />
                  <Text style={{ color: '#D87C2A', fontFamily: 'Nexa Bold', marginTop: 20 }}>or Select Specs</Text>
                  <FlatList
                    style={{ marginTop: 20, marginBottom: 20, height: '80%' }}
                    data={this.state.AllCategoryState}
                    renderItem={({ item }) => (
                      <TouchableOpacity
                        onPress={() => {
                          this.setState({ selectcategorymodalVisible: false, category: item })
                        }}>
                        <Text key={item} style={{ fontSize: 20, color: 'white', fontFamily: 'Nexa Bold', marginBottom: 30 }}>{item}</Text>
                      </TouchableOpacity>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                  />



                </View>
              </LinearGradient>
            </Modal>

          </View>
          {/* End Select Category Modal */}
          {/* Select Equipment Type Modal */}
          <View style={{ marginTop: 22 }}>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.selectequipmenttypemodalVisible}
              onRequestClose={() => { this.setState({ selectequipmenttypemodalVisible: false }) }}>
              <LinearGradient colors={['#323232', '#14181A', '#000000']} style={styles.linearGradient} start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 1 }}>
                <View style={{ marginTop: 22, paddingLeft: 20, paddingRight: 20, }}>

                  <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                    <View><Text style={{ color: 'white', fontFamily: 'Nexa Bold', fontSize: 20 }}>Select Equipment Type</Text></View>
                    <TouchableOpacity onPress={() => { this.setState({ selectequipmenttypemodalVisible: false }) }}>
                      <Cross />
                    </TouchableOpacity>
                  </View>
                  <TextInput
                    placeholder='Enter Here...'
                    placeholderTextColor='#FFFFFF'
                    style={{ height: 40, borderBottomColor: 'gray', borderBottomWidth: 1, marginTop: 20, fontFamily: 'Nexa Bold', color: '#FFFFFF' }}
                    onChangeText={text => this.onSearchEquipmenttype(text)}
                  // onChangeText={text => onChangeText(text)}
                  // value={value}
                  />
                  <Text style={{ color: '#D87C2A', fontFamily: 'Nexa Bold', marginTop: 20 }}>or Select Equipment</Text>
                  <FlatList
                    style={{ marginTop: 20, marginBottom: 20, height: '80%' }}
                    data={this.state.AllEquipmentTypeState}
                    renderItem={({ item }) => (
                      <TouchableOpacity
                        onPress={() => {
                          this.setState({ selectequipmenttypemodalVisible: false, equipment_type: item })
                        }}>
                        <Text key={item} style={{ fontSize: 20, color: 'white', fontFamily: 'Nexa Bold', marginBottom: 30 }}>{item}</Text>
                      </TouchableOpacity>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                  />



                </View>
              </LinearGradient>
            </Modal>

          </View>
          {/* End Select Equipment Type Modal */}
          {/* On Create Listing Sucess Dialog Box */}
          <Modal
            visible={this.state.Alert_Visibility}
            transparent={true}
            animationType={"fade"}
            onRequestClose={() => {
              this.setState({ Alert_Visibility: false })
              Actions.managelisting()
            }} >

            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.7)' }}>

              <View style={styles.MainAlertView}>

                <View style={{ width: '100%', alignItems: 'center', marginTop: 20 }} >
                  <Successfull />
                </View>
                <Text style={styles.AlertTitle}>Listing Added successfully</Text>


                <Text style={styles.AlertMessage}>You can manage your listings via Listing tab. At any time, you can add, edit, activate and deactivate your listings.</Text>

                {/* <View style={{ width: '100%', height: 0.5, backgroundColor: '#fff' }} /> */}

                {/* <View style={{ flexDirection: 'row', height: '20%', position: 'absolute', bottom: -20, left: 20, right: 20 }}> */}

                  {/* <TouchableOpacity onPress={() => this.alertaction()} style={styles.buttonStyle} activeOpacity={1} >
                    <LinearGradient
                      colors={['#FFAC00', '#FF8B00']}
                      style={styles.ButtonView}
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 1 }}>

                      <Text style={styles.TextStyle}> CHECK ALL LISTING </Text>

                    </LinearGradient>
                  </TouchableOpacity> */}
                  <TouchableOpacity onPress={() => this.alertaction()} activeOpacity={1} style={{ position: 'absolute', bottom: -20, left: 20, right: 20}}  >
                    <CTA ctaText={'VIEW ALL LISTINGS'} />
                  </TouchableOpacity>
                  {/* <TouchableOpacity  style={styles.buttonStyle} onPress={this.okButton} activeOpacity={0.7} >
                  <Text style={styles.TextStyle}> CHECK ALL LISTING </Text>
                </TouchableOpacity> */}

                  {/* <View style={{ width: 0.5, height: '100%', backgroundColor: '#fff' }} />

                    <TouchableOpacity style={styles.buttonStyle} onPress={() => { Actions.managelisting() }} activeOpacity={0.7} >
                      <Text style={styles.TextStyle}> CANCEL </Text>
                    </TouchableOpacity> */}
                {/* </View> */}

              </View>
            </View>
          </Modal>
          {/* END On Create Listing Sucess Dialog Box */}
        </View>
      </>
    )
  }
}

export default CreateListingMain


const styles = StyleSheet.create({
  container: {
    flex: 10,
    paddingLeft: 20,
    paddingRight: 20,
    //  marginBottom:50
    paddingBottom: 60
  },
  createNewListingDiv: {
    flexDirection: 'row',
    // justifyContent:'center',
    alignItems:'center'
  },
  newListingText: {
    color: 'white',
    fontSize: 20,
    marginHorizontal: 10,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'

  },
  topHeading: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  listingOne: {
    color: 'white',
    fontSize: 14,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'

  },
  borderBottomListing: {
    borderBottomColor: '#595959',
    borderBottomWidth: 1,
    marginTop: 10,
    opacity: .5
  },
  div1: {
    // height:85,
    backgroundColor: '#28292B',
    marginTop: 15,
    borderRadius: 4,
    padding: 10,
    paddingLeft: 18,
    paddingRight: 18
  },
  buttondiv: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0
  },
  TextAndDropdown: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
  },
  title: {
    color: '#FF8B00',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'

  },
  boldText: {
    width: '85%',
    fontSize: 22,
    color: 'white',
    overflow: 'hidden',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
    marginTop: 5

  },
  uploadPicturediv: {
    backgroundColor: '#28292B',
    marginTop: 15,
    borderRadius: 3,
    padding: 18,
    // borderWidth:1,
    // borderColor:'red',
    // marginBottom:20
  },
  uploadDocumentsdiv: {
    backgroundColor: '#28292B',
    marginTop: 15,
    borderRadius: 3,
    padding: 18,
    // flexDirection:'row',
    // borderWidth:1,
    // borderColor:'red',
    marginBottom: 50
  },
  // div2: {
  //   backgroundColor: '#28292B',
  //   marginTop: 15,
  //   borderRadius: 3,
  //   padding: 18,
  //   borderWidth:1,
  //   borderColor:'red',
  //   marginBottom:20
  // },
  row1: {
    marginTop: 10,
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  col1: {
    height: 70,
    width: 70,
    backgroundColor: '#4A4A4A',
    borderRadius: 3,
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  col1Dark: {
    height: 70,
    width: 70,
    backgroundColor: '#2F3132',
    borderRadius: 3,
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  picText: {
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular',
    color: '#7B7B7B'

  },
  uploadDocuments: {
    height: 70,
    width: 70,
    backgroundColor: '#0D1011',
    borderRadius: 5,
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#444646',
    borderWidth: 2,
    borderStyle: 'dashed',
    flexDirection: 'row'
  },
  linearGradient: {
    flex: 1

  },
  MainAlertView: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#FFFFFF",
    height: 400,
    width: '80%',
    borderRadius: 10
  },
  AlertTitle: {
    fontSize: 18,
    color: "#5ECA94",
    textAlign: 'center',
    padding: 15,
    height: '22%',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular'

  },
  AlertMessage: {
    fontSize: 16,
    color: "#999999",
    textAlign: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    height: '40%',
    fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
    lineHeight: 25,

  },
  buttonStyle: {
    width: '100%',
    position: 'absolute',
    bottom: 0,

  },
  TextStyle: {
    color: '#FFFFFF',
    fontSize: 20,
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
  },
  ButtonView: {
    backgroundColor: '#FF8B00',
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 0.85,
  },
  buttomText: {
    color: '#261400',
    fontFamily: Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
    fontSize: 14,
    marginTop: screenheight * 0.01,
  },
})