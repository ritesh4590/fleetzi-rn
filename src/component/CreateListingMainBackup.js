import React from 'react';
import { Col, Row, Grid } from "react-native-easy-grid";
import { View, Text, StyleSheet, Platform, ScrollView, TouchableOpacity, TextInput, Picker, Button, Alert, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';
import ImagePicker from 'react-native-image-picker';
import HeaderComp from './HeaderComp'
import Back from "./svg/back"
import Pen from './svg/Pen'
import Dropdown from './svg/Dropdown'
import Plus from "./svg/Plus"
import AsyncStorage from '@react-native-community/async-storage';

// More info on all the options is below in the API Reference... just some common use cases shown here
const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class CreateListingMainBackup extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      listing_name: ' Listing 1',
      cat_name:'CAT1',
      make_and_model:'740 B Dump',
      spec:'40 Ton',
      price:'700',
      ACCESSTOKEN:''

    };
}
componentDidMount(){
  this._retrieveData();
}


_retrieveData = async () => {
  try {
    const value = await AsyncStorage.getItem('ACCESSTOKEN');
    if (value !== null) {
      // We have data!!
      var eventstring = new String();
      eventstring = value.replace(/"/g, "");
      this.setState({ACCESSTOKEN:eventstring})
      console.log(eventstring);
    }
  } catch (error) {
    // Error retrieving data
  }
};

renderLoading() {
  if (this.state.isLoading) {
    return (
      <ActivityIndicator size="large"  color="#FF8B00" style={{
          position:'absolute', left:0, right:0, bottom:0, top:0 }}/>        
    )
  } else {
    return null
  }
}



_onsubmit(){
  if (!!this.state.listing_name && this.state.cat_name && this.state.make_and_model && this.state.spec && this.state.price) {
    this.setState({isLoading: true});
    let formdata = new FormData();
    formdata.append("name", this.state.listing_name);
    formdata.append("brand", this.state.cat_name);
    formdata.append("make_and_model", this.state.make_and_model);
    formdata.append("spec", this.state.spec);
    formdata.append("price", this.state.price);
    fetch("http://34.87.55.15/api/v1/marketplace/listing/?format=json", {
      method: 'POST',
      headers: {
        'Authorization':'Token '+this.state.ACCESSTOKEN,
        'Content-Type': 'multipart/form-data',
      },
      body: formdata
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson)
      if (responseJson.status == 'success') {
        this.setState({isLoading: false});
        Actions.managelisting()
      } else {
        this.setState({isLoading: false});
        console.log("somthing error");
      }
    })
    .catch((error) => {
      console.error(error);
      console.log("somthing error");
      this.setState({isLoading: false});
    });
   } else {
    this.setState({isLoading: false});
    Alert.alert("All fields are mandatory")
   }
}



    render(){
      
        // ImagePicker.showImagePicker(options, (response) => {
        //   console.log('Response = ', response);

        //   if (response.didCancel) {
        //     console.log('User cancelled image picker');
        //   } else if (response.error) {
        //     console.log('ImagePicker Error: ', response.error);
        //   } else if (response.customButton) {
        //     console.log('User tapped custom button: ', response.customButton);
        //   } else {
        //     const source = { uri: response.uri };
 
        //     this.setState({
        //       avatarSource: source,
        //     });
        //   }
        // });



      return (
          <View style={styles.container}>
          <View style={styles.createNewListingDiv}>
           <TouchableOpacity onPress={() => Actions.pop()}><Back/></TouchableOpacity> 
            <Text style={styles.newListingText}>Create New Listing</Text>
          </View>
          <Text>{'\n'}</Text>
            <ScrollView>
              <View>
              <View style={styles.topHeading}>
              <TextInput
                style={styles.listingOne}
                onChangeText={text => this.setState({listing_name:text})}
                value={this.state.listing_name} />
                {/* <Text style={styles.listingOne}>Listing 1</Text> */}
                <View>
                     <Pen/>
                </View>
              </View>
              <View style={styles.borderBottomListing}></View>
              </View>
              <View style={styles.div1}>
                <Text style={styles.title}>Select Brand</Text>
                <Picker
                  selectedValue={this.state.cat_name}
     
                  style={[styles.TextAndDropdown, {color:'white'}]}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({cat_name: itemValue})
                  }>
                  <Picker.Item label="CAT1"  value="cat1" />
                  <Picker.Item label="CAT2" value="cat2" />
                </Picker>
                {/* <View style={styles.TextAndDropdown}>
                  <Text style={styles.boldText}>CAT</Text>
                  <View><Dropdown/></View>
                </View> */}
              </View>
              <View style={styles.div1}>
                <Text style={styles.title}>Select Make and model</Text>
                <Picker
                  selectedValue={this.state.make_and_model}
     
                  style={[styles.TextAndDropdown, {color:'white'}]}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({make_and_model: itemValue})
                  }>
                  <Picker.Item label="740 B Dump "  value="740 B Dump" />
                  <Picker.Item label="74 A Dump" value="74 A Dump" />
                </Picker>
                {/* <View style={styles.TextAndDropdown}>
                  <Text style={styles.boldText} numberOfLines={1}>740 B Dump Trunckttttttttt </Text>
                  <View><Dropdown/></View>
                </View> */}
              </View>
              <View style={styles.div1}>
                <Text style={styles.title}>Add specs</Text>
                <Picker
                  selectedValue={this.state.spec}
     
                  style={[styles.TextAndDropdown, {color:'white'}]}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({spec: itemValue})
                  }>
                  <Picker.Item label="40 Ton"  value="40 Ton" />
                  <Picker.Item label="10 Ton" value="10 Ton" />
                </Picker>
                {/* <View style={styles.TextAndDropdown}>
                  <Text style={styles.boldText}>40 Ton </Text>
                  <View><Dropdown/></View>
                </View> */}
              </View>
              <View style={styles.div1}>
                <Text style={styles.title}>Add Price</Text>
                <Picker
                  selectedValue={this.state.price}
     
                  style={[styles.TextAndDropdown, {color:'white'}]}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({price: itemValue})
                  }>
                  <Picker.Item label="700"  value="700" />
                  <Picker.Item label="100" value="100" />
                </Picker>
                {/* <View style={styles.TextAndDropdown}>
                  <Text style={styles.boldText}>40 Ton </Text>
                  <View><Dropdown/></View>
                </View> */}
              </View>
              <View style={styles.div1}>
              <Button
                title="SUBMIT"
                color="#FF8B00"
                style={{backgroundColor: '#FF8B00',}}
                onPress={() => this._onsubmit()}
                />
              </View>






              {/* <View style={styles.div2}>
              <Text style={styles.title}>Upload Pictures</Text>
              <View style={styles.row1}>
                <View  style={styles.col1}></View>
                <View  style={styles.col1}></View>
                <View  style={styles.col1}></View>
                <View  style={styles.col1}></View>
                <View  style={styles.col1}></View>
                
                <View  style={styles.col1Dark}>
                  <Plus/>
                </View>
                
              </View>
             
              </View>
              <View style={styles.div2}>
                <Text style={styles.title}>Upload Documents</Text>
                <View style={styles.row1}>
                  <View  style={styles.col1}></View>
                  <View  style={styles.col1}></View>
                  <View  style={styles.col1}></View>
                  <View  style={styles.col1}></View>
                
                  
                  <View  style={styles.col1Dark}>
                    <Plus/>
                  </View>
                  
                </View>
             
              </View> */}
            </ScrollView>
            {this.renderLoading()}
          </View>
      )}
}

export default CreateListingMain


const styles = StyleSheet.create({
    container:{
      flex:1,
      width:'90%',
      marginTop: 0,
      marginRight: 'auto',
      marginBottom: 0,
      marginLeft: 'auto',
  },
  createNewListingDiv:{
      flexDirection:'row',
  },
  newListingText:{
      color:'white',
      fontSize:23,
      marginHorizontal:10,
      fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'

  },
  topHeading:{
    flexDirection:'row',
    justifyContent:'space-between'
  },
  listingOne:{
    color:'white',
    fontSize:14,
    fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'

  },
  borderBottomListing:{
    borderBottomColor:'#595959',
    borderBottomWidth:1,
    marginTop:10,
    opacity:.5
  },
  div1:{
    // height:85,
    backgroundColor:'#28292B',
    marginTop:15,
    borderRadius:3,
    padding:18
  },
  TextAndDropdown:{
    justifyContent:'space-between',
    flexDirection:'row',
  },
  title:{
    color:'#FF8B00',
    fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'

  },
  boldText:{
    width:'85%',
    fontSize:26,
    color:'white',
    overflow:'hidden',
    fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
    marginTop:5
    
  },
  div2:{
    backgroundColor:'#28292B',
    marginTop:15,
    borderRadius:3,
    padding:18
  },
  row1:{
    marginTop:10,
    flexDirection:'row',
    flexWrap:'wrap'
  },
  col1:{
    height:70,
    width:60,
    backgroundColor:'#D8D8D8',
    borderRadius:3,
    margin:5
  },
  col1Dark:{
    height:70,
    width:60,
    backgroundColor:'#979797',
    borderRadius:3,
    margin:5,
    justifyContent: 'center',
    alignItems: 'center',
  },
    })