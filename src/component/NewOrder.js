import React from 'react';
import { Col, Row, Grid } from "react-native-easy-grid";
import { View, Text, StyleSheet, Platform, ScrollView,Image,TextInput, Button } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import HeaderComp from './HeaderComp'
import Back from "./svg/back"
import IconImage from './svg/IconImage'
import {KeyboardAvoidingView} from 'react-native';


class NewOrder extends React.Component{
    constructor() {
        super();
        this.state = {
          type: '',
          heading: '',
        };
      }

      componentDidMount() {
          var data = this.props
          if(data){
              this.setState({
                  type:data.type,
                  heading:data.heading,
              })
          }
      }
      renderBoxElement(){
          if(this.state.type==='neworder'){
              return (
                <View>
                <View style={styles.InputfieldView}>
                    <View style={styles.InputfieldInnerView}>
                        <Text style={styles.yourOffer}>Your Offer</Text>
                        <View style={{flexDirection:'row',   alignItems:'baseline'}}> 
                        <TextInput keyboardType = 'numeric' style={styles.textInput} />
                        <Text style={styles.unit}>SAR</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.thinLine}>
                 <Text style={styles.recommendedPrice}> Recommended price is 80000 SR.</Text>   
                 <Text style={styles.whyThisPrice}>Why this price?</Text>   
                </View>
                </View>
                
              )
          }
          if(this.state.type==='payment'){
              return(
                  <View>
                <View style={styles.InputfieldViewPayment}>
                    <View style={styles.InputfieldInnerView}>
                        <Text style={styles.yourOffer} numberOfLines={1}>{this.props.dueAmount}</Text>
                        <View style={styles.rupeeAndText}> 
                            <Text style={styles.paymentsDue}>{this.props.Amount}</Text>
                            <Text style={styles.PaymentDueunit}>SAR</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.thinLine}>
                 <Text style={styles.recommendedPricePayment} >{this.props.thinLineCont}</Text>   
                   
                </View>
                </View>
              )
          }
      }

    render(){
       
      return (
            <View>
              <View style={styles.bodyContainer}>
                  <Grid>
                      <Col size={40} style={{justifyContent:'center' , alignItems:'center'}}><IconImage/></Col>
                      <Col size={60}>
                      <View style={styles.contentDetailsView}>
                        <Text style={styles.ProductName} numberOfLines={2}>{this.props.prefixTitle}</Text>
                        <Text style={styles.ProductModel} numberOfLines={2}>{this.props.Title}
                        </Text>
                        <Text style={styles.ProductDesc} numberOfLines={2}>{this.props.sufixTitle}</Text>
                    </View>
                      </Col>
                  </Grid>

                  <View >
                      <Grid>
                          <Col size={50} style={styles.col1NewOrder}>
                          <Text style={styles.from}>{this.props.from}</Text>
                          <Text style={styles.fromDate}>{this.props.fromDate}</Text>
                          <Text style={styles.to}>{this.props.to}</Text>
                          <Text style={styles.toDate} >{this.props.toDate}</Text>
                          </Col>
                          <Col size={50} style={styles.col2NewOrder}>
                          <Text style={styles.for}>{this.props.for}</Text>
                          <Text style={styles.forDays}>{this.props.forDays}</Text>
                          <Text style={styles.LocationText}>{this.props.location}</Text>
                          <View style={styles.location}>
                          <Text style={styles.locationAddress} numberOfLines={1}>{this.props.locationAdd}</Text>
                          <Text style={styles.cityAddress} numberOfLines={1}>{this.props.locationCity}</Text>
                          </View>
                          </Col>
                      </Grid>
                  </View>
                  {this.renderBoxElement()}
                <View style={styles.buttonContainer}>
                    <Grid>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Rent"  color="#FF8B00" />
                        </Col>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Reject" color="#666666" />
                        </Col>
                    </Grid>
                </View>
              </View>
            <Text>{'\n'}</Text>

            {/*  */}
            <View style={styles.bodyContainer}>
                  <Grid>
                      <Col size={40} style={{justifyContent:'center' , alignItems:'center'}}><IconImage/></Col>
                      <Col size={60}>
                      <View style={styles.contentDetailsView}>
                        <Text style={styles.ProductName} numberOfLines={2}>{this.props.prefixTitle}</Text>
                        <Text style={styles.ProductModel} numberOfLines={2}>{this.props.Title}
                        </Text>
                        <Text style={styles.ProductDesc} numberOfLines={2}>{this.props.sufixTitle}</Text>
                    </View>
                      </Col>
                  </Grid>

                  <View >
                      <Grid>
                          <Col size={50} style={styles.col1NewOrder}>
                          <Text style={styles.from}>{this.props.from}</Text>
                          <Text style={styles.fromDate}>{this.props.fromDate}</Text>
                          <Text style={styles.to}>{this.props.to}</Text>
                          <Text style={styles.toDate} >{this.props.toDate}</Text>
                          </Col>
                          <Col size={50} style={styles.col2NewOrder}>
                          <Text style={styles.for}>{this.props.for}</Text>
                          <Text style={styles.forDays}>{this.props.forDays}</Text>
                          <Text style={styles.LocationText}>{this.props.location}</Text>
                          <View style={styles.location}>
                          <Text style={styles.locationAddress} numberOfLines={1}>{this.props.locationAdd}</Text>
                          <Text style={styles.cityAddress} numberOfLines={1}>{this.props.locationCity}</Text>
                          </View>
                          </Col>
                      </Grid>
                  </View>
                  {this.renderBoxElement()}
                <View style={styles.buttonContainer}>
                    <Grid>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Rent"  color="#FF8B00" />
                        </Col>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Reject" color="#666666" />
                        </Col>
                    </Grid>
                </View>
              </View>
            {/* <Text>{'\n'}</Text> */}
            {/* <View style={styles.bodyContainer}>
                  <Grid>
                      <Col size={40} style={{justifyContent:'center' , alignItems:'center'}}><IconImage/></Col>
                      <Col size={60}>
                      <View style={styles.contentDetailsView}>
                        <Text style={styles.ProductName} numberOfLines={2}>{this.props.prefixTitle}</Text>
                        <Text style={styles.ProductModel} numberOfLines={2}>{this.props.Title}
                        </Text>
                        <Text style={styles.ProductDesc} numberOfLines={2}>{this.props.sufixTitle}</Text>
                    </View>
                      </Col>
                  </Grid>

                  <View >
                      <Grid>
                          <Col size={50} style={styles.col1NewOrder}>
                          <Text style={styles.from}>{this.props.from}</Text>
                          <Text style={styles.fromDate}>{this.props.fromDate}</Text>
                          <Text style={styles.to}>{this.props.to}</Text>
                          <Text style={styles.toDate} >{this.props.toDate}</Text>
                          </Col>
                          <Col size={50} style={styles.col2NewOrder}>
                          <Text style={styles.for}>{this.props.for}</Text>
                          <Text style={styles.forDays}>{this.props.forDays}</Text>
                          <Text style={styles.LocationText}>{this.props.location}</Text>
                          <View style={styles.location}>
                          <Text style={styles.locationAddress} numberOfLines={1}>{this.props.locationAdd}</Text>
                          <Text style={styles.cityAddress} numberOfLines={1}>{this.props.locationCity}</Text>
                          </View>
                          </Col>
                      </Grid>
                  </View>
                  {this.renderBoxElement()}
                <View style={styles.buttonContainer}>
                    <Grid>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Rent"  color="#FF8B00" />
                        </Col>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Reject" color="#666666" />
                        </Col>
                    </Grid>
                </View>
              </View> */}
            {/* <Text>{'\n'}</Text> */}
            {/* <View style={styles.bodyContainer}>
                  <Grid>
                      <Col size={40} style={{justifyContent:'center' , alignItems:'center'}}><IconImage/></Col>
                      <Col size={60}>
                      <View style={styles.contentDetailsView}>
                        <Text style={styles.ProductName} numberOfLines={2}>{this.props.prefixTitle}</Text>
                        <Text style={styles.ProductModel} numberOfLines={2}>{this.props.Title}
                        </Text>
                        <Text style={styles.ProductDesc} numberOfLines={2}>{this.props.sufixTitle}</Text>
                    </View>
                      </Col>
                  </Grid>

                  <View >
                      <Grid>
                          <Col size={50} style={styles.col1NewOrder}>
                          <Text style={styles.from}>{this.props.from}</Text>
                          <Text style={styles.fromDate}>{this.props.fromDate}</Text>
                          <Text style={styles.to}>{this.props.to}</Text>
                          <Text style={styles.toDate} >{this.props.toDate}</Text>
                          </Col>
                          <Col size={50} style={styles.col2NewOrder}>
                          <Text style={styles.for}>{this.props.for}</Text>
                          <Text style={styles.forDays}>{this.props.forDays}</Text>
                          <Text style={styles.LocationText}>{this.props.location}</Text>
                          <View style={styles.location}>
                          <Text style={styles.locationAddress} numberOfLines={1}>{this.props.locationAdd}</Text>
                          <Text style={styles.cityAddress} numberOfLines={1}>{this.props.locationCity}</Text>
                          </View>
                          </Col>
                      </Grid>
                  </View>
                  {this.renderBoxElement()}
                <View style={styles.buttonContainer}>
                    <Grid>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Rent"  color="#FF8B00" />
                        </Col>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Reject" color="#666666" />
                        </Col>
                    </Grid>
                </View>
              </View> */}
            {/* <Text>{'\n'}</Text> */}
            {/* <View style={styles.bodyContainer}>
                  <Grid>
                      <Col size={40} style={{justifyContent:'center' , alignItems:'center'}}><IconImage/></Col>
                      <Col size={60}>
                      <View style={styles.contentDetailsView}>
                        <Text style={styles.ProductName} numberOfLines={2}>{this.props.prefixTitle}</Text>
                        <Text style={styles.ProductModel} numberOfLines={2}>{this.props.Title}
                        </Text>
                        <Text style={styles.ProductDesc} numberOfLines={2}>{this.props.sufixTitle}</Text>
                    </View>
                      </Col>
                  </Grid>

                  <View >
                      <Grid>
                          <Col size={50} style={styles.col1NewOrder}>
                          <Text style={styles.from}>{this.props.from}</Text>
                          <Text style={styles.fromDate}>{this.props.fromDate}</Text>
                          <Text style={styles.to}>{this.props.to}</Text>
                          <Text style={styles.toDate} >{this.props.toDate}</Text>
                          </Col>
                          <Col size={50} style={styles.col2NewOrder}>
                          <Text style={styles.for}>{this.props.for}</Text>
                          <Text style={styles.forDays}>{this.props.forDays}</Text>
                          <Text style={styles.LocationText}>{this.props.location}</Text>
                          <View style={styles.location}>
                          <Text style={styles.locationAddress} numberOfLines={1} >{this.props.locationAdd}</Text>
                          <Text style={styles.cityAddress} numberOfLines={1} >{this.props.locationCity}</Text>
                          </View>
                          </Col>
                      </Grid>
                  </View>
                  {this.renderBoxElement()}
                <View style={styles.buttonContainer}>
                    <Grid>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Rent"  color="#FF8B00" />
                        </Col>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Reject" color="#666666" />
                        </Col>
                    </Grid>
                </View>
              </View> */}
            {/* <Text>{'\n'}</Text> */}
            {/* <View style={styles.bodyContainer}>
                  <Grid>
                      <Col size={40} style={{justifyContent:'center' , alignItems:'center'}}><IconImage/></Col>
                      <Col size={60}>
                      <View style={styles.contentDetailsView}>
                        <Text style={styles.ProductName} numberOfLines={2}>{this.props.prefixTitle}</Text>
                        <Text style={styles.ProductModel} numberOfLines={2}>{this.props.Title}
                        </Text>
                        <Text style={styles.ProductDesc} numberOfLines={2}>{this.props.sufixTitle}</Text>
                    </View>
                      </Col>
                  </Grid>

                  <View >
                      <Grid>
                          <Col size={50} style={styles.col1NewOrder}>
                          <Text style={styles.from}>{this.props.from}</Text>
                          <Text style={styles.fromDate}>{this.props.fromDate}</Text>
                          <Text style={styles.to}>{this.props.to}</Text>
                          <Text style={styles.toDate} >{this.props.toDate}</Text>
                          </Col>
                          <Col size={50} style={styles.col2NewOrder}>
                          <Text style={styles.for}>{this.props.for}</Text>
                          <Text style={styles.forDays}>{this.props.forDays}</Text>
                          <Text style={styles.LocationText}>{this.props.location}</Text>
                          <View style={styles.location}>
                          <Text style={styles.locationAddress} numberOfLines={1}>{this.props.locationAdd}</Text>
                          <Text style={styles.cityAddress} numberOfLines={1}>{this.props.locationCity}</Text>
                          </View>
                          </Col>
                      </Grid>
                  </View>
                  {this.renderBoxElement()}
                <View style={styles.buttonContainer}>
                    <Grid>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Rent"  color="#FF8B00" />
                        </Col>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Reject" color="#666666" />
                        </Col>
                    </Grid>
                </View>
              </View> */}
            {/* <Text>{'\n'}</Text> */}
            {/* <View style={styles.bodyContainer}>
                  <Grid>
                      <Col size={40} style={{justifyContent:'center' , alignItems:'center'}}><IconImage/></Col>
                      <Col size={60}>
                      <View style={styles.contentDetailsView}>
                        <Text style={styles.ProductName} numberOfLines={2}>{this.props.prefixTitle}</Text>
                        <Text style={styles.ProductModel} numberOfLines={2}>{this.props.Title}
                        </Text>
                        <Text style={styles.ProductDesc} numberOfLines={2}>{this.props.sufixTitle}</Text>
                    </View>
                      </Col>
                  </Grid>

                  <View >
                      <Grid>
                          <Col size={50} style={styles.col1NewOrder}>
                          <Text style={styles.from}>{this.props.from}</Text>
                          <Text style={styles.fromDate}>{this.props.fromDate}</Text>
                          <Text style={styles.to}>{this.props.to}</Text>
                          <Text style={styles.toDate} >{this.props.toDate}</Text>
                          </Col>
                          <Col size={50} style={styles.col2NewOrder}>
                          <Text style={styles.for}>{this.props.for}</Text>
                          <Text style={styles.forDays}>{this.props.forDays}</Text>
                          <Text style={styles.LocationText}>{this.props.location}</Text>
                          <View style={styles.location}>
                          <Text style={styles.locationAddress} numberOfLines={1}>{this.props.locationAdd}</Text>
                          <Text style={styles.cityAddress} numberOfLines={1}>{this.props.locationCity}</Text>
                          </View>
                          </Col>
                      </Grid>
                  </View>
                  {this.renderBoxElement()}
                <View style={styles.buttonContainer}>
                    <Grid>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Rent"  color="#FF8B00" />
                        </Col>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Reject" color="#666666" />
                        </Col>
                    </Grid>
                </View>
              </View> */}
            {/* <Text>{'\n'}</Text> */}
            {/* <View style={styles.bodyContainer}>
                  <Grid>
                      <Col size={40} style={{justifyContent:'center' , alignItems:'center'}}><IconImage/></Col>
                      <Col size={60}>
                      <View style={styles.contentDetailsView}>
                        <Text style={styles.ProductName} numberOfLines={2}>{this.props.prefixTitle}</Text>
                        <Text style={styles.ProductModel} numberOfLines={2}>{this.props.Title}
                        </Text>
                        <Text style={styles.ProductDesc} numberOfLines={2}>{this.props.sufixTitle}</Text>
                    </View>
                      </Col>
                  </Grid>

                  <View >
                      <Grid>
                          <Col size={50} style={styles.col1NewOrder}>
                          <Text style={styles.from}>{this.props.from}</Text>
                          <Text style={styles.fromDate}>{this.props.fromDate}</Text>
                          <Text style={styles.to}>{this.props.to}</Text>
                          <Text style={styles.toDate} >{this.props.toDate}</Text>
                          </Col>
                          <Col size={50} style={styles.col2NewOrder}>
                          <Text style={styles.for}>{this.props.for}</Text>
                          <Text style={styles.forDays}>{this.props.forDays}</Text>
                          <Text style={styles.LocationText}>{this.props.location}</Text>
                          <View style={styles.location}>
                          <Text style={styles.locationAddress} numberOfLines={1}>{this.props.locationAdd}</Text>
                          <Text style={styles.cityAddress} numberOfLines={1}>{this.props.locationCity}</Text>
                          </View>
                          </Col>
                      </Grid>
                  </View>
                  {this.renderBoxElement()}
                <View style={styles.buttonContainer}>
                    <Grid>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Rent"  color="#FF8B00" />
                        </Col>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Reject" color="#666666" />
                        </Col>
                    </Grid>
                </View>
              </View> */}
            {/* <Text>{'\n'}</Text> */}
            {/* <View style={styles.bodyContainer}>
                  <Grid>
                      <Col size={40} style={{justifyContent:'center' , alignItems:'center'}}><IconImage/></Col>
                      <Col size={60}>
                      <View style={styles.contentDetailsView}>
                        <Text style={styles.ProductName} numberOfLines={2}>{this.props.prefixTitle}</Text>
                        <Text style={styles.ProductModel} numberOfLines={2}>{this.props.Title}
                        </Text>
                        <Text style={styles.ProductDesc} numberOfLines={2}>{this.props.sufixTitle}</Text>
                    </View>
                      </Col>
                  </Grid>

                  <View >
                      <Grid>
                          <Col size={50} style={styles.col1NewOrder}>
                          <Text style={styles.from}>{this.props.from}</Text>
                          <Text style={styles.fromDate}>{this.props.fromDate}</Text>
                          <Text style={styles.to}>{this.props.to}</Text>
                          <Text style={styles.toDate} >{this.props.toDate}</Text>
                          </Col>
                          <Col size={50} style={styles.col2NewOrder}>
                          <Text style={styles.for}>{this.props.for}</Text>
                          <Text style={styles.forDays}>{this.props.forDays}</Text>
                          <Text style={styles.LocationText}>{this.props.location}</Text>
                          <View style={styles.location}>
                          <Text style={styles.locationAddress} numberOfLines={1}>{this.props.locationAdd}</Text>
                          <Text style={styles.cityAddress} numberOfLines={1}>{this.props.locationCity}</Text>
                          </View>
                          </Col>
                      </Grid>
                  </View>
                  {this.renderBoxElement()}
                <View style={styles.buttonContainer}>
                    <Grid>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Rent" style={{height:100}} color="#FF8B00" />
                        </Col>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Reject" color="#666666" />
                        </Col>
                    </Grid>
                </View>
              </View> */}
            {/* <Text>{'\n'}</Text> */}
            {/* <View style={styles.bodyContainer}>
                  <Grid>
                      <Col size={40} style={{justifyContent:'center' , alignItems:'center'}}><IconImage/></Col>
                      <Col size={60}>
                      <View style={styles.contentDetailsView}>
                        <Text style={styles.ProductName} numberOfLines={2}>{this.props.prefixTitle}</Text>
                        <Text style={styles.ProductModel} numberOfLines={2}>{this.props.Title}
                        </Text>
                        <Text style={styles.ProductDesc} numberOfLines={2}>{this.props.sufixTitle}</Text>
                    </View>
                      </Col>
                  </Grid>

                  <View >
                      <Grid>
                          <Col size={50} style={styles.col1NewOrder}>
                          <Text style={styles.from}>{this.props.from}</Text>
                          <Text style={styles.fromDate}>{this.props.fromDate}</Text>
                          <Text style={styles.to}>{this.props.to}</Text>
                          <Text style={styles.toDate} >{this.props.toDate}</Text>
                          </Col>
                          <Col size={50} style={styles.col2NewOrder}>
                          <Text style={styles.for}>{this.props.for}</Text>
                          <Text style={styles.forDays}>{this.props.forDays}</Text>
                          <Text style={styles.LocationText}>{this.props.location}</Text>
                          <View style={styles.location}>
                          <Text style={styles.locationAddress} numberOfLines={1}>{this.props.locationAdd}</Text>
                          <Text style={styles.cityAddress} numberOfLines={1}>{this.props.locationCity}</Text>
                          </View>
                          </Col>
                      </Grid>
                  </View>
                  {this.renderBoxElement()}
                <View style={styles.buttonContainer}>
                    <Grid>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Rent"  color="#FF8B00" />
                        </Col>
                        <Col size={50} style={styles.buttonCol1}>
                        <Button title="Reject" color="#666666" />
                        </Col>
                    </Grid>
                </View>
              </View> */}
            {/* <Text>{'\n'}</Text> */}
            {/*  */}
            </View>
      )}
}

export default NewOrder


const styles = StyleSheet.create({
        bodyContainer:{
            backgroundColor:'#262829',
            flex:1,
            marginBottom:15
        },
        contentDetailsView:{
            width:'50%',
        },
        contentDetailsView:{
          padding:15
        },
        ProductName:{
            color:'#7D7E7E',
            fontSize:12,
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'
        },
        ProductModel:{
            marginTop:5,
            color:'#E8E9E9',
            fontSize:20,
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
        },
        ProductDesc:{
            marginTop:5,
            fontSize:14,
            color:'white',
            fontWeight:'100',
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Light'
        },
        col1NewOrder:{
            padding:15,
            backgroundColor:'#2B2D2E',
        }, 
        col2NewOrder:{
            padding:20,
            backgroundColor:'#292C2D',
        },
         
        from:{
            color:'#FF8B00',
            fontSize:14, 
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'
        },
        fromDate:{
            color:'#E8E9E9',
            fontSize:16,
            paddingTop:5, 
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
        },
        to:{
            color:'#FF8B00', 
            fontSize:14,
            paddingTop:20, 
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'
        },
        toDate:{
            color:'#E8E9E9',
            fontSize:16,
            paddingTop:5, 
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
        },
        for:{
            color:'#FF8B00',
            fontSize:14,
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'
        },
        forDays:{
            color:'#E8E9E9',
            fontSize:16,
            paddingTop:5,
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
        },
        LocationText:{
            color:'#FF8B00', 
            fontSize:14,
            paddingTop:20,
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'
        },
        locationAddress:{
            color:'#E8E9E9',
            fontSize:16,
            width:'65%',
            paddingRight:2,
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
        },
        cityAddress:{
            fontSize:14,
            color:'#E8E9E9',
            width:'35%',
            paddingBottom:1,
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
        },
        location:{
            paddingTop:5,
            flexDirection:'row',
            alignItems:'baseline',
     
        },
        InputfieldView:{
            height:130,
            backgroundColor:'#0F1111'
        },
        InputfieldViewPayment:{
            backgroundColor:'#0F1111'
        },

        yourOffer:{
            color:'#FF8B00',
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'
        },
        textInput:{
            color:'#E8E9E9',
            fontSize:26,
            height:60,
            width:'40%',
            borderBottomColor:'#7D7E7E',
            borderBottomWidth:1,
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
        },
        InputfieldInnerView:{
            padding:20
        },
        unit:{
            marginHorizontal:5,
            color:'#E8E9E9',
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
        },
        thinLine:{
            backgroundColor:'#000000',
            padding:10,
            justifyContent:'space-between',
            flexDirection:'row'
        },
        recommendedPrice:{
            color:'#7D7E7E',
            fontSize:12,
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular',
            width:'70%'
        },
         recommendedPricePayment:{
            color:'#7D7E7E',
            fontSize:12,
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular',
        },
        whyThisPrice:{
            color:'#7D7E7E',
            fontSize:12,
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'
        },
       
        buttonContainer:{
            flexDirection:"row",
        },
        rentButton:{
            height:50,
        },
        paymentsDue:{
            color:'#E8E9E9',
            fontSize:20,
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'

        },
        PaymentDueunit:{
            marginHorizontal:5,
            paddingBottom:2,
            color:'#E8E9E9',
            fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'

        },
        rupeeAndText:{
            flexDirection:'row',
            alignItems:"baseline",
            
            
        }
    })