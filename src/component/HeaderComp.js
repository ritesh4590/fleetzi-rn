import React from 'react';
import { Col, Row, Grid } from "react-native-easy-grid";
import { View, Text, StyleSheet, Platform } from 'react-native';
import Flogo from './svg/Flogo'
import Contact from './svg/Contact'
import Profile from './svg/Profile'
class HeaderComp extends React.Component{
render(){
 

  return (
      
    <View style={{marginBottom:60, marginTop:20, marginLeft:20, marginRight:20}}>
            <Grid>
                <Col size={50}>
                    <Flogo/>
                </Col>
                <Col size={50} >
                {
                    this.props.customer_support?<Text></Text>:
                    <View style={{flexDirection:'row', justifyContent:'flex-end'}}>
                        {/* <View><Contact/></View> */}
                        {/* <View style={{marginLeft:30}}><Profile/></View>                     */}
                    </View>
                }
                </Col>
            </Grid>
        </View> 
  );
}
}

 

export default HeaderComp;