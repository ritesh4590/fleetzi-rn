import React, { Component } from 'react';
import { View, StatusBar, TextInput, Animated } from 'react-native';

export default class FloatingLabelInput extends Component {
  state = {
    isFocused: false,
  };


  getInnerRef = () => this.ref;

  componentWillMount() {
    this._animatedIsFocused = new Animated.Value(this.props.value === '' ? 0 : 1);
  }

  handleFocus = () => this.setState({ isFocused: true });
  handleBlur = () => this.setState({ isFocused: false });

  componentDidUpdate() {
    Animated.timing(this._animatedIsFocused, {
      toValue: (this.state.isFocused || this.props.value !== '') ? 1 : 0,
      duration: 200,
    }).start();
  }

  render() {
    const { label, ...props } = this.props;
    const labelStyle = {
      position: 'absolute',
      marginTop: 14,
      left: 3,
      fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular',
      top: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [10, -10],
      }),
      fontSize: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [14, 10],
      }),
      color: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: ['#6a6b6c', '#6a6b6c'],
      }),
		

    };
    return (
      <View style={{ paddingTop: 10 }}>
        <Animated.Text style={labelStyle}>
          {label}
        </Animated.Text>
        <TextInput
          {...props}
          ref={(r) => this.ref = r}
          style={{ fontSize: 18,
            borderBottomWidth: 1,
            borderColor:  this.state.isFocused ? '#FFFFFF' : '#333',
            color: '#FFFFFF',
            fontFamily: Platform.OS === 'ios' ? 'NexaRegular' : 'Nexa Regular', }}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          blurOnSubmit
        />
      </View>
    );
  }
}
