import React from 'react'
import { View } from 'react-native'
import Svg, { SvgUri, G, Path, Defs, LinearGradient, Circle, Stop, Rect, Line } from 'react-native-svg';


class Payment extends React.Component {
	render(props) {
		console.log(height)
		return (
			<View style={{ alignItems: 'center' }} >

				<Svg width="25px" height='50px' viewBox="0 0 75 89" version="1.1" >
					<G id="Partner" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
						<G id="02-Dashboard-Home" transform="translate(-908.000000, -2222.000000)">
							<G id="Bottom-Bar" transform="translate(0.000000, 2164.000000)">
								<G id="Payments" transform="translate(872.000000, 58.000000)">
									<G id="Payment" transform="translate(36.000000, 0.000000)">
										<G id="Group-4">
											<Rect id="Rectangle" stroke="#FFFFFF" strokeWidth='6' x="3" y="11" width="45" height="75" rx="8"></Rect>
											<Path d="M35.8365224,0.491527228 L69.167113,12.6228701 C73.0424766,14.0333871 75.0337595,18.3373153 73.6147678,22.2359629 L51.1332906,84.0033139 C49.9516422,87.2498662 46.7777517,89.1916045 43.5111833,88.9358114 C47.5739884,88.5530296 50.7527941,85.1331247 50.7527941,80.97077 L50.7527941,80.97077 L50.7518744,68.6746786 L68.352043,20.3204877 C68.7067909,19.3458258 68.2089702,18.2698437 67.2401293,17.9172145 L49.0664736,11.3014172 C47.602548,9.42304461 45.3187483,8.21499834 42.7527941,8.21499834 L42.7527941,8.21499834 L40.5828744,8.2146786 L33.9095387,5.7858716 C32.9406978,5.43324235 31.867717,5.93749944 31.5129691,6.91216133 L31.0388744,8.2146786 L25.0788744,8.2146786 L26.2502443,4.99668614 C27.669236,1.09803859 31.9611589,-0.918989754 35.8365224,0.491527228 Z" id="Combined-Shape" fill="#FFFFFF" fillRule="nonzero"></Path>
										</G>
										<Circle id="Oval" stroke="#FF8B00" strokeWidth="6" cx="26" cy="48.5833333" r="10"></Circle>
										<Line x1="25.5" y1="28" x2="25.5506576" y2="37.1666667" id="Line-7" stroke="#FF8B00" strokeWidth="6" strokeLinecap="round"></Line>
										<Line x1="25.5" y1="59" x2="25.5506576" y2="68.1666667" id="Line-7-Copy" stroke="#FF8B00" strokeWidth="6" strokeLinecap="round"></Line>
									</G>
								</G>
							</G>
						</G>
					</G>
				</Svg>
			</View>
		)
	}
}
export default Payment
