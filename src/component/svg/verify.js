import React from 'react'
import { View } from 'react-native'
import Svg, { G, Polyline, TSpan, Defs, Rect, Use, Text, Circle } from 'react-native-svg';

class Verify extends React.Component {
	render() {
		return (
			<View>
				<Svg width="55px" height="55px" viewBox="0 0 133 35" version="1.1">
					<Defs>
						<Rect id="path-1" x="0" y="2.84217094e-14" width="133" height="34.9482631" rx="17.4741316"></Rect>
					</Defs>
					<G id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
						<G id="Login-06" transform="translate(-888.000000, -566.000000)">
							<G id="Verified" transform="translate(888.000000, 566.000000)">
								<G id="Rectangle">
									<Use fillOpacity="0.102081512" fill="#FFFFFF" href="#path-1"></Use>
									<Rect strokeOpacity="0.35" stroke="#95FF00" strokeWidth="1" strokeLinejoin="square" fillOpacity="0.0958533654" fill="#95FF00" x="0.5" y="0.5" width="132" height="33.9482631" rx="16.9741316"></Rect>
								</G>
								<G id="Group" transform="translate(11.000000, 5.000000)">
									<Text id="Verified" fontFamily="NexaBold, Nexa Bold" fontSize="20" fontWeight="bold" fill="#95FF00">
										<TSpan x="34.6927199" y="19">Verified</TSpan>
									</Text>
									<Circle id="Oval" stroke="#95FF00" strokeWidth="3" cx="11.2333703" cy="13.2481523" r="9.73337029"></Circle>
									<Polyline id="Line-7" stroke="#95FF00" strokeWidth="3" strokeLinecap="square" points="11.2333703 9.50369549 11.2333703 13.7931177 16.463807 13.7931177"></Polyline>
								</G>
							</G>
						</G>
					</G>
				</Svg>

			</View>
		)
	}
}
export default Verify