import React from 'react'
import { View } from 'react-native'
import Svg, { G, Path, Defs,Mask,Use } from 'react-native-svg';


class PhoneNoSvg extends React.Component {
	render() {
		return (
			<View>

				<Svg width="18px" height="18px" viewBox="0 0 37 36" version="1.1" >

					<Defs>
						<Path d="M36.2526667,1.97545833 L36.2526667,13.9025833 L33.5026667,13.9025833 L33.5026667,5.22016667 L18.9973333,19.3958333 L17.0521667,17.494875 L31.5666667,3.31920833 L22.6731667,3.31920833 L22.6731667,0.631708333 L34.8776667,0.631708333 C35.6370582,0.631708333 36.2526667,1.2333257 36.2526667,1.97545833 Z M29.4583333,32.8333333 C29.4583333,33.0807109 29.2531305,33.28125 29,33.28125 L3.33333333,33.28125 C3.08020282,33.28125 2.875,33.0807109 2.875,32.8333333 L2.875,7.75 C2.875,7.50262246 3.08020282,7.30208333 3.33333333,7.30208333 L18,7.30208333 L18,4.61458333 L3.33333333,4.61458333 C1.56225741,4.61655803 0.127020625,6.0191758 0.125,7.75 L0.125,32.8333333 C0.127020625,34.5641575 1.56225741,35.9667753 3.33333333,35.96875 L29,35.96875 C30.7710759,35.9667753 32.2063127,34.5641575 32.2083333,32.8333333 L32.2083333,18.5 L29.4583333,18.5 L29.4583333,32.8333333 Z" id="path-1"></Path>
					</Defs>
					<G id="App-Screens---Partners" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
						<G id="Dashboard-Home-Option-New" transform="translate(-414.000000, -1038.000000)">
							<G id="New-Order-Tab" transform="translate(124.000000, 976.000000)">
								<G id="Group-6">
									<G id="Add-Icon-To-Home" transform="translate(290.000000, 62.000000)">
										<Mask id="mask-2" fill="white">
											<Use href="#path-1"></Use>
										</Mask>
										<Use id="Shape" fill="#FF9800" fillRule="nonzero" href="#path-1"></Use>
									</G>
								</G>
							</G>
						</G>
					</G>
				</Svg>
			</View>
		)
	}
}
export default PhoneNoSvg