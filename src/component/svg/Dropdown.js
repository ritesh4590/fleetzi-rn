import React from 'react'
import {View} from 'react-native'
import Svg, { G, Path,Line } from'react-native-svg';


class Dropdown extends React.Component{
    render(){
        return(
            <View>
            <Svg width="15px" height="15px" viewBox="0 0 41 25" version="1.1" >
                <G id="Partner" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <G id="05-Create-New-Listing" transform="translate(-916.000000, -768.000000)" fill="#FFFFFF" fillRule="nonzero">
                        <G id="Group-3" transform="translate(62.000000, 647.000000)">
                            <Path d="M883.213203,112.213203 C883.213203,110.556349 884.556349,109.213203 886.213203,109.213203 C887.870058,109.213203 889.213203,110.556349 889.213203,112.213203 L889.213203,139.213203 L862.213203,139.213203 C860.556349,139.213203 859.213203,137.870058 859.213203,136.213203 C859.213203,134.556349 860.556349,133.213203 862.213203,133.213203 L883.213203,133.213203 L883.213203,112.213203 Z" id="Rectangle" transform="translate(874.213203, 124.213203) rotate(45.000000) translate(-874.213203, -124.213203) "></Path>
                        </G>
                    </G>
                </G>
            </Svg>
              </View>
        )
    }
}
export default Dropdown