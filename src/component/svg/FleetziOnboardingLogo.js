import React from 'react'
import { View } from 'react-native'
import Svg, { G, Polygon ,Text,TSpan} from 'react-native-svg';


class FleetziOnboardingLogo extends React.Component {
	render() {
		return (
			<View>

				<Svg width="100px" height="56px" viewBox="0 0 573 56" version="1.1" >

					<G id="Partner" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
						<G id="01-Splash-Screen" transform="translate(-48.000000, -892.000000)">
							<G id="Group" transform="translate(48.000000, 892.000000)">
								<Polygon id="Path" fill="#FFFFFF" points="58 17.2857143 20.3783784 17.2857143 20.3783784 23.1785714 53.2972973 23.1785714 53.2972973 40.4642857 20.3783784 40.4642857 20.3783784 55 0 55 0 0 58 0"></Polygon>
								<Polygon id="Path" fill="#FFFFFF" points="86.2222222 37.7142857 122 37.7142857 122 55 66 55 66 0 86.2222222 0"></Polygon>
								<Polygon id="Path" fill="#FFFFFF" points="189 15.7142857 151.378378 15.7142857 151.378378 20.8214286 189 20.8214286 189 34.1785714 151.378378 34.1785714 151.378378 39.2857143 189 39.2857143 189 55 131 55 131 0 189 0"></Polygon>
								<Polygon id="Path" fill="#FFFFFF" points="255 15.7142857 217.378378 15.7142857 217.378378 20.8214286 255 20.8214286 255 34.1785714 217.378378 34.1785714 217.378378 39.2857143 255 39.2857143 255 55 197 55 197 0 255 0"></Polygon>
								<Polygon id="Path" fill="#FFFFFF" points="330 0 330 17.2857143 306.27907 17.2857143 306.27907 55 285.72093 55 285.72093 17.2857143 262 17.2857143 262 0"></Polygon>
								<Polygon id="Path" fill="#FF8B00" points="400 0 400 17.2857143 367.037975 37.7142857 400 37.7142857 400 55 338 55 338 37.7142857 370.962025 17.2857143 338 17.2857143 338 0"></Polygon>
								<Polygon id="Path" fill="#FF8B00" points="430 0 430 55 409 55 409 0"></Polygon>
								<Text id="Partner" fontFamily="NexaBold, Nexa Bold" fontSize="30" fontWeight="bold" letterSpacing="2" fill="#FFFFFF">
									<TSpan x="448.89" y="55">Partner</TSpan>
								</Text>
							</G>
						</G>
					</G>
				</Svg>
			</View>
		)
	}
}
export default FleetziOnboardingLogo