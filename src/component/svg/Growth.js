import React from 'react'
import {View} from 'react-native'
import Svg, { SvgUri, G, Path, Defs, LinearGradient,Circle,Stop,Rect,Line,Polyline } from'react-native-svg';


class Growth extends React.Component{
    render(){
        return(
            <View >
             <Svg width="25px" height="25px" viewBox="0 0 66 55" version="1.1" >
             <G id="Partner" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round">
                <G id="06-Manage-Listing" transform="translate(-915.000000, -939.000000)" strokeWidth="6">
                  <G id="Card-01" transform="translate(62.000000, 480.000000)">
                      <G id="Group-4" transform="translate(856.000000, 462.000000)">
                          <G id="Group-2" transform="translate(11.578947, 10.526316)" stroke="#FF8400">
                               <Polyline id="Path-4" points="-4.13021539e-13 15.2809927 7.2386697 9.07894737 23.1199862 25.6468442 47.6654018 0"></Polyline>
                                <Polyline id="Path-5" points="34.9601952 0.599943462 47.6654018 0 47.6654018 12.8234221"></Polyline>
                         </G>
                              <Path d="M0,0 L21.7828947,0 C22.8874642,-2.02906125e-16 23.7828947,0.8954305 23.7828947,2 L23.7828947,46.4210526 C23.7828947,47.5256221 22.8874642,48.4210526 21.7828947,48.4210526 L0,48.4210526 L0,48.4210526" id="Rectangle" stroke="#FF8B00" strokeLinejoin="round" transform="translate(11.891447, 24.210526) scale(-1, 1) translate(-11.891447, -24.210526) "></Path>
                        </G>
                    </G>
                </G>
            </G>
           </Svg>
              </View>
        )
    }
}
export default Growth
