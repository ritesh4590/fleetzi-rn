import React from 'react'
import { View } from 'react-native'
import Svg, { G, Polyline,Circle } from 'react-native-svg';


class Successfull extends React.Component {
	render() {
		return (
			<View>
				<Svg width="100px" height="100px" viewBox="0 0 216 216" version="1.1"  >
					<G id="App-Screens---Partners" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
						<G id="11-Create-New-Listing" transform="translate(-432.000000, -887.000000)" stroke="#1DC78D" strokeWidth="12">
							<G id="Group-4" transform="translate(432.000000, 887.000000)">
								<Polyline id="Line-14" strokeLinecap="round" transform="translate(108.000000, 100.000000) rotate(-45.000000) translate(-108.000000, -100.000000) " points="72 82 72 118 144 118"></Polyline>
								<Circle id="Oval" cx="108" cy="108" r="102"></Circle>
							</G>
						</G>
					</G>
				</Svg>
			</View>
		)
	}
}
export default Successfull