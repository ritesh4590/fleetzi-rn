import React from 'react'
import { View } from 'react-native'
import Svg, { G, Rect, Defs, Use, Text, TSpan, Ellipse, Polyline } from 'react-native-svg';


class DoneComp extends React.Component {
	render() {
		return (
			<View>
				<Svg width="50px" height="50px" viewBox="0 0 108 35" version="1.1">
					<Defs>
						<Rect id="path-1" x="0" y="0" width="108" height="35" rx="17.5"></Rect>
					</Defs>
					<G id="App-Screens---Partners" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
						<G id="Login-05" transform="translate(-541.000000, -893.000000)">
							<G id="Tag" transform="translate(541.000000, 893.000000)">
								<G id="Rectangle">
									<Use fillOpacity="0.102081512" fill="#FFFFFF" href="#path-1"></Use>
									<Rect strokeOpacity="0.35" stroke="#FF9800" strokeWidth="1" strokeLinejoin="square" fillOpacity="0.101890297" fill="#FF9800" x="0.5" y="0.5" width="107" height="34" rx="17"></Rect>
								</G>
								<Text id="Done" fontFamily="NexaBold, Nexa Bold" fontSize="20" fontWeight="bold" fill="#FF9800">
									<TSpan x="41" y="25">Done</TSpan>
								</Text>
								<Ellipse id="Oval" stroke="#FF9800" strokeWidth="3" cx="20" cy="17.5" rx="9.5" ry="10"></Ellipse>
								<Polyline id="Line-7" stroke="#FF9800" strokeWidth="3" strokeLinecap="square" points="16 18.6952243 18.2201931 21 24 15"></Polyline>
							</G>
						</G>
					</G>
				</Svg>
			</View>
		)
	}
}
export default DoneComp