import React from 'react'
import { View } from 'react-native'
import Svg, { G, Path,Rect } from 'react-native-svg';


class NewListingSvg extends React.Component {
	render() {
		return (
			<View>
				<Svg width="20px" height="20px" viewBox="0 0 48 42" version="1.1" >
					<G id="App-Screens---Partners" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
						<G id="Login-06" transform="translate(-95.000000, -1419.000000)">
							<G id="Icons" transform="translate(94.000000, 823.000000)">
								<G id="Create-New-Listing" transform="translate(1.000000, 596.000000)">
									<Path d="M41.3793103,0 C45.0358163,0 48,3.00864648 48,6.72 L48,35.28 C48,38.9913535 45.0358163,42 41.3793103,42 L6.62068966,42 C2.96418373,42 0,38.9913535 0,35.28 L0,6.72 C0,3.00864648 2.96418373,0 6.62068966,0 L41.3793103,0 Z M41.3793103,5.04 L6.62068966,5.04 C5.77185792,5.04 5.07226401,5.68854752 4.9766528,6.52407651 L4.96551724,6.72 L4.96551724,35.28 C4.96551724,36.1415642 5.60448031,36.851652 6.42766159,36.9486974 L6.62068966,36.96 L41.3793103,36.96 C42.2281421,36.96 42.927736,36.3114525 43.0233472,35.4759235 L43.0344828,35.28 L43.0344828,6.72 C43.0344828,5.85843579 42.3955197,5.14834797 41.5723384,5.05130259 L41.3793103,5.04 Z" id="Rectangle-Copy-7" fill="#FFFFFF" fillRule="nonzero"></Path>
									<Rect id="Rectangle" fill="#FF9800" x="10" y="13" width="21" height="5" rx="2.5"></Rect>
									<Rect id="Rectangle-Copy-10" fill="#FF9800" x="9" y="22" width="15" height="5" rx="2.5"></Rect>
								</G>
							</G>
						</G>
					</G>
				</Svg>
			</View>
		)
	}
}
export default NewListingSvg