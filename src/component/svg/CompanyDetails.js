import React from 'react'
import { View } from 'react-native'
import Svg, { G, Path, Rect } from 'react-native-svg';

class CompanyDetails extends React.Component {
	render() {
		return (
			<View>
				<Svg width="20px" height="20px" viewBox="0 0 45 49" version="1.1" >
					<G id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
						<G id="Login-06" transform="translate(-59.000000, -558.000000)">
							<G id="Company-Address" transform="translate(59.000000, 558.000000)">
								<G id="Company-Details">
									<Path d="M20.5396627,0.487188741 L2.20666447,10.2049802 C0.176242942,11.2812478 -0.591881929,13.7896488 0.491009554,15.8076507 C1.2151374,17.1570854 2.62864127,18 4.16741295,18 L40.8334095,18 C43.1345539,18 45,16.1459645 45,13.8588957 C45,12.3295364 44.1518977,10.9246787 42.794158,10.2049802 L24.4611597,0.487188741 C23.2356919,-0.162396247 21.7651305,-0.162396247 20.5396627,0.487188741 Z M22.5004112,5.07959709 L37.5001371,13.0305173 L7.49901873,13.0305173 L22.5004112,5.07959709 Z" id="Line-8-Copy" fill="#FFFFFF" fillRule="nonzero"></Path>
									<Path d="M42.5,44 C43.8807119,44 45,45.1192881 45,46.5 C45,47.7994935 44.0085199,48.8674145 42.7407667,48.9885557 L42.5,49 L2.5,49 C1.11928813,49 0,47.8807119 0,46.5 C0,45.2005065 0.991480139,44.1325855 2.25923332,44.0114443 L2.5,44 L42.5,44 Z" id="Line-8" fill="#FFFFFF" fillRule="nonzero"></Path>
									<Rect id="Rectangle" fill="#FF9800" x="6" y="23" width="21" height="5" rx="2.5"></Rect>
									<Rect id="Rectangle-Copy-10" fill="#FF9800" x="6" y="32" width="15" height="5" rx="2.5"></Rect>
								</G>
							</G>
						</G>
					</G>
				</Svg>
			</View>
		)
	}
}
export default CompanyDetails