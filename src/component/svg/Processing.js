import React from 'react'
import { View } from 'react-native'
import Svg, { G, Rect,Defs,Use,Text,TSpan,Circle,Polyline } from 'react-native-svg';

class ProcessingComp extends React.Component {
	render() {
		return (
			<View>
				<Svg width="70px" height="70px" viewBox="0 0 165 35" version="1.1">
					<Defs>
						<Rect id="path-1" x="0" y="0" width="165" height="34.9482631" rx="17.4741316"></Rect>
					</Defs>
					<G id="App-Screens---Partners" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
						<G id="Login-05" transform="translate(-380.000000, -1517.000000)">
							<G id="Tag-Copy" transform="translate(380.000000, 1517.000000)">
								<G id="Rectangle">
									<Use fillOpacity="0.102081512" fill="#FFFFFF" href="#path-1"></Use>
									<Rect strokeOpacity="0.35" stroke="#95FF00" strokeWidth="1" strokeLinejoin="square" fillOpacity="0.0958533654" fill="#95FF00" x="0.5" y="0.5" width="164" height="33.9482631" rx="16.9741316"></Rect>
								</G>
								<Text id="Processing" fontFamily="NexaBold, Nexa Bold" fontSize="20" fontWeight="bold" fill="#95FF00">
									<TSpan x="43.4297857" y="23.2259793">Processing</TSpan>
								</Text>
								<Circle id="Oval" stroke="#95FF00" strokeWidth="3" cx="19.9704361" cy="17.4741316" r="9.73337029"></Circle>
								<Polyline id="Line-7" stroke="#95FF00" strokeWidth="3" strokeLinecap="square" points="19.9704361 13.7296748 19.9704361 18.019097 25.2008728 18.019097"></Polyline>
							</G>
						</G>
					</G>
				</Svg>
			</View>
		)
	}
}
export default ProcessingComp