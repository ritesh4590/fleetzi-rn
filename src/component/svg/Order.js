import React from 'react'
import {View} from 'react-native'
import Svg, { SvgUri, G, Path, Defs, LinearGradient,Circle,Stop,Rect,Line } from'react-native-svg';


class order extends React.Component{
    render(){
        return(
            <View style={{alignItems:'center'}}>
             
            <Svg width="25px" height="25px" viewBox="0 0 86 86" version="1.1" >
    
                <G id="Partner" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <G id="02-Dashboard-Home" transform="translate(-632.000000, -2225.000000)" strokeWidth="3">
                        <G id="Bottom-Bar" transform="translate(0.000000, 2164.000000)">
                            <G id="Order" transform="translate(632.000000, 61.000000)">
                                <G id="Rectangle">
                                    <Rect stroke="#FFFFFF" x="3" y="3" width="80" height="80" rx="8"></Rect>
                                    <Path d="M31,3 L31,45.9435166 L40.0757663,37.4137804 C42.0003058,35.605028 44.9996942,35.605028 46.9242337,37.4137804 L56,45.9435166 L56,3 L31,3 Z" stroke="#FF8B00"></Path>
                                </G>
                            </G>
                        </G>
                    </G>
                </G>
            </Svg>
              </View>
        )
    }
}
export default order
