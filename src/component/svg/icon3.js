import React from 'react'
import {View} from 'react-native'
import Svg, { SvgUri, G, Path, Defs, LinearGradient,Circle,Stop,Rect,Mask,Use } from'react-native-svg';


class Icon3 extends React.Component{
    render(){
        return(
            <View>
            <Svg   width="30" height="30" viewBox="0 0 129.772 129.772">
                <G id="Group_3" dataName="Group 3" transform="translate(5845.386 -218.114)">
                    <G id="Group" transform="translate(-5846 217.5)">
                    <G id="Oval" transform="matrix(-0.985, 0.174, -0.174, -0.985, 120.527, 104.03)" fill="none" stroke="#707070" strokeMiterlimit="10" strokeWidth="21" strokeDasharray="116 269">
                        <Circle cx="47.5" cy="47.5" r="47.5" stroke="none"/>
                        <Circle cx="47.5" cy="47.5" r="37" fill="none"/>
                    </G>
                    <G id="Oval-2" dataName="Oval" transform="matrix(0.017, -1, 1, 0.017, 17.178, 112.164)" fill="none" stroke="#707070" strokeMiterlimit="10" strokeWidth="13" strokeDasharray="84 220" opacity="0.446">
                        <Circle cx="47.5" cy="47.5" r="47.5" stroke="none"/>
                        <Circle cx="47.5" cy="47.5" r="41" fill="none"/>
                    </G>
                    <G id="Oval-3" dataName="Oval" transform="translate(48.114 0.614) rotate(30)" fill="none" stroke="#707070" strokeMiterlimit="10" strokeWidth="30" strokeDasharray="99 230" opacity="0.196">
                        <Circle cx="47.5" cy="47.5" r="47.5" stroke="none"/>
                        <Circle cx="47.5" cy="47.5" r="32.5" fill="none"/>
                    </G>
                    </G>
                </G>
            </Svg>


              </View>
        )
    }
}
export default Icon3