import React from 'react'
import {View} from 'react-native'
import Svg, { SvgUri, G, Path, Defs, LinearGradient,Circle,Stop,Rect,Line } from'react-native-svg';


class Star extends React.Component{
    render(){
        return(
            <View>
                 <Svg width="30px" height="30px" viewBox="0 0 109 109" version="1.1" >
                    <G id="Partner" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                         <G id="06-Manage-Listing" transform="translate(-894.000000, -491.000000)">
                            <G id="Card-01" transform="translate(62.000000, 480.000000)">
                               <G id="Group-6" transform="translate(832.000000, 11.000000)">
                                  <Circle id="Oval" fill="#FFFFFF" opacity="0.0297154018" cx="54.5" cy="54.5" r="54.5"></Circle>
                                     <Path d="M43.3664718,82.6161077 L42.3626171,83.1438654 C38.9180605,84.9547759 34.6576627,83.6304442 32.8467522,80.1858876 C32.1256381,78.8142472 31.8767995,77.2431422 32.1387595,75.7157981 L32.3304787,74.5979894 C33.7214272,66.4881377 31.0327151,58.2131327 25.1405629,52.4697038 L24.3284273,51.6780673 C21.5417226,48.9617015 21.4847016,44.5005814 24.2010674,41.7138766 C25.2827385,40.6041961 26.7000528,39.8820384 28.2335934,39.6592019 L29.3559372,39.4961158 C37.4986912,38.3129044 44.5378309,33.1986701 48.1793812,25.8200832 L48.6813085,24.8030673 C50.4035868,21.3133504 54.6287437,19.8805582 58.1184607,21.6028365 C59.5080843,22.2886568 60.6328712,23.4134437 61.3186915,24.8030673 L61.8206188,25.8200832 C65.4621691,33.1986701 72.5013088,38.3129044 80.6440628,39.4961158 L81.7664066,39.6592019 C85.6175378,40.2188041 88.2858494,43.79441 87.7262473,47.6455413 C87.5034108,49.1790819 86.7812531,50.5963962 85.6715727,51.6780673 L84.8594371,52.4697038 C78.9672849,58.2131327 76.2785728,66.4881377 77.6695213,74.5979894 L77.8612405,75.7157981 C78.5190922,79.5513681 75.9430426,83.1940063 72.1074725,83.851858 C70.5801284,84.113818 69.0090234,83.8649795 67.6373829,83.1438654 L66.6335282,82.6161077 C59.3504276,78.7871551 50.6495724,78.7871551 43.3664718,82.6161077 Z" id="Star" fill="#2DB892"></Path>
                                        <G id="Group" transform="translate(55.142136, 52.142136) rotate(-45.000000) translate(-55.142136, -52.142136) translate(44.142136, 47.142136)" stroke="#2B2D2E" strokeLinecap="round" strokeWidth="6">
                                           <Line x1="1.25" y1="0.656854249" x2="1.25" y2="8.72326025" id="Line-11"></Line>
                                           <Line x1="11.7237083" y1="-0.966348968" x2="11.7237083" y2="18.066854" id="Line-11" transform="translate(11.723708, 8.550253) rotate(90.000000) translate(-11.723708, -8.550253) "></Line>
                                        </G>
                                    </G>
                                </G>
                            </G>
                        </G>
                    </Svg>
              </View>
        )
    }
}
export default Star
