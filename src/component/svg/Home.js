import React from 'react'
import {View} from 'react-native'
import Svg, { SvgUri, G, Path, Defs, LinearGradient,Circle,Stop,Rect,Line } from'react-native-svg';


class Home extends React.Component{
    render(){
        return(
            <View style={{alignItems:'center'}}>
            <Svg width="25px" height="25px" viewBox="0 0 76 86" version="1.1" >
     
                <G id="Partner" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <G id="02-Dashboard-Home" transform="translate(-97.000000, -2225.000000)" strokeWidth="3">
                        <G id="Bottom-Bar" transform="translate(0.000000, 2164.000000)">
                            <G id="Home" transform="translate(92.000000, 61.000000)">
                                <G transform="translate(5.000000, 0.000000)">
                                    <Path d="M5.55393269,33.2693285 C3.9164118,34.9499425 3,37.2036364 3,39.55011 L3,73.6706964 C3,78.6412591 7.02943725,82.6706964 12,82.6706964 L64,82.6706964 C68.9705627,82.6706964 73,78.6412591 73,73.6706964 L73,39.55011 C73,37.2036364 72.0835882,34.9499425 70.4460673,33.2693285 L43.0136079,5.11495308 C42.9713143,5.07154651 42.9284588,5.02869101 42.8850523,4.98639744 C40.1161131,2.28845757 35.684332,2.34601389 32.9863921,5.11495308 L5.55393269,33.2693285 Z" id="Rectangle" stroke="#FFFFFF"></Path>
                                    <Line x1="21.5" y1="59.5" x2="55.3984375" y2="59.5" id="Line-4" stroke="#FF8B00" strokeLinecap="round"></Line>
                                </G>
                            </G>
                        </G>
                    </G>
                </G>
            </Svg>
              </View>
        )
    }
}
export default Home
