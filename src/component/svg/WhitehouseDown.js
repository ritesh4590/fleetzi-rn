import React from 'react'
import {View} from 'react-native'
import Svg, { SvgUri, G, Path, Defs, LinearGradient,Circle,Stop,Rect,Line } from'react-native-svg';


class WhitehouseDown extends React.Component{
    render(){
        return(
            <View>
                <Svg width="50px" height="50px" viewBox="0 0 113 115" version="1.1">
                    <G id="Partner" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                        <G id="10-Payments" transform="translate(-851.000000, -1879.000000)">
                            <G id="Card-03" transform="translate(62.000000, 1689.000000)">
                                <G id="Icons" transform="translate(791.000000, 192.113402)">
                                    <G id="Group-2">
                                        <Line x1="41.5" y1="43.1597938" x2="41.5" y2="95.6134021" id="Line-13-Copy" stroke="#979797" strokeWidth="6" strokeLinecap="round"></Line>
                                        <Line x1="93.5" y1="43.1597938" x2="93.5" y2="95.6134021" id="Line-13-Copy-3" stroke="#979797" strokeWidth="6" strokeLinecap="round"></Line>
                                        <Line x1="67.5" y1="43.1597938" x2="67.5" y2="95.6134021" id="Line-13-Copy-2" stroke="#979797" strokeWidth="6" strokeLinecap="round"></Line>
                                        <Line x1="54.5" y1="49.3865979" x2="54.7268041" y2="143.613402" id="Line-13-Copy-4" stroke="#979797" strokeWidth="6" strokeLinecap="round" transform="translate(55.000000, 96.613402) rotate(-90.000000) translate(-55.000000, -96.613402) "></Line>
                                        <Line x1="54.6134021" y1="-20.5" x2="54.6134021" y2="86.5" id="Line-13-Copy-5" stroke="#979797" strokeWidth="6" strokeLinecap="round" transform="translate(54.500000, 33.000000) rotate(-90.000000) translate(-54.500000, -33.000000) "></Line>
                                        <Line x1="15.5" y1="-7.22680412" x2="47.1134021" y2="40" id="Line-13-Copy-5" stroke="#979797" strokeWidth="6" strokeLinecap="round" transform="translate(31.500000, 16.500000) rotate(-90.000000) translate(-31.500000, -16.500000) "></Line>
                                        <Line x1="62.806701" y1="-7.22680412" x2="94.4201031" y2="40.306701" id="Line-13-Copy-5" stroke="#979797" strokeWidth="6" strokeLinecap="round" transform="translate(78.806701, 16.500000) scale(-1, 1) rotate(-90.000000) translate(-78.806701, -16.500000) "></Line>
                                        <Circle id="Oval" fill="#979797" cx="55" cy="19.8865979" r="6"></Circle>
                                        <Line x1="15.5" y1="43.1597938" x2="15.5" y2="95.6134021" id="Line-13" stroke="#979797" strokeWidth="6" strokeLinecap="round"></Line>
                                    </G>
                                    <G id="Group" transform="translate(59.000000, 60.886598)">
                                        <Circle id="Oval" fill="#1D2122" cx="26" cy="26" r="26"></Circle>
                                        <Line x1="18.0374632" y1="34.781722" x2="40.0460366" y2="12" id="Line-13-Copy-6" stroke="#00C08D" strokeWidth="6" strokeLinecap="round"></Line>
                                        <Line x1="18.2960366" y1="18" x2="17.7960366" y2="34.4536082" id="Line-13-Copy-2" stroke="#00C08D" strokeWidth="6" strokeLinecap="round" transform="translate(18.296037, 26.226804) rotate(-2.000000) translate(-18.296037, -26.226804) "></Line>
                                        <Line x1="18.0730284" y1="35.4766888" x2="34.534232" y2="35.4766888" id="Line-13-Copy-2" stroke="#00C08D" strokeWidth="6" strokeLinecap="round"></Line>
                                    </G>
                                </G>
                            </G>
                        </G>
                    </G>
                </Svg>
            </View>
        )
    }
}
export default WhitehouseDown
