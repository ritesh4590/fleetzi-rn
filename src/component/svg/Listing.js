import React from 'react'
import {View} from 'react-native'
import Svg, { SvgUri, G, Path, Defs, LinearGradient,Circle,Stop,Rect,Line } from'react-native-svg';


class ListingSVG extends React.Component{
    render(){
        return(
            <View style={{alignItems:'center'}}>
             
                    <Svg width="25px" height="25px" viewBox="0 0 69 86" version="1.1" >
                        
                        <G id="Partner" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                            <G id="02-Dashboard-Home" transform="translate(-371.000000, -2225.000000)" strokeWidth="3">
                                <G id="Bottom-Bar" transform="translate(0.000000, 2164.000000)">
                                    <G id="Listing" transform="translate(358.000000, 61.000000)">
                                        <G transform="translate(13.000000, 0.000000)">
                                            <Path d="M51.7573593,83 L66,68.7573593 L66,12 C66,7.02943725 61.9705627,3 57,3 L12,3 C7.02943725,3 3,7.02943725 3,12 L3,74 C3,78.9705627 7.02943725,83 12,83 L51.7573593,83 Z" id="Rectangle" stroke="#FFFFFF"></Path>
                                            <Line x1="17.5" y1="21.5" x2="51.3984375" y2="21.5" id="Line-4" stroke="#FF8B00" strokeLinecap="round"></Line>
                                            <Line x1="17.5" y1="37.5" x2="39" y2="37.5" id="Line-4-Copy" stroke="#FF8B00" strokeLinecap="round"></Line>
                                        </G>
                                    </G>
                                </G>
                            </G>
                        </G>
                    </Svg>
              </View>
        )
    }
}
export default ListingSVG
