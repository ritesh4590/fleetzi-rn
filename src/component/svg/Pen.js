import React from 'react'
import {View} from 'react-native'
import Svg, { G, Path,Line } from'react-native-svg';


class Pen extends React.Component{
    render(){
        return(
            <View>
            <Svg width="15px" height="15px" viewBox="0 0 55 55" version="1.1">
            <G id="Partner" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <G id="05-Create-New-Listing" transform="translate(-948.000000, -499.000000)" stroke="#FFFFFF" strokeWidth="5">
                    <G id="Group-4" transform="translate(940.000000, 493.999952)">
                        <Path d="M34,1.5 C28.7532949,1.5 24.5,5.75329488 24.5,11 L24.5,46.7751887 C24.5,48.1736609 24.8087538,49.5548862 25.4042177,50.8202507 L31.737952,64.2794761 C31.9856495,64.8058348 32.4091513,65.2293366 32.93551,65.4770341 C34.1848046,66.0649357 35.6741464,65.5287707 36.262048,64.2794761 L42.5957823,50.8202507 C43.1912462,49.5548862 43.5,48.1736609 43.5,46.7751887 L43.5,11 C43.5,5.75329488 39.2467051,1.5 34,1.5 Z" id="Rectangle" transform="translate(34.000000, 34.454545) rotate(45.000000) translate(-34.000000, -34.454545) "></Path>
                        <Line x1="18.5" y1="39.5000483" x2="29.5" y2="49.5000483" id="Line-10" strokeLinecap="square"></Line>
                    </G>
                </G>
            </G>
            </Svg>
              </View>
        )
    }
}
export default Pen