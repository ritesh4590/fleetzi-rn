import React from 'react'
import { View } from 'react-native'
import Svg, { SvgUri, G, Path, Line, LinearGradient, Circle, Stop, Rect } from 'react-native-svg';


class IncorrectOTP extends React.Component {
	render() {
		return (
			<View>
				<Svg width="30px" height="30px" viewBox="0 0 46 46" version="1.1">
					<G id="App-Screens---Partners" stroke="none" strokWidth="3" fill="none" fillRule="evenodd">
						<G id="Group-9" stroke="#FF2929" strokeWidth="4">
							<Circle id="Oval" cx="23" cy="23" r="21.5"></Circle>
							<Line x1="18.2338244" y1="22.3823648" x2="26.0204087" y2="14.5957805" id="Line-7" strokeLinecap="square" transform="translate(22.127117, 18.489073) rotate(-45.000000) translate(-22.127117, -18.489073) "></Line>
							<Line x1="20.2878616" y1="34.4358779" x2="23.8739031" y2="30.8498364" id="Line-7" transform="translate(22.080882, 32.642857) rotate(-45.000000) translate(-22.080882, -32.642857) "></Line>
						</G>
					</G>
				</Svg>

			</View>
		)
	}
}
export default IncorrectOTP