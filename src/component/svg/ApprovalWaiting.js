import React from 'react'
import { View } from 'react-native'
import Svg, { G, Polyline, TSpan, Defs, Rect, Use, Text, Circle } from 'react-native-svg';

class ApprovalWaiting extends React.Component {
	render() {
		return (
			<View>
				<Svg width="95px" height="100px" viewBox="0 0 254 35" version="1.1">
					<Defs>
						<Rect id="path-1" x="0" y="2.84217094e-14" width="254" height="34.9482631" rx="17.4741316"></Rect>
					</Defs>
					<G id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
						<G id="Login-06" transform="translate(-767.000000, -716.000000)">
							<G id="Waiting-For-Approval" transform="translate(767.000000, 716.000000)">
								<G id="Rectangle">
									<Use fillOpacity="0.102081512" fill="#FFFFFF" href="#path-1"></Use>
									<Rect strokeOpacity="0.35" stroke="#FF9800" strokeWidth="1" strokeLinejoin="square" fillOpacity="0.1" fill="#FF9800" x="0.5" y="0.5" width="253" height="33.9482631" rx="16.9741316"></Rect>
								</G>
								<G id="Group-5" transform="translate(10.000000, 5.000000)">
									<Text id="Waiting-for-Approval" fontFamily="NexaBold, Nexa Bold" fontSize="20" fontWeight="bold" fill="#FF9800">
										<TSpan x="34.6927199" y="19">Waiting for Approval</TSpan>
									</Text>
									<Circle id="Oval" stroke="#FF9800" strokeWidth="3" cx="11.2333703" cy="13.2333703" r="9.73337029"></Circle>
									<G id="Group-3" transform="translate(10.000000, 7.000000)" fill="#FF9800">
										<Rect id="Rectangle" x="0" y="0" width="3" height="7"></Rect>
										<Circle id="Oval" cx="1.5" cy="10.5" r="1.5"></Circle>
									</G>
								</G>
							</G>
						</G>
					</G>
				</Svg>

			</View>
		)
	}
}
export default ApprovalWaiting