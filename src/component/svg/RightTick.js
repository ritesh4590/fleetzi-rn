import React from 'react'
import { View } from 'react-native'
import Svg, { G, Path, Ellipse } from 'react-native-svg';


class RightTick extends React.Component {
	render() {
		return (
			<View>

<Svg width="29px" height="21px" viewBox="0 0 29 21" version="1.1">
    <G id="App-Screens---Partners" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <G id="01-Create-New-Listing" transform="translate(-186.000000, -2719.000000)" fill="#FF9600" fillRule="nonzero">
            <G id="Group-4" transform="translate(171.500000, 2702.000000)">
                <Path d="M42.5899468,17.4187952 C42.0432365,16.8604016 41.1568626,16.8604016 40.610043,17.4187952 L23.8371884,34.5485422 L17.3899776,27.9642121 C16.8432673,27.4058186 15.9568934,27.4058744 15.4100737,27.9642121 C14.8633088,28.5225498 14.8633088,29.4277751 15.4100737,29.9861687 L22.8472365,37.5813933 C23.3937827,38.139731 24.2808129,38.1393401 24.8271403,37.5813933 L42.5899468,19.4408076 C43.1367117,18.8824699 43.1366571,17.9771887 42.5899468,17.4187952 Z" id="Path"></Path>
            </G>
        </G>
    </G>
</Svg>
			</View>
		)
	}
}
export default RightTick