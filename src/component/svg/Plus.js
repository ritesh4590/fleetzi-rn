import React from 'react'
import {View} from 'react-native'
import Svg, { G, Path,Line,Polygon } from'react-native-svg';


class Plus extends React.Component{
    render(){
        return(
            <View>
                <Svg width="15px" height="15px" viewBox="0 0 28 28" version="1.1">
                    
                    <G id="Partner" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                        <G id="05-Create-New-Listing" transform="translate(-396.000000, -1981.000000)" fill="#FF8B00" fillRule="nonzero">
                            <Polygon id="+" points="411.657431 2009 411.657431 1996.51637 424 1996.51637 424 1993.13098 411.657431 1993.13098 411.657431 1981 408.342569 1981 408.342569 1993.13098 396 1993.13098 396 1996.51637 408.342569 1996.51637 408.342569 2009"></Polygon>
                        </G>
                    </G>
                </Svg>
              </View>
        )
    }
}
export default Plus