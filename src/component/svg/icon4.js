import React from 'react'
import {View} from 'react-native'
import Svg, { SvgUri, G, Path, Defs, LinearGradient,Circle,Stop,Rect } from'react-native-svg';


class Icon4 extends React.Component{
    render(){
        return(
            <View>
            <Svg  width="30" height="30" viewBox="0 0 129.772 129.772">
                <Defs>
                    <LinearGradient id="linear-gradient" y1="0.5" x2="1" y2="0.5" gradientUnits="objectBoundingBox">
                    <Stop offset="0" stopColor="#ffac00"/>
                    <Stop offset="1" stopColor="#ff8b00"/>
                    </LinearGradient>
                </Defs>
                <G id="Group_4" dataName="Group 4" transform="translate(5845.386 -808.114)">
                    <G id="Oval" transform="translate(-5797.886 808.114) rotate(30)" fill="none" stroke="#707070" strokeMiterlimit="10" strokeWidth="15">
                    <Circle cx="47.5" cy="47.5" r="47.5" stroke="none"/>
                    <Circle cx="47.5" cy="47.5" r="40" fill="none"/>
                    </G>
                    <Circle id="Oval-2" dataName="Oval" cx="12.5" cy="12.5" r="12.5" transform="translate(-5793 860.5)" fill="url(#linear-gradient)"/>
                </G>
            </Svg>

              </View>
        )
    }
}
export default Icon4