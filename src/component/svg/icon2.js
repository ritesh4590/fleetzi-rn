import React from 'react'
import {View} from 'react-native'
import Svg, { SvgUri, G, Path, Defs, LinearGradient,Circle,Stop,Rect } from'react-native-svg';


class Icon2 extends React.Component{
    render(){
        return(
            <View>
            <Svg  width="30" height="30" viewBox="0 0 95 95">
                <Defs>
                    <LinearGradient id="linear-gradient" y1="0.5" x2="1" y2="0.5" gradientUnits="objectBoundingBox">
                    <Stop offset="0" stopColor="#ffac00"/>
                    <Stop offset="1" stopColor="#ff8b00"/>
                    </LinearGradient>
                </Defs>
                <Path id="Oval" d="M47.5,95A47.5,47.5,0,0,0,95,47.5c-10.891.211-47.5,0-47.5,0s.25-43.657,0-47.5a47.5,47.5,0,0,0,0,95Z" fill="url(#linear-gradient)"/>
            </Svg>

              </View>
        )
    }
}
export default Icon2