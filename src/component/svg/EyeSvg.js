import React from 'react'
import {View} from 'react-native'
import Svg, { G, Path } from'react-native-svg';


class EyeSvg extends React.Component{
    render(){
        return(
            <View>
              <Svg width="20" height="28" viewBox="0 0 50 28" fill="none" >
						<Path d="M33 14C33 17.866 29.866 21 26 21C22.134 21 19 17.866 19 14C19 10.134 22.134 7 26 7C29.866 7 33 10.134 33 14Z" stroke="#979797" strokeOpacity="0.40218" strokeWidth="4"/>
						<Path d="M38.8888 7.04021L46.9414 14L38.8888 20.9598C35.1271 24.2109 30.3212 26 25.3493 26C20.6118 26 16.0175 24.3754 12.3331 21.3973L3.18154 14L12.3331 6.60272C16.0175 3.6246 20.6118 2 25.3493 2C30.3212 2 35.1271 3.78905 38.8888 7.04021Z" stroke="white" strokeOpacity="0.40218" strokeWidth="4" strokeLinejoin="round"/>
					</Svg>
              </View>
        )
    }
}
export default EyeSvg