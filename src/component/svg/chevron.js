import React from 'react'
import {View} from 'react-native'
import Svg, { SvgUri, G, Path, Defs, LinearGradient,Circle,Stop,Rect } from'react-native-svg';


class Chevron extends React.Component{
    render(){
        return(
            <View>
            <Svg width="20px" height="20px" viewBox="0 0 25 41" version="1.1"  >
                              <G id="Partner" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                  <G id="02-Dashboard-Home" transform="translate(-612.000000, -443.000000)" fill="#FFFFFF" fill-rule="nonzero">
                                      <G id="New-Order-Tab" transform="translate(118.000000, 399.000000)">
                                          <G id="Arrow" transform="translate(465.000000, 26.000000)">
                                              <Path d="M41.2132034,26.2132034 C41.2132034,24.5563492 42.5563492,23.2132034 44.2132034,23.2132034 C45.8700577,23.2132034 47.2132034,24.5563492 47.2132034,26.2132034 L47.2132034,53.2132034 L20.2132034,53.2132034 C18.5563492,53.2132034 17.2132034,51.8700577 17.2132034,50.2132034 C17.2132034,48.5563492 18.5563492,47.2132034 20.2132034,47.2132034 L41.2132034,47.2132034 L41.2132034,26.2132034 Z" id="Rectangle" transform="translate(32.213203, 38.213203) rotate(-45.000000) translate(-32.213203, -38.213203) "></Path>
                                          </G>
                                      </G>
                                  </G>
                              </G>
                          </Svg>
              </View>
        )
    }
}
export default Chevron