import React from 'react'
import {View} from 'react-native'
import Svg, { SvgUri, G, Path, Defs, LinearGradient,Circle,Stop,Rect } from'react-native-svg';


class Icon extends React.Component{
    render(){
        return(
            <View>
            <Svg width="30" height="30" viewBox="0 0 96 101">
                        <Defs>
                          <LinearGradient id="linear-gradient" y1="0.5" x2="1" y2="0.5" gradientUnits="objectBoundingBox">
                            <Stop offset="0" stopColor="#ffac00"/>
                            <Stop offset="1" stopColor="#ff8b00"/>
                          </LinearGradient>
                        </Defs>
                        <G id="Group_2" dataName="Group 2" transform="translate(5821.5 29.5)">
                          <G id="Group_2-2" dataName="Group 2" transform="translate(-5822 -29.5)">
                            <Path id="Line_8" dataName="Line 8" d="M0,.5H100" transform="translate(1.5 0.5) rotate(90)" fill="none" stroke="#fff" strokeLinecap="square" strokeMiterlimit="10" strokeWidth="1" opacity="0.196"/>
                            <Path id="Line_8-2" dataName="Line 8" d="M0,.5H100" transform="translate(33.167 0.5) rotate(90)" fill="none" stroke="#fff" strokeLinecap="square" strokeMiterlimit="10" strokeWidth="1" opacity="0.196"/>
                            <Path id="Line_8-3" dataName="Line 8" d="M0,.5H100" transform="translate(64.833 0.5) rotate(90)" fill="none" stroke="#fff" strokeLinecap="square" strokeMiterlimit="10" strokeWidth="1" opacity="0.196"/>
                            <Path id="Line_8-4" dataName="Line 8" d="M0,.5H100" transform="translate(96.5 0.5) rotate(90)" fill="none" stroke="#fff" strokeLinecap="square" strokeMiterlimit="10" strokeWidth="1" opacity="0.196"/>
                            <Path id="Rectangle" d="M0,31.918l31.629,13.2L63.336,8.129,95,0V79.086a6,6,0,0,1-6,6H6a6,6,0,0,1-6-6Z" transform="translate(1 15.914)" fill="url(#linear-gradient)"/>
                          </G>
                        </G>
                      </Svg>
              </View>
        )
    }
}
export default Icon