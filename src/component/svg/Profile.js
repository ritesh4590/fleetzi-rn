import React from 'react'
import {View} from 'react-native'
import Svg, { SvgUri, G, Path, Defs, TSpan,Polygon,Stop,Rect, Text } from'react-native-svg';


class Profile extends React.Component{
    render(){
        return(
            <View>
                <Svg width="25px" height="25px" viewBox="0 0 66 63" version="1.1" >
                    <G id="Partner" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                        <G id="02-Dashboard-Home" transform="translate(-961.000000, -71.000000)" fill="#FFFFFF" fillRule="nonzero">
                            <G id="Top-Bar" transform="translate(59.000000, 60.000000)">
                                <Path d="M935,11 C943.284271,11 950,17.7157288 950,26 C950,31.4837315 947.057362,36.280173 942.665001,38.896408 C957.193946,42.3505988 968,55.4133548 968,71 C968,72.6568542 966.656854,74 965,74 C963.343146,74 962,72.6568542 962,71 C962,56.0883118 949.911688,44 935,44 C920.088312,44 908,56.0883118 908,71 C908,72.6568542 906.656854,74 905,74 C903.343146,74 902,72.6568542 902,71 C902,55.4133548 912.806054,42.3505988 927.335125,38.8947695 C922.942638,36.280173 920,31.4837315 920,26 C920,17.7157288 926.715729,11 935,11 Z M935,17 C930.029437,17 926,21.0294373 926,26 C926,30.9705627 930.029437,35 935,35 C939.970563,35 944,30.9705627 944,26 C944,21.0294373 939.970563,17 935,17 Z" id="Combined-Shape"></Path>
                            </G>
                        </G>
                    </G>
                </Svg>
              </View>
        )
    }
}
export default Profile