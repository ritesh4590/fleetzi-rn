import React from 'react'
import {View} from 'react-native'
import Svg, { SvgUri, G, Path, Defs, LinearGradient,Circle,Stop,Rect } from'react-native-svg';


class Bell extends React.Component{
    render(){
        return(
            <View>
            <Svg width="40" height="30" viewBox="0 0 63 78.5">
                  <Defs>
                  <LinearGradient id="linear-gradient" y1="0.5" x2="1" y2="0.5" gradientUnits="objectBoundingBox">
                  <Stop offset="0" stopColor="#ffac00"/>
                  <Stop offset="1" stopColor="#ff8b00"/>
                </LinearGradient>
              </Defs>
              <G id="Group_1" dataName="Group 1" transform="translate(5795 748.5)">
                <G id="Rectangle" transform="translate(-5776 -748.5)" fill="none" stroke="#fff" strokeMiterlimit="10" strokeWidth="6">
                  <Path d="M7.5,0h0A7.5,7.5,0,0,1,15,7.5V15a0,0,0,0,1,0,0H0a0,0,0,0,1,0,0V7.5A7.5,7.5,0,0,1,7.5,0Z" stroke="none"/>
                  <Path d="M7.5,3h0A4.5,4.5,0,0,1,12,7.5v4.361a.139.139,0,0,1-.139.139H3.139A.139.139,0,0,1,3,11.861V7.5A4.5,4.5,0,0,1,7.5,3Z" fill="none"/>
                </G>
                <G id="Rectangle-2" dataName="Rectangle" transform="translate(-5795 -737.5)" fill="none" stroke="#fff" strokeMiterlimit="10" strokeWidth="6">
                  <Path d="M26.5,0h0A26.5,26.5,0,0,1,53,26.5V49A11,11,0,0,1,42,60H11A11,11,0,0,1,0,49V26.5A26.5,26.5,0,0,1,26.5,0Z" stroke="none"/>
                  <Path d="M26.5,3h0A23.5,23.5,0,0,1,50,26.5V49a8,8,0,0,1-8,8H11a8,8,0,0,1-8-8V26.5A23.5,23.5,0,0,1,26.5,3Z" fill="none"/>
                </G>
                <G id="Oval" transform="translate(-5758 -728.5)" stroke="#313333" strokeMiterlimit="10" strokeWidth="6" fill="url(#linear-gradient)">
                  <Circle cx="10" cy="10" r="10" stroke="none"/>
                  <Circle cx="10" cy="10" r="13" fill="none"/>
                </G>
                <Path id="Line_2" dataName="Line 2" d="M0,.5H8" transform="translate(-5773 -673.5)" fill="none" stroke="#fff" strokeLinecap="round" strokeMiterlimit="10" strokeWidth="6"/>
              </G>
              </Svg>
              </View>
        )
    }
}
export default Bell