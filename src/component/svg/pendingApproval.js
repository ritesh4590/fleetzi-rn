import React from 'react'
import { View } from 'react-native'
import Svg, { G, Defs, Text, TSpan, Rect, Use, Ellipse, Polyline } from 'react-native-svg';
class PendingApproval extends React.Component {
	render() {
		return (
			<View>
				<Svg width="80px" height="95px" viewBox="0 0 218 35" version="1.1" >
					<Defs>
						<Rect id="path-1" x="0" y="0" width="218" height="35" rx="17.5"></Rect>
					</Defs>
					<G id="App-Screens---Partners" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
						<G id="Group-10">
							<G id="Rectangle">
								<Use fillOpacity="0.102081512" fill="#FFFFFF" href="#path-1"></Use>
								<Rect strokeOpacity="0.35" stroke="#FF9800" strokeWidth="1" strokeLinejoin="square" fillOpacity="0.101890297" fill="#FF9800" x="0.5" y="0.5" width="217" height="34" rx="17"></Rect>
							</G>
							<G id="Group-9" transform="translate(8.000000, 6.000000)">
								<Text id="Pending-Approval" fontFamily="NexaBold, Nexa Bold" fontSize="20" fontWeight="bold" fill="#FF9800">
									<TSpan x="32" y="19">Pending Approval</TSpan>
								</Text>
								<Ellipse id="Oval" stroke="#FF9800" strokeWidth="3" cx="11" cy="11.5" rx="9.5" ry="10"></Ellipse>
								<Polyline id="Line-7" stroke="#FF9800" strokeWidth="3" strokeLinecap="square" points="7 12.6952243 9.22019306 15 15 9"></Polyline>
							</G>
						</G>
					</G>
				</Svg>
			</View>
		)
	}
}
export default PendingApproval