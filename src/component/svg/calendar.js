import React from 'react'
import {View} from 'react-native'
import Svg, { SvgUri, G, Path, Defs, LinearGradient,Circle,Stop,Rect } from'react-native-svg';


class Calendar extends React.Component{
    render(){
        return(
            <View>
            <Svg id="Calendar"  width="25" height="25" viewBox="0 0 62 63">
                <G id="Rectangle" fill="none" stroke="#fff" strokeMiterlimit="10" strokeWidth="6">
                  <Rect width="62" height="63" rx="12" stroke="none"/>
                  <Rect x="3" y="3" width="56" height="57" rx="9" fill="none"/>
                </G>
                <Path id="Line" d="M.5.5h50" transform="translate(6 18)" fill="none" stroke="#fff" strokeLinecap="square" strokeMiterlimit="10" strokeWidth="6"/>
                <Rect id="Rectangle-2" dataName="Rectangle" width="8" height="8" rx="2" transform="translate(12 28)" fill="#fff"/>
                <Rect id="Rectangle_Copy_5" dataName="Rectangle Copy 5" width="8" height="8" rx="2" transform="translate(24 28)" fill="#fff"/>
              </Svg>
              </View>
        )
    }
}
export default Calendar