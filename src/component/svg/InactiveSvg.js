import React from 'react'
import { View } from 'react-native'
import Svg, { G, Path,Use,Defs,Rect,Text,TSpan,Circle,Line } from 'react-native-svg';

class InactiveSvg extends React.Component {
	render() {
		return (
			<View>

				<Svg width="65px" height="65px" viewBox="0 0 136 35" version="1.1">

					<Defs>
						<Rect id="path-1" x="0" y="0" width="136" height="35" rx="17.5"></Rect>
					</Defs>
					<G id="App-Screens---Partners" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
						<G id="Login-06" transform="translate(-818.000000, -1708.000000)">
							<G id="Inactive-">
								<G id="Inactive" transform="translate(818.000000, 1708.000000)">
									<G id="Rectangle">
										<Use fillOpacity="0.102081512" fill="#FFFFFF" href="#path-1"></Use>
										<Rect strokeOpacity="0.35" stroke="#FF0000" strokeWidth="1" strokeLinejoin="square" fillOpacity="0.0911549388" fill="#FF0000" x="0.5" y="0.5" width="135" height="34" rx="17"></Rect>
									</G>
									<Text fontFamily="NexaBold, Nexa Bold" fontSize="20" fontWeight="bold" fill="#FF2929">
										<TSpan x="42.5" y="23.25">Inactive</TSpan>
									</Text>
									<Circle id="Oval" stroke="#FF2929" strokeWidth="3" cx="20" cy="17.5" r="9.75"></Circle>
									<Line x1="17.8535536" y1="17.4420108" x2="21.6920106" y2="13.6035538" id="Line-7" stroke="#FF2929" strokeWidth="3" strokeLinecap="square" transform="translate(19.772782, 15.522782) rotate(-45.000000) translate(-19.772782, -15.522782) "></Line>
									<Line x1="18.8661165" y1="23.3838835" x2="20.6338835" y2="21.6161165" id="Line-7" stroke="#FF2929" strokeWidth="3" transform="translate(19.750000, 22.500000) rotate(-45.000000) translate(-19.750000, -22.500000) "></Line>
								</G>
							</G>
						</G>
					</G>
				</Svg>
			</View>
		)
	}
}
export default InactiveSvg