import React from 'react';
import { Col, Row, Grid } from "react-native-easy-grid";
import { View, Text, StyleSheet, Platform } from 'react-native';
import Flogo from './svg/Flogo'
import Contact from './svg/Contact'
import Profile from './svg/Profile'
class FooterComp extends React.Component{
render(){
 

  return (
      
    <Footer>
                <TouchableOpacity onPress={() => Actions.home()}>
                    <HomeFooter />
                    <Text style={{color:'white', textAlign:'center'}}>Home</Text>
                </TouchableOpacity>
                <View>
                  <ListingSVG />
                    <Text style={{color:'white', textAlign:'center'}}>Listing</Text>
                </View>
                <View>
                  <Order />
                    <Text style={{color:'white', textAlign:'center'}}>Order</Text>
                </View>
                <View>
                  <Payments />
                    <Text style={{color:'white', textAlign:'center'}}>Payments</Text>
                </View>
            </Footer>
  );
}
}

 

export default FooterComp;