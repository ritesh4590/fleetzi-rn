/**
* This is the Footer Component
**/

// React native and others libraries imports
import React from 'react';
import { View, Text, StyleSheet, Platform,TouchableOpacity, Dimensions } from 'react-native';
import { Actions } from 'react-native-router-flux';
import HomeFooter from '../component/svg/Home'
import Payments from '../component/svg/Payments'
import ListingSVG from '../component/svg/Listing'
import Order from '../component/svg/Order'

(widthDim = Dimensions.get('window').width),
(heightDim = Dimensions.get('window').height);
const Footer = (props) => {

  return (
    <View style={[styles.footer, {backgroundColor: props.color ? props.color : '#0F1110'}]}>
       
                <TouchableOpacity onPress={() => Actions.home()} style={props.index==='home' ?  styles.iconBox :                    styles.onlyBox } >
                     <HomeFooter />
                     <Text style= { props.index==='home' ?  styles.listingStySelect : styles.listingSty }>Home</Text>
                </TouchableOpacity>
                
                 
                <TouchableOpacity onPress={() => Actions.listing()} style=      { props.index==='listing' ?  styles.iconBox :               styles.onlyBox } >
                      <ListingSVG />
                      <Text style= { props.index==='listing' ?  styles.listingStySelect : styles.listingSty }>Listing</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => Actions.ordermain()}
                       style={ props.index==='ordermain' ?  styles.iconBox : styles.onlyBox } >
                       <Order />
                       <Text style= { props.index==='ordermain' ?  styles.listingStySelect : styles.listingSty }>Order</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => Actions.payment()} style=        { props.index==='payment' ?  styles.iconBox :               styles.onlyBox } >
                        <Payments />
                       <Text style= { props.index==='payment' ?  styles.listingStySelect : styles.listingSty }>Payment</Text>
                </TouchableOpacity>
    </View>
  );

}

const styles = StyleSheet.create({
    footer: {
        flex: Platform.OS === 'ios' ? 1 : 1.2,
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent: 'space-between',
        paddingLeft:15,
        // paddingRight:15
      },
      iconBox:{
        backgroundColor:'#1A1D1C',
        height:80,
        width:widthDim*0.23,
        paddingTop:15,
        borderRadius:5
      },
      onlyBox:{
        width:widthDim*0.23,
      },
      
      listingSty:{
        color:'#FFFFFF',
        textAlign:'center',
        paddingTop:5,
        letterSpacing:1,
        fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Light'
      },
      listingStySelect:{
        color:'#FF8B00',
        textAlign:'center',
        paddingTop:5,
        letterSpacing:1,
        fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Light'
      },
     
  });
  

export default Footer;