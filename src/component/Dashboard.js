import React from 'react'
import { View,Text,StyleSheet, Platform, FlatList, TouchableHighlight } from 'react-native'
import { Col, Row, Grid } from "react-native-easy-grid";
import Svg, { SvgUri, G, Path, Def,Circle,Stop,Rect } from'react-native-svg';
import {  Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import HeaderComp from "./HeaderComp"
import Bell from './svg/bell'
import Calendar from './svg/calendar'
import Chevron from './svg/chevron'
import Icon1 from './svg/icon1'
import Icon2 from './svg/icon2'
import Icon3 from './svg/icon3'
import Icon4 from './svg/icon4'
import Flogo from './svg/Flogo'
import Profile from './svg/Profile'
import Contact from './svg/Contact'
class Dashboard extends React.Component{
  rendersvg(index){
    // var Num = 'Icon'+index
    // return(
    //   <Num />
    // )
    if (index === 1) {
      return(
        <Icon1 />
      )}
      if (index === 2) {
        return(
          <Icon2 />
        )}
      if (index === 3) {
        return(
          <Icon3 />
      )}
      if (index === 4) {
        return(
          <Icon4 />
      )}
      if (index === 5) {
        return(
          <Icon2 />
      )}
      if (index === 6) {
          return(
            <Icon1 />
      )}
      if (index === 7) {
          return(
            <Icon4 />
      )
    }
    if (index === 8) {
      return(
        <Icon4 />
    )}
    if (index === 9) {
      return(
        <Icon2 />
    )}
    if (index === 10) {
        return(
          <Icon1 />
    )}
    if (index === 11) {
      return(
        <Icon4 />
  )
}if (index === 12) {
  return(
    <Icon1 />
)
}if (index === 13) {
  return(
    <Icon2 />
)
}if (index === 14) {
  return(
    <Icon3 />
)
}if (index === 15) {
  return(
    <Icon4 />
)
}
  }
  render(){
    const DATA = [
      {
        image:<icon/>,
        heading:'Fleetzi_BOX1  ',
        subHeading:30000,
        unit:'SAR',
        key:1,
        chevron:'https://cdn1.iconfinder.com/data/icons/arrows-150/100/sign-chevron-right-512.png',
      },
      {
        image:'https://tjori.gumlet.com/images/image/415497-2019-10-23-15:42:42.535782-image.jpg',
        heading:'Fleetzi_BOX2',
        subHeading:30000,
        unit:'SAR',
        key:2  ,
        chevron:'https://cdn1.iconfinder.com/data/icons/arrows-150/100/sign-chevron-right-512.png',
      },
      {
        image:'https://tjori.gumlet.com/images/image/415497-2019-10-23-15:42:42.535782-image.jpg',
        heading:'Fleetzi_BOX3',
        subHeading:30000,
        unit:'SAR',
        key:3,
        chevron:'https://cdn1.iconfinder.com/data/icons/arrows-150/100/sign-chevron-right-512.png',
        image:'https://tjori.gumlet.com/images/image/415497-2019-10-23-15:42:42.535782-image.jpg',
      },
      {
        image:'https://tjori.gumlet.com/images/image/415497-2019-10-23-15:42:42.535782-image.jpg',
        heading:'Fleetzi_BOX4',
        subHeading:30000,
        unit:'SAR',
        key:4 ,
        chevron:'https://cdn1.iconfinder.com/data/icons/arrows-150/100/sign-chevron-right-512.png',
        image:'https://tjori.gumlet.com/images/image/415497-2019-10-23-15:42:42.535782-image.jpg',
      },
      {
        image:'https://tjori.gumlet.com/images/image/415497-2019-10-23-15:42:42.535782-image.jpg',
        heading:'Fleetzi_BOX5',
        subHeading:30000,
        unit:'SAR',
        key:5 ,
        chevron:'https://cdn1.iconfinder.com/data/icons/arrows-150/100/sign-chevron-right-512.png',
        image:'https://tjori.gumlet.com/images/image/415497-2019-10-23-15:42:42.535782-image.jpg',
      },
      {
        image:'https://tjori.gumlet.com/images/image/415497-2019-10-23-15:42:42.535782-image.jpg',
        heading:'Fleetzi_BOX6',
        subHeading:30000,
        unit:'SAR',
        key:6 ,
        chevron:'https://cdn1.iconfinder.com/data/icons/arrows-150/100/sign-chevron-right-512.png',
        image:'https://tjori.gumlet.com/images/image/415497-2019-10-23-15:42:42.535782-image.jpg',
      },
      {
        image:'https://tjori.gumlet.com/images/image/415497-2019-10-23-15:42:42.535782-image.jpg',
        heading:'Fleetzi_BOX7',
        subHeading:30000,
        unit:'SAR',
        key:7 ,
        chevron:'https://cdn1.iconfinder.com/data/icons/arrows-150/100/sign-chevron-right-512.png',
        image:'https://tjori.gumlet.com/images/image/415497-2019-10-23-15:42:42.535782-image.jpg',
      },  {
        image:'https://tjori.gumlet.com/images/image/415497-2019-10-23-15:42:42.535782-image.jpg',
        heading:'Fleetzi_BOX8',
        subHeading:30000,
        unit:'SAR',
        key:8 ,
        chevron:'https://cdn1.iconfinder.com/data/icons/arrows-150/100/sign-chevron-right-512.png',
        image:'https://tjori.gumlet.com/images/image/415497-2019-10-23-15:42:42.535782-image.jpg',
      },
    ];
    return(
      
      <View style={styles.body_contaioner}>
      
        
        <TouchableHighlight onPress={() => Actions.home2()} underlayColor="transparent" >
          <Text style={styles.heading_text}> Hi Partner</Text>
        </TouchableHighlight>
         <View style={styles.more_items_main}>
             
              <Grid>
                <Col size={25}>
                <Bell/>
                </Col>
                <Col size={60} style={{justifyContent:'center'}}>
                <Text style={styles.no_of_items} onPress={this.onPressTitle}>1 New Order</Text>
                </Col>
                <Col size={15}>
                <View style={styles.chevron_right_svg}>
              <Chevron/>
              </View>
                </Col>
              </Grid>
            
         </View>
        <View style={styles.do_you_know_text}>
            <Text style={styles.small_text_do}>Do you Know?</Text>
            <Text style={styles.small_text_do}>
              Keeping Your listings up to date will give you
              much better chances for good quality enquiries
            </Text>
        </View>

        
        <View style={styles.main_dashboard_container}>
              <View>
                <Text style={styles.dashboard}>Dashboard</Text>
                <Text style={styles.month_yr}>January 2020</Text>
              </View >
              <View style={styles.wallet_svg_import}>
              <Calendar/>
              </View>
      </View>
      <FlatList
        data={DATA}
        renderItem={({ item }) =>
                <View style={styles.main_inner_content}>
                 <Grid  >
                    <Col size={15}>
                    <View style={styles.import_icons_svg}>
                      {this.rendersvg(item.key)}
                      </View>
                    </Col>
                    <Col size={70}>
                    <Text style={styles.heading_flatlist} numberOfLines={1}>
                      {item.heading}
                    </Text>
                    <View style={styles.subheading_unit_main} numberOfLines={1}>
                    <Text style={styles.subHeading_text}>{item.subHeading}</Text>
                    <Text style={styles.unit_text}>{item.unit}</Text>
                    </View>
                    </Col>
                    <Col size={10}>
                    <View style={styles.chevron_right_svg_content_flatlist}>
                        <Chevron/>
                    </View>
                    </Col>
                </Grid>
                 </View>
         
        }
      />
      </View>
          
 
    )
  }
}
export default Dashboard


const styles = StyleSheet.create({
  
  body_contaioner:{
    width:'90%',
    marginTop: 0,
    marginRight: 'auto',
    marginBottom: 0,
    marginLeft: 'auto',
    flex:1
  },
  heading_text:{
    color:'white',
    fontSize:22,
    fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
  },
  more_items_main:{
    width:'60%',
    height:50,
    backgroundColor:'#2D2F30',
    marginTop:20,
    borderRadius:3,
    padding:10,
  
  },
  img1:{
    height:30,
    width:30,
  },
  no_of_items:{
    color:'#C18042',
    fontSize:14,
    fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular',
    alignItems:'baseline',
    
  },
  do_you_know_text:{
    width:'90%',
    marginTop:20,
  },
  small_text_do:{
    color:'#515154',
    fontSize:14,
    letterSpacing:.15,
    fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'
  },
  main_dashboard_container:{
    backgroundColor:'#1D2124',
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    justifyContent:'space-between',
    flexDirection:'row',
    marginTop:30,
    padding:20
  },
  dashboard:{
    color:'white',
    fontSize:18,
    fontStyle:'normal',
    fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
  },
  month_yr:{
    color:'#6D7172',
    fontSize:15,
    letterSpacing:.5,
    fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'
  },
  image_dp:{
    height:60,
    width:60,
  },
  chevron_dp:{
    height:50,
    width:50,
    position:'absolute',
    right:0,
  },
  main_inner_content:{
    flexDirection:'row',
    backgroundColor:'#1D2122',
    marginTop:1.5,
    padding:20
  },
  heading_flatlist:{
    width:'80%',
    color:'#C18042',
    fontSize:15,
    fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Regular'
  },
  subheading_unit_main:{
    flexDirection:'row',
    width:"100%",
    alignItems:'baseline',
    marginTop:Platform.OS === 'ios' ? 5 : 0

  },
  subHeading_text:{
    color:'white',
    fontSize:24,
    fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold'
  },
  unit_text:{
    color:'white',
    fontSize:15,
    fontFamily:Platform.OS === 'ios' ? 'NexaBold' : 'Nexa Bold',
    marginHorizontal:3,
    // paddingBottom:3
  },
  chevron_right_svg:{
    height:30,
    width:30,
    backgroundColor:'#37383A',
    justifyContent:"center",
    alignItems:'center',
    
  },
  chevron_right_svg_content_flatlist:{
    height:35,
    width:35,
    backgroundColor:'#37383A',
    justifyContent:"center",
    alignItems:'center',
    marginTop:10,
    marginRight:15
  },
  wallet_svg_import:{
    paddingRight:5,
    paddingTop:5
  },

  import_icons_svg:{
    marginTop:7
  }
})

