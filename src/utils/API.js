// utils/API.js

import axios from "axios";

export default axios.create({
  baseURL: "http://api.fleetzi.com/v1/",
  responseType: "json",
  // headers: {'Content-Type': 'application/json; charset=UTF-8' }
});
