/**
 * @format
 */
if(__DEV__) {
    import('./ReactotronConfig').then(() => console.log('Reactotron Configured'))
}
import bgMessaging from './bgMessaging';
import {AppRegistry} from 'react-native';
import App from './App';
import Index from './src/Index'
import {name as appName} from './app.json';
// Current main application
AppRegistry.registerComponent(appName, () => Index);
// New task registration
AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging);
